import { SocialAnalysis } from './social-analysis';
import { PostAnalysis } from './post';
import { Utils } from './utils';
import { ProfileAnalysis } from './profile';
import { ImageAnalysis } from './image-analysis';
import { Stats } from './stats';
import { Stories } from './stories';
import { FollowersAnalysis } from './followers';
import { expect, assert} from 'chai';
import { User, Story } from './db-schema';
import mocha from 'mocha';
import { fail } from 'assert';
import { readFileSync } from 'fs';
import { Tags } from './tags';
import { PrivateProfile } from './private-profile';
import { PostingLocation, PostingUsertags } from 'instagram-private-api/dist/types/posting.options'

let USER = 'officialsscnapoli';
mocha.describe('Get followers function', function () {
  let 
    utils = new Utils();
    utils.writeLog = <any>console.log;
  let
    post = new PostAnalysis(),
    followers = new FollowersAnalysis(),
    profile = new ProfileAnalysis(),
    stories = new Stories(),
    stats = new Stats(),
    social_analysis = new SocialAnalysis(),
    tags = new Tags(),
    image_analysis = new ImageAnalysis(),
    private_profile = new PrivateProfile();

  
  this.timeout(150000);

  // GET ALL FOLLOWERS
  xit('should get ALL followers', async () => {
    
    utils.connectToDB();
    let auth = await profile.login();
    const username = USER;
    const pk = 0;
    const result = await followers.getFollowers(username, pk);
    assert(typeof result === typeof User);
  });

  xit('Should get archived stories information', async function () {
    this.timeout(10000);
    let sa = new SocialAnalysis();
    await profile.login();
    const archivedStories = await stories.getArchivedStories();
    console.log(archivedStories);
    expect(archivedStories.length).to.be.greaterThan(0);
  });

  xit('Should get 1 archived stories with voters', async function () {
    this.timeout(70000);
    let sa = new SocialAnalysis();
    const auth = await profile.login();
    if (!auth) fail("No auth");
    stories.getArchivedStories = async function () {
        return <any>[
            {
                timestamp: 1562371300,
                media_count: 9,
                id: 'archiveDay:17850004441478177',
                reel_type: 'archive_day_reel',
                latest_reel_media: 1562403786
            }
        ]
    }
    utils.SLEEP_TIMEOUT = 3;
    const personWithArchivedStories = await stories.getAllArchiviedStoriesWithVoters(auth['username'], auth['pk']);
    //console.log(personWithArchivedStories);
    expect(personWithArchivedStories.stories.length).to.be.greaterThan(0);
  });

  
  // GET ALL ARCHIVED STORIES WITH ALL POLL VOTERS
  xit('Should get ALL archived stories with voters', async function () {
    let sa = new SocialAnalysis();
    const auth = await profile.login();
    if (!auth) fail("No auth");
    utils.SLEEP_TIMEOUT = 10;
    const personWithArchivedStories = await stories.getAllArchiviedStoriesWithVoters(auth['username'], auth['pk']);
    //console.log(personWithArchivedStories);
    expect(personWithArchivedStories.stories.length).to.be.greaterThan(0);
  });

  // GET ACTIVE STORIES
  xit('Should get active story with all reels', async function () {
    let username = USER;
    utils.connectToDB();
    utils.SLEEP_TIMEOUT = 10;
    let auth = await profile.login();
    if (!auth) fail("No auth");
    let person = await stories.getActiveStories(username);
    assert(typeof person === typeof User);
  });

  // GET USER INFO FOR ALL USERS IN DB
  xit('Should get all user info in db', async function () {
    utils.connectToDB();
    utils.SLEEP_TIMEOUT = 10;
    let auth = await profile.login();
    let users = await User.find({}).select({ "username": 1, "_id": 0, "user_id": 1, "info": 1}).exec();
    console.log("Total users: " + users.length);
    let c = 0;
    let userInfo;
    for (let user of users) {
        userInfo = await profile.getUserInfo(user.username, user.user_id);
        console.log(user.username + " " + c + "/" + users.length);
        c += 1;   
    }
    expect(typeof userInfo).to.be.equal(typeof User);
  });

  xit('Should not find one in mongo', async function () {
    utils.connectToDB();
    let found = await utils.find(Story, "story_id", "archiveDay:17850004441478177");
    assert(found ===  null);
  });

  xit('Should find one user in mongo', async function () {
    utils.connectToDB();
    let found = await utils.find(User, "username", "antiumnostra");
    console.log(found);
    assert(found !==  null);
  });

  xit('Should connect to mongo', () => {

    utils.connectToDB();
  });

  // GET ALL POSTS WITH ALL LIKERS.
  xit('Should get posts with likers', async function () {
    utils.connectToDB();
    const username = USER;
    const auth = await profile.login();
    await post.getPosts(username).should.be.greaterThan(0);
    
  });

  // GET ALL POSTS WITH ALL LIKERS Followers and update db.
  xit('Should get posts with likers, followers and update db.', async function () {
    utils.connectToDB();
    const username = USER;
    const auth = await profile.login();
    console.log("Processing followers for " + username);
    const response = await followers.getFollowers(username);
    console.log(response);
    console.log("Processing posts for " + username);
    const resp = await post.getPosts(username);
    console.log(resp);
    let users = await User.find({}).select({ "username": 1, "_id": 0, "user_id": 1, "info": 1}).exec();
    console.log("Total users: " + users.length);
    let c = 0;
    let userInfo;
    for (let user of users) {
        userInfo = await profile.getUserInfo(user.username, user.user_id);
        console.log(user.username + " " + c + "/" + users.length);
        c += 1;   
    }
    console.log("Processed " + c + " users");
  });

  // GET INTERACTED USERS WITH SCORE
  xit('Should get interacted users with score', async function () {
    utils.connectToDB();
    const username = USER;
    await stats.getUniqueInteractedUsers(username);
    let person = await stats.calculateMostEngagedUsers(username);
    assert(person.interacted_with_data.length > 0);
    
  });

  // ANALYSE NEW USER
  xit('Should Analyse user and save info in db.', async function () {
    utils.connectToDB();
    await profile.login();
    let username = USER;
    let user = await social_analysis.analyseUser(username);
    assert(user.posts.length > 0 === true);
  });

  xit('Should calculate engagement for user posts and stories.', async function () {
    let sa = new SocialAnalysis();
    utils.connectToDB();
    //await profile.login();
    let username = USER;
    let user = await stats.calculatePostsEngagement(username);
    assert(user.stats.posts.posts_with_engagement.length > 0 === true);
  });

  xit('Should get user data in bulk.', async function () {
    let sa = new SocialAnalysis();
    utils.connectToDB();
    //await profile.login();
    let username = USER;
    let postIds = [
        '2206680454883575200', '2203139185511192787', '2191642760516377933',
        '2156709736171498150', '2140846227839808718', '2117034648669518200',
        '2105243987887962740', '2093753925007166852', '2089969504055467223',
        '2082863868645822519', '2078435919892845675', '2065428840151049753',
        '2045056893122537544', '2034922660014987059', '2031254973263078339',
        '2024775818795069437', '2008143823961021398', '2007356434225293073',
        '2005317500901021560', '2003108134115126487', '2002262232395869315',
        '1990010635304570772', '1987085082964594196', '1921225911975564676',
    ];
    let posts = await profile.getUserSavedPosts(username);
    assert(posts.length === postIds.length);
  });

  // CALCULATE ALL STATS AND EXPORT TO CSV
  xit('Should export user data to CSV after calculating all engagement.', async function () {
    utils.SLEEP_TIMEOUT = 4;
    utils.connectToDB();
    await profile.login();
    let username = USER;
    console.log("Calculating most engaged users")
    let person = await stats.calculateMostEngagedUsers(username);
    console.log("Calculating most engaged users")
    let user = await stats.calculatePostsEngagement(username);
    console.log("Exporting data to CSV");
    let savedFiles = await stats.exportPostsEngagementToCSV(username);
    let savedInteractorFiles = await stats.exportInteractorsDataToCSV(username);
    assert(savedFiles.length > 0 === true);
  });

  // CALCULATE ONLY MOST ENGAGED USERS AND EXPORT TO CSV
  xit('Should export user data to CSV after calculating all engagement.', async function () {
      let username = "officialasroma";
      utils.SLEEP_TIMEOUT = 4;
      utils.connectToDB();
      utils.ANALYSIS_CONFIG.profiles[username].analysis.limits.max_posts = 1800;
      let eng = stats.calculateMostEngagedUsers(username);
      let savedFiles = await stats.exportInteractorsDataToCSV(username);
      console.log(savedFiles);
      assert(savedFiles.length > 0 === true);
  });

  // ANALYSE NEW USERS IN BATCH
  xit('Should Analyse list of users in batch.', async function () {
    utils.SLEEP_TIMEOUT = 4;
    utils.connectToDB();
    let auth = await profile.login();
    if (auth) console.log("Logged in as:::: " + auth.username);
    //let count = await sa.analyseBatchUsernames();
    //assert(count > 0 === true);
  });

  xit('Should create next max id from latest post', async function () {
    let pk = 460993888;
    utils.SLEEP_TIMEOUT = 4;
    utils.connectToDB();
    let auth = await profile.login();
    if (auth) console.log("Logged in as:::: " + auth.username);
    let userFeed = profile.IG.feed.user(pk);
    console.log("creating next max id")
    let newUserFeed = await utils.createMaxId(userFeed);
    let posts = await newUserFeed.items();
    console.log(posts.length, newUserFeed);
    assert(newUserFeed['nextMaxId'] === '2211195130047288955_460993888' && posts.length > 0);
  });
  
  // Analyse all user who interacted with the starting username
  xit('Should Analyse list of users who interacted with a specific user.', async function () {
    utils.connectToDB();
    let auth = await profile.login();
    if (auth) console.log("Logged in as:::: " + auth.username);
    let count = await profile.populateAllUserInfo(USER);
    assert(count > 0 === true);
  });

  xit('Should fetch post with action key', async function () {
    utils.SLEEP_TIMEOUT = 4;
    utils.connectToDB();
    let auth = await profile.login();
    if (auth) console.log("Logged in as:::: " + auth.username);
    let user = await social_analysis.analyseUser("fcinter", ['posts']);
    assert(user.posts.length > 0); 
  });

  xit('Should analyse user with action key', async function () {
    
    utils.SLEEP_TIMEOUT = 4;
    utils.connectToDB();
    let auth = await profile.login();
    if (auth) console.log("Logged in as:::: " + auth.username);
    let user = await social_analysis.analyseUser("officialsscnapoli", ['analysis']);
    assert(user.interacted_with_engagement.length > 0);
  });  

  xit('Should do userinfo with action key', async function () {
    
    utils.SLEEP_TIMEOUT = 4;
    utils.connectToDB();
    let auth = await profile.login();
    if (auth) console.log("Logged in as:::: " + auth.username);
    let user = await social_analysis.analyseUser("antiumnostra", ['userinfo']);
    assert(user.interacted_with_engagement.length > 0);
  });  

  xit('Should get posts anonmyously', async function () {
    utils.SLEEP_TIMEOUT = 4;
    utils.connectToDB();
    let person = <any>await post.getPostsAnonymous("acmilan");
    assert(person.posts.length > 0 === true);
  });

  xit('Should get posts anonmyously with remote db', async function () {
    utils.SLEEP_TIMEOUT = 4;
    utils.connectToDB(1);
    let person = <any>await post.getPostsAnonymous("acmilan");
    assert(person.posts.length > 0 === true);
  });

  xit('Should get list of proxies', async function () {
    utils.SLEEP_TIMEOUT = 4;
    utils.connectToDB();
    await utils.getProxies();
    console.log(utils.proxies);
    assert(utils.proxies.length > 0 === true);
  });

  xit('Should get list of user agents from local file', async function () {
    utils.SLEEP_TIMEOUT = 4;
    utils.connectToDB();
    let uaStrings = await utils.getUserAgentString();
    assert(uaStrings.length > 0 === true);
  });

  xit('Should get list of proxies which work being queried from AWS', async function () {
    utils.connectToDB();
    await utils.getProxiesWorkingWithAWS();
    assert(utils.proxies.length > 0 === true);
  });

  xit('Should return AWS Rekognition result after getting the image with axios', async function () {
    let imageUrl = "https://scontent-lhr8-1.cdninstagram.com/v/t51.2885-15/e35/30086309_451168418629423_2681310231346020352_n.jpg?_nc_ht=scontent-lhr8-1.cdninstagram.com&_nc_cat=107&_nc_ohc=qmk6enphvFcAX8fPAEJ&oh=82e0ab4741359de523c598bed802aa6a&oe=5EA8771D";
    let res = await image_analysis.getAWSRekognitionResultMedia(imageUrl);
    assert(res !== undefined);
  });

  xit('Should analyse all image media for a user', async function () {
    let username = USER;
    utils.writeLog = <any>console.log;
    utils.connectToDB();
    let count = await image_analysis.getPostsImagesAnalysis(username);
    assert (count > 0 === true);
  });

  xit('Should calculate Most Engaging Labels', async function () {
    let username = USER;
    utils.connectToDB();
    let labels = await stats.calculateMostEngagingLabels(username);
    console.log(labels);
    assert (Object.keys(labels).length > 0 === true);
  });

  xit('Should calculate engagement for contacts and export to CSV', async function () {
    let username = USER;
    await utils.connectToDB();
    let totalContacts = await stats.calculateEngagementAndExportContactInfoToCSV(username)
    assert (totalContacts > 0 === true);
  });
  
  xit('Should extract mentions and tags from text', async function () {
    let text = "this is a post with @mentions, #hashtags, @morementions and #morehashtags"
    let extractedInfo = await post.extractPostTextData(text);
    assert (extractedInfo['mentions'] && extractedInfo['tags'] && extractedInfo['mentions'].length > 0 === true && extractedInfo['tags'].length > 0 === true);
  });


  xit('Should extract mentions and tags from all posts of a user', async function () {
    let username = USER;
    await utils.connectToDB();
    let savedFiles = await post.extractAllTagsAndMentionsFromPostsText(username);
    console.log(savedFiles);
    assert (savedFiles.length === 2 === true);
  });

  xit('Should extract mentions and tags from all posts of a user for a list of users', async function () {
    await utils.connectToDB();
    let usernames = <any>[];
    for (let username of usernames) {
      console.log("Processing " + username);
      let savedFiles = await post.extractAllTagsAndMentionsFromPostsText(username);
      console.log(savedFiles);
      assert (savedFiles.length === 2 === true);
    }
    
  });

  xit('Should extract most engaged users within a date range', async function () {
    let username = USER;
    await utils.connectToDB();
    utils.ANALYSIS_CONFIG.profiles[username].analysis.date_from = "2020-01-01T00:00:00.000Z";
    let eng = await stats.calculateMostEngagedUsers(username);
    let savedFiles = await stats.exportInteractorsDataToCSV(username);
    console.log(savedFiles);
    assert (savedFiles.length === 1 === true);
  });

  xit('Should calculate engagement for contacts and export to CSV for a specific DATE range', async function () {
    let username = USER;
    await utils.connectToDB();
    utils.ANALYSIS_CONFIG.profiles[username].analysis.date_from = "2020-01-01T00:00:00.000Z";
    let totalContacts = await stats.calculateEngagementAndExportContactInfoToCSV(username)
    assert (totalContacts > 0 === true);
  });

  xit('Should process a list of usernames to get missing info', async function () {
    let usernames = await readFileSync('./usernames_to_fetch.txt').toString().split(/\r?\n/);
    await utils.connectToDB();
    //let totalProcessedProfiles = await batch_analysis.getUserInfoList(usernames);
    //assert (totalProcessedProfiles === usernames.length === true);
  });

  xit('Should make query for explore page by a specific tag', async function () {
    let tag = "primagliitaliani";
    let initialExplorePageResponse = await tags.getExplorePageAnonymousQuery(tag);
    assert (initialExplorePageResponse.graphql.hashtag.edge_hashtag_to_media.count > 0 === true);
  });

  xit('Should make query for explore page by a specific tag which doesnt exist', async function () {
    let tag = "rrttttprimagliitaliani";
    await utils.connectToDB();
    let initialExplorePageResponse = await tags.getExplorePageAnonymousQuery(tag);
    assert (initialExplorePageResponse === undefined);
  });

  xit('Should save a post coming from a tag page.', async function () {
    let tag = "primagliitaliani";
    await utils.connectToDB();
    let initialExplorePageResponse = await tags.getExplorePageAnonymousQuery(tag);
    let post = initialExplorePageResponse.graphql.hashtag.edge_hashtag_to_media.edges[0];
    let person = await tags.savePostAndCreateProfileFromExploreTag(post);
    assert (person && person.posts.length > 0);
  });

  xit('Should download posts for a tag all or until max posts per tag reached', async function () {
    let tag = "primagliitaliani";
    await utils.connectToDB();
    let savedHashtag = await tags.getExplorePagePostsByTag(tag);
    assert (savedHashtag.posts.length > 0);
  });

  xit('Should fetch a profile anonymously', async function () {
    let username = "zerocalcare";
    let profileData = <any>await profile.getProfileAnonymousQuery(username);
    assert (profileData && profileData.graphql.user);
  });

  xit('Should fetch profile info with preflight request', async function () {
    let username = "zerocalcare";
    profile.THIRD_PARTY_PROVIDERS_CONFIG = "./thirdpartyproviders_with_preflight.json";
    let profileData = <any>await profile.getProfileAnonymousQuery(username);
    assert (profileData && profileData.graphql.user);
  });

  xit('Should calculate number of posts per user for a tag', async function () {

    utils.connectToDB();
    stats.utils.writeLog = <any>console.log;
    const tag = "lazionapoli";
    let results = await stats.uniqueTagPosters(tag);
    assert(results.length > 0);
    
  });

  xit('Should create config to post an image', async function () {
    utils.connectToDB();
    stats.utils.writeLog = <any>console.log;
    const caption = "Viva l'Italia!";
    const imagePath = "./images/image_1.JPG";
    const postingLocation = <PostingLocation> {
      name: "Italy"
    };
    const userTags = <PostingUsertags> {
      in: [
        {
          user_id: 47884563,
          position: [0,0]
        }
      ]
    };

    const file = await utils.loadImage(imagePath);
    console.log("Image loaded: " + imagePath);
    let postConfig = await private_profile.createPostConfig(caption, file, postingLocation, userTags);
    console.log(typeof postConfig.file);
    
    assert(typeof postConfig.file === 'object');
    
  });

  xit('Should create config AND post an image without location or tagged profiles', async function () {
    utils.connectToDB();
    stats.utils.writeLog = <any>console.log;
    const caption = "Viva l'Italia!";
    const imagePath = "./images/image_2.JPG";
    const file = await utils.loadImage(imagePath);
    console.log("Image loaded: " + imagePath);
    let postConfig = await private_profile.createPostConfig(caption, file);
    let postingResult = await private_profile.publishPost(postConfig);
    console.log(postingResult);
    assert(postingResult && postingResult.media !== undefined);
    
  });

  xit('Should create config AND post an image with a location', async function () {
    utils.connectToDB();
    stats.utils.writeLog = <any>console.log;
    const caption = "Viva l'Italia!";
    const imagePath = "./images/image_1.JPG";
    const postingLocation = <PostingLocation> {
      name: "Italy",
      lat: 43.0,
      lng: 12.0,
      external_id: "270531492",
      //external_id_source: "xyz",
      address: "IT"
    };
    const userTags = <PostingUsertags> {
      in: []
/*        {
          user_id: 31821916805,
          position: [120,120]
        }
      ]*/
    };

    const file = await utils.loadImage(imagePath);
    console.log("Image loaded: " + imagePath);
    let postConfig = await private_profile.createPostConfig(caption, file, postingLocation, userTags);
    let postingResult = await private_profile.publishPost(postConfig);
    console.log(postingResult);
    assert(postingResult && postingResult.media !== undefined);
    
  });

  xit('LIVE POSTING Should create and publish a post given text and image and save results in db.', async function () {
    utils.connectToDB();
    stats.utils.writeLog = <any>console.log;
    const caption = "Viva l'Italia!";
    const imagePath = "./images/image_3.JPG";
    let savedPerson = await private_profile.postImageContent(caption, imagePath)
    assert(savedPerson && savedPerson.posted.length > 0);
  });

  xit('Check if content should be posted now', async function () {
    stats.utils.writeLog = <any>console.log;
    let datetime = "2020-05-03T19:28:00";
    let canBePosted = utils.isItTimeToPost(datetime);
    assert(canBePosted === true);
  });

  xit('Post scheduled content if it is the right time', async function () {
    private_profile.utils.writeLog = <any>console.log;
    //let username = "italia_patria_nostra";
    let username = "rpolverini11";
    let postedCount = await private_profile.postScheduledItems(username);
    assert(postedCount && postedCount > 0 === true);
    process.exit(1);
  });

  xit('Calculate posts engagement for a hashtag', async function () {
    utils.connectToDB();
    let tag = "forzanapolisempre";
    stats.utils.writeLog = <any>console.log;
    let item = await stats.calculatePostsEngagement(tag, 'hashtag');
    assert(item.engagement.posts.posts_with_engagement.length > 0);
    console.log("Exporting data to CSV");
    let savedFiles = await stats.exportPostsEngagementToCSV(tag, 'hashtag');
    console.log(savedFiles);
    assert(savedFiles.length > 0);
  });

  it('Get all user info', async function () {
    private_profile.utils.writeLog = <any>console.log;
    let count = await profile.populateAllUserInfo();
    assert(count > 0 === true);
            
  });
});


        