import { PostAnalysis } from './post';
import { Utils } from './utils';
import { Tags } from './tags';
import { ProfileAnalysis } from './profile';
import { User, Post, IPost } from './db-schema';
import axios from 'axios';
import AWS from 'aws-sdk';
import { CONFIG, ANALYSIS_CONFIG } from './config.interface';

export class ImageAnalysis {

    constructor () {
        this.post = new PostAnalysis;
        this.utils = new Utils;
        this.profile = new ProfileAnalysis();
        this.tags = new Tags();
        this.AWS_CONFIG = './aws_creds.json';
        this.CONFIG = this.utils.CONFIG;
        this.ANALYSIS_CONFIG = this.utils.ANALYSIS_CONFIG;
    }
    post: PostAnalysis;
    tags: Tags;
    utils: Utils;
    profile: ProfileAnalysis;
    AWS_CONFIG: string;
    CONFIG: CONFIG;
    ANALYSIS_CONFIG: ANALYSIS_CONFIG;
    // IMAGE ANALYSIS

    /**
     * Analyses images in posts to understand what components are contained, updates the post object.
     * @param inputText identifier for what to extract reks for either a useranem or a hashtag
     */
    async getPostsImagesAnalysis (inputText: string, inputType: string = 'username') {
        let posts = <any>[];
        let count = 0;
        let index = 0;
        if (inputType === 'username') {
            posts = await this.profile.getUserSavedPosts(inputText);
        } else if (inputType === 'hashtag') {
            posts = await this.profile.getUserSavedPosts(inputText);
        }
        for (let post of posts) {

            let images = <any>[];
            if (post.info['__typename'] === 'GraphVideo') {
                continue;
            }
            if (post.info['image_versions2']) {
                let image = post.info['image_versions2']['candidates'][0].url;
                images.push(image);
            } else if (post.info['carousel_media']) {
                for (let c_media of post.info['carousel_media']) {
                    images.push(c_media['image_versions2']['candidates'][0].url);
                }
            } else if (post.info['display_url']) {
                images.push(post.info['display_url']);
            } else if (post.info['dispaly_resources']) {
                images.push(post.info['dispaly_resources'][2].src);
            }
            if ( (post.media_data && post.media_data.length === 0) || !post.media_data ) {
                 let savedPostWithImageData = await this.utils.downloadImageMedia(post);
                 if (savedPostWithImageData.length > 0) {
                     post.media_data = savedPostWithImageData;
                 }
            }
            for (let image of images) {
                this.utils.writeLog("Processing image: " + image + " of post ("+ post.post_id +") " + index + "/" + posts.length);
                let analysis = await this.getAWSRekognitionResultMedia(image);
                if (analysis === undefined) {
                    this.utils.writeLog("analysis is undefined");
                    continue;
                }
                let imageInfo = {
                    analysis: analysis,
                    image: image
                }
                post.image_analysis.push(imageInfo);
                count += 1;
            }
            index += 1;
            let savedPost = await this.utils.createOrUpdate(Post, "post_id", post.post_id, post);
            this.utils.writeLog("Total images analysed for post ("+post.post_id+") : " + savedPost.image_analysis.length);
        }
        this.utils.writeLog("Processed with AWS Rekognition: " + count + " images for " + inputText);
        return count;
    }

    /**
     * Uses AWS Rekognition to get labels in image. Creates the params for the actual call.
     * @param imageUrl path of public location of image, string.
     */
    async getAWSRekognitionResultMedia (imageUrl: string) {
        try {
            let response = await axios.get(imageUrl, {responseType: 'arraybuffer'});
            const buffer = new Buffer(response.data, 'base64');
            let params = {
                Image: { 
                Bytes: buffer
                }
            };
            let labels = await this.doRek('detectLabels', params);
            let text = await this.doRek('detectText', params);

            let celebrities = await this.doRek('recognizeCelebrities', params);
            params['Attributes'] = ["ALL", "DEFAULT"];
            let faces = await this.doRek('detectFaces', params);
            const res = {
                labels: labels,
                celebrities: celebrities,
                text: text,
                faces: faces
            };
            return res;
        } catch (err) {
            console.log("Error in rekognition", err);
            this.utils.writeLog("ERROR IN REKOGNITION");
            this.utils.writeLog(err);
        }
    }


    /**
     * Perfmors the actual call to the AWS Rekognition service
     * @param action Either labels, text, faces, celebrities ML process, string.
     * @param params Details of request to AWS, object.
     */
    async doRek (action: string, params: object) {
        // TODO: Move this in constructor!
        AWS.config.loadFromPath(this.AWS_CONFIG);
        const rekognition = await new AWS.Rekognition();
        return new Promise((resolve, reject) => {
            rekognition[action](params, function (err:any, data: any) {
                if (err) {
                    reject(err);
                }
                else
                    resolve(data);
            });
        });
    }



    /**
     * Aggregates a specific labels type extracted from the image of a post to the object with all already extracted labels.
     * @param postLabels List of specific labels(either celebs, labels, text, faces) extracted from an image, array.
     * @param labels overall dictionary with all the labels across all the posts, object.
     * @param post current post of a user being analyzed, IPost.
     * @param labelType either celebs, labels, text, faces extracted from image, string.
     */
    processLabels (postLabels: Array<any>, labels: object, post: IPost, labelType: string) {
        for (let label of postLabels) {
            let name;
            switch (labelType) {
                case "labels":
                    if (label.Confidence > this.CONFIG.min_feature_confidence) {
                        name = "labels__"+label.Name;
                        if (labels[name]) {
                            labels[name].push(post.engagement);
                        } else {
                            labels[name] = [post.engagement];
                        }
                    }
                    
                    break;
                case "celebrities":
                    if (label.Face.Confidence > this.CONFIG.min_feature_confidence_celebrities) {
                        name = "celebrities__"+label.Name;
                        if (labels[name]) {
                            labels[name].push(post.engagement);
                        } else {
                            labels[name] = [post.engagement];
                        }
                    }
                    
                    break;
                case "text":
                    name = "text__"+label.DetectedText;
                    if (labels[name]) {
                        labels[name].push(post.engagement);
                    } else {
                        labels[name] = [post.engagement];
                    }
                    break;
                case "faces":
                    if (labels["faces__TotalPeopleInImage_"+postLabels.length.toString()]) {
                        labels["faces__TotalPeopleInImage_"+postLabels.length.toString()].push(post.engagement);
                    } else {
                        labels["faces__TotalPeopleInImage_"+postLabels.length.toString()] = [post.engagement];
                    }
                    
                    if (typeof label === 'object') {
                        let features = Object.keys(label);
                        for (let feature of features) {
                            if (feature === "Emotions") {
                                for (let emotion of label[feature]) {
                                    if (emotion.Confidence > this.CONFIG.min_feature_confidence) {
                                        name = "faces__"+emotion.Type;
                                        if (labels[name]) {
                                            labels[name].push(post.engagement);
                                        } else {
                                            labels[name] = [post.engagement];
                                        }
                                    }
                                }
                            } else if (feature === "Landmarks") {

                            } else if (feature === "AgeRange") {
                                name = "faces__"+label[feature].Low.toString()+"Low-"+label[feature].High.toString()+"High"+feature;
                                if (labels[name]) {
                                    labels[name].push(post.engagement);
                                } else {
                                    labels[name] = [post.engagement];
                                }
                            } else {
                                let featurePresent = label[feature].Value;
                                if (featurePresent === true && label[feature].Confidence > this.CONFIG.min_feature_confidence) {
                                    name = "faces__"+feature;
                                    if (labels[name]) {
                                        labels[name].push(post.engagement);
                                    } else {
                                        labels[name] = [post.engagement];
                                    }
                                }
                            }
                        }
                    }
            }
            
        }
        return labels;
    }




}