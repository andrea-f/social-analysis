import { User } from './db-schema';
import { PostAnalysis } from './post';
import { Utils } from './utils';
import { ProfileAnalysis } from './profile';
import { ImageAnalysis } from './image-analysis';
import { Stats } from './stats';
import { Stories } from './stories';
import { FollowersAnalysis } from './followers';

  process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
    // application specific logging, throwing an error, or other logic here
    if (reason) console.log(reason['stack']);
  });

export class SocialAnalysis {
    constructor () {
        this.post = new PostAnalysis;
        this.utils = new Utils;
        this.profile = new ProfileAnalysis();
        this.image_analysis = new ImageAnalysis();
        this.stats = new Stats();
        this.stories = new Stories();
        this.followers = new FollowersAnalysis;
    }
    post: PostAnalysis;
    utils: Utils;
    profile: ProfileAnalysis;
    image_analysis: ImageAnalysis
    stats: Stats;
    stories: Stories;
    followers: FollowersAnalysis;

    // ENTRY POINT

    /**
     * Fetches all available data for a user.
     * @param username Identifier for the user to analyse.
     * @param actions List of things to do on a profile, array. 
     */
    async analyseUser (username: string, actions: Array<string> = []) {
        //this.writeLog(username, actions);
        if (username.length < 2) {
            this.utils.writeLog("[analyseUser] username: " + username + " is too short, returning." );
            return;
        }
        let user = await this.utils.find(User, "username", username);
        
        if (!user || !user.info) {
            // Fetch user information if its not already present.
            try {
                this.utils.writeLog("[analyseUser] Getting analysed user info for " + username);
                user = await this.profile.getUserInfo(username);
            } catch (err) {
                this.utils.writeLog("[analyseUser] Couldnt get userinfo because");
                this.utils.writeLog(err);
            }
        }
        if (actions.length === 0) {
            actions = [
                "posts",
                "engagement", 
                "rek",
                "stories", 
                "userinfo", 
                "contacts",
                "followers", 
                "labels",
                "textinfo"
            ];
        }
        /** GET POSTS */
        if (actions.indexOf("posts") !== -1) {
            
            try {
                /*if (this.CONFIG.logged_in === true) {
                    this.writeLog("Getting posts for " + username);
                    user = await this.getPosts(username);
                } else {*/
                this.utils.writeLog("[analyseUser] Getting anonimously posts for " + username);
                user = await this.post.getPostsAnonymous(username);
                //console.log(user);
                this.utils.writeLog("[analyseUser] FINISHED GETTING POSTS FOR " + username);
                console.log("[analyseUser] FINISHED GETTING POSTS FOR " + username);
            } catch (err) {
                this.utils.writeLog("[analyseUser] Error in get posts anon for " + username + " trying with logged in");
                this.utils.writeLog(err);
                try {
                    
                    user = await this.post.getPosts(username);
                } catch (err) {

                }
            }
        }
        /** GET STORIES */
        if (actions.indexOf("stories") !== -1) {
            this.utils.writeLog("[analyseUser] Getting stories for " + username);
            try {
                user = await this.stories.getActiveStories(username);
                console.log("STORY");
            } catch (err) {
                this.utils.writeLog("[analyseUser] error in get active stories", err.toString().substr(0, 100));
            }
            console.log("[analyseUser] Finished fetching stories for " + username);
        }
        
        /** GET FOLLOWERS */
        if (actions.indexOf("followers") !== -1) {
            this.utils.writeLog("[analyseUser] Getting followers for " + username);
            try {
                user = await this.followers.getFollowers(username);

            } catch (err) {
                this.utils.writeLog("[analyseUser] Error in get followers");
                this.utils.writeLog(err);
            }
            console.log("[analyseUser] Finished getting followers for " + username);
        }

        /** GET ARCHIVED STORIES IF LOGGED IN */
        if (username === this.utils.CONFIG.username) {
            user = await this.stories.getAllArchiviedStoriesWithVoters(username);
        }

        /** DO ANALYSIS ON PROFILE AND FETCH USERS INFO */
        if (actions.indexOf("engagement") !== -1) {
            this.utils.writeLog("[analyseUser] Calculating " + username + " engagement for posts...");
            user = await this.stats.calculatePostsEngagement(username);
            await this.stats.exportPostsEngagementToCSV(username);  
            await this.post.extractAllTagsAndMentionsFromPostsText(username);
            console.log("[analyseUser] Finished calculating engagement and mentions and hashtags for " + username);
        }
        if (actions.indexOf("userinfo") !== -1) {
            this.utils.writeLog("[analyseUser] Getting all user info for " + username);
            let totalFetched = await this.profile.populateAllUserInfo(username);
            this.utils.writeLog("[analyseUser] Fetched " + totalFetched + " user info for " + username);
            console.log("[analyseUser] Fetched " + totalFetched + " user info for " + username);
        }

        if (actions.indexOf("textinfo") !== -1) {
            this.utils.writeLog("[analyseUser] Extracting tags and mentions for " + username);
            await this.post.extractAllTagsAndMentionsFromPostsText(username);
        }
        
        if (actions.indexOf("contacts") !== -1) {
            this.utils.writeLog("[analyseUser] Doing most engaged users analysis on " + username );
            user = await this.stats.calculateEngagementAndExportContactInfoToCSV(username);
            console.log("[analyseUser] Finished exporting contacts for " + username);
        }

        if (actions.indexOf("rek") !== -1) {
            this.utils.writeLog("[analyseUser] Doing image analysis for " + username + " for all media...");
            let count = await this.image_analysis.getPostsImagesAnalysis(username);
            this.utils.writeLog("[analyseUser] Analysed " + count + " images for " + username);
            let labels = await this.stats.calculateMostEngagingLabels(username);
            if (Object.keys(labels).indexOf("labels") !== -1) {
                this.utils.writeLog("[analyseUser] Extracted " + Object.keys(labels['labels']).length + " labels for " + username);
            } else {
                this.utils.writeLog("[analyseUser] Processed labels for " + username);
            }
            console.log("[analyseUser] Finished doing rek and extracting labels for " + username);
        }
        if (actions.indexOf("labels") !== -1) {
            let labels = await this.stats.calculateMostEngagingLabels(username);
            this.utils.writeLog("[analyseUser] Processed most engaging labels for " + username);
            console.log("[analyseUser] Finished extracting just labels for " + username);
        }
        user = await this.utils.createOrUpdate(User, "username", user.username, user);
        return user;
    }







}

