# This repository is a social listening and analysis tool for Instagram.
# To run:
- `npm install`
- create a config file similar to `config_copy.json`, no login info needed if not posting or getting user info (like number of followers, contact details)
- make sure you have an instance of mongodb running and a db called social.
- make sure there are aws credentials loaded either in a specific aws_creds.json file or in the env (only needed for AWS rekognition)
# Some actions:
* `npm start posts official_sslazio` <- gets all the posts of Lazio with likers until `"fetching": {"max_likers": 3000, "max_comments": 3000}`, includes retry, uses proxies.
* `npm start userinfo official_sslazio` <- gets all user info of people who interacted with Lazio profile, requires login in the `expandables` part of the config.
* `npm start explore (#)sslazio` <- saves all posts for a tag , hash not needed
* `npm start engagement official_sslazio` <- calculates engagement, hashtags and mentions for a profile
* `npm start rek official_sslazio` <- applies AWS rekognition to images of a profiles (includes carousel)
* `npm start posters (#)sslazio` <- extracts unique posters for an hashtags,  hash not needed
* `npm start tagengagement (#)sslazio` <- extracts engagement for a specific hashtags,  hash not needed
* `npm start contacts official_sslazio` <- extracts users who have contact details and interacted with a profile, also calculates engagement (#of likes and comments made to Lazio, per user)
* `npm start labels official_sslazio` <- after `rek` has been performed extracts image items for a specific user
* `npm start followers official_sslazio` <- fetches followers, requires login, max 20k per day, haven't tested in a bit
* `npm start stories official_sslazio` <- saves stories info, if the user logged in runs `stories`, it will also pull all the saved stories in the archive. 
* `npm start bulkuserinfo` <- tries to fetch contact details of all users in db, requires login.

