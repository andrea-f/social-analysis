

import { CONFIG, ANALYSIS_CONFIG } from './config.interface';
import fs from 'fs';
import { reject } from 'bluebird';
import { User, Status, IStatus, Post, IPost } from './db-schema';
import mongoose from 'mongoose';

let logFileName = "logs/log_" + new Date().toISOString() + ".log";
console.log("Log file: " + logFileName);
let stream = fs.createWriteStream(logFileName, {flags:'a'});

export class Utils {

    constructor () {
        
        this.CONFIG = require('./config.json');
        this.ANALYSIS_CONFIG = require('./profiles_to_analyse.config.json');
        this.totalQueries = 0;
        this.proxies = <any>[];
        this.env = this.CONFIG.env;
        this.currentProxyIndex = 0;
        this.maxRetries = 3;
        this.currentRetries  = 0;
        this.userAgents = <any>[];
        this.DEFAULT = "default";
        this.STATUSES = ['posts', 'followers', 'story', 'hashtags'];
        this.totalErrors = 0;
        this.SLEEP_TIMEOUT = this.CONFIG.timeout;
    }
    STATUSES: Array<string>;
    userAgents: Array<string>;
    CONFIG: CONFIG;
    ANALYSIS_CONFIG: ANALYSIS_CONFIG;
    currentProxyIndex: number;
    totalErrors: number;
    totalQueries: number;
    maxRetries: number;
    currentRetries: number;
    
    proxies: Array<{
        ip_address: string,
        port: number
    }>;
    db: any;
    env: string;
    DEFAULT: string;

    SLEEP_TIMEOUT: number;

    /**
     * Retrieves an url (if any) from a string.
     * @param text information to parse to find a url, string.
     */
    extractUrlFromText (text: string) {
        let splitted = text.split("\n");
        //console.log(splitted);
        let url = "";
        let cookie = "";
        for (let i =0; i < splitted.length; i++) {
            if (splitted[i].toLowerCase().indexOf("location") !== -1) {
                url = splitted[i].split(" ")[1];
            }
            if (splitted[i].toLowerCase().indexOf("cookie") !== -1) {
                cookie = splitted[i].split(" ")[1].replace(";",'');
                //console.log(urlString);
                //Set-Cookie: s=fgcolg6pp9ot32skp7e2t1jhh6; path=/
            }
        }
        return {
            url: url,
            cookie: cookie
        }
    }



    // UTILS

    /**
     * Writes to log file
     * @param text text about log information.
     * @param item optional object to save in log.
     * @param item2 another optional object to save in log.
     */
    async writeLog (text: string, item: object = {}, item2: object = {}) {
        try {
            let message = Array<string>();
            if (typeof text === 'string') {
                message.push(text);
            } else if (typeof text === 'object') {
                message.push(JSON.stringify(text, null, 4));
            }
            
            if (Object.keys(item).length > 0) {
                message.push(JSON.stringify(item, null, 4));
            }
            if (Object.keys(item2).length > 0) {
                message.push(JSON.stringify(item, null, 4));
            }
            //this.stream.end();
            stream.write("\n"+message.join("\n"));
        } catch (err) {
            this.writeLog("error in writing to log", err);
        }
    }


    /**
     * Performs a request from node using curl.
     * @param query process to run from node, string.
     * @param commands curl specific options, list.
     * @param showProxy flag to show the proxy being used, if any.
     */
    async getAnonymousQuery (query: string, commandsInput: Array<any>, useProxy: boolean = true, showProxy: boolean = false, retries: number = 0) {
        try {
            const spawn = require('child_process').spawn;
            let commands = <any>[];
            for (let c of commandsInput) {
                commands.push(c);
            }
            const childProcess = spawn(query, commands);
            
            let main = this;
            let uaString = await this.getUserAgentString();
            commands.push('-H', 'User-Agent: ' + uaString);
            if (this.CONFIG.use_proxy === true && useProxy === true) {  
                try {
                    let count = 0;
                    //this.writeLog("Env var: " + this.env);
                    while (this.proxies.length < 2) {

                        switch (this.env) {
                            case "pi":
                                await this.getProxies();
                                break;
                            case "aws":
                            case "local":
                                await this.getProxiesWorkingWithAWS();
                                break;
                        }
                        count += 1;
                        if (count === 10) {
                            break;
                        }
                    }        
                    let currentProxyIndex = Math.floor((Math.random() * this.proxies.length - 2) + 1);
                    while (currentProxyIndex > (this.proxies.length -1 )) {
                        currentProxyIndex = Math.floor((Math.random() * this.proxies.length - 2) + 1);
                    }
                    if (showProxy === true) {
                        this.writeLog("Using proxy: " + this.proxies[currentProxyIndex].ip_address + ":" + this.proxies[currentProxyIndex].port + " (" + currentProxyIndex + ")");
                    }
                    //
                    commands.push('--proxy', this.proxies[currentProxyIndex].ip_address + ':' + this.proxies[currentProxyIndex].port);
                    // TODO: change headers and wait
                    //await this.delay(this.SLEEP_TIMEOUT);
                    this.currentProxyIndex = currentProxyIndex;

                } catch (err) {
                    this.writeLog("error in getting proxy");
                    this.writeLog(err);
                    console.log(err);
                }
            }
            //commands.unshift('--max-time', 10);
            //commands.unshift('--connect-timeout', 10);
            console.log(commands);
            return new Promise((resolve, reject) => {
                let stdout = '';
                childProcess.stdout.on('data', (data:any) => {
                    stdout += data;
                });
    
                childProcess.on('error', function (error:any) {
                    console.log("Raised errror", error);
                    reject({code: 1, error: error});
                });
    
                childProcess.on('close', async function (code:any) {
                    if (code > 0) {
                        main.totalErrors += 1;
                        if (code === 6 || code === 60) {
                            resolve(undefined);
                        }
                        if (main.totalErrors < main.proxies.length) {
                            let removed = main.proxies[main.currentProxyIndex];
                            main.proxies.splice(main.currentProxyIndex, 1);
                            // Remove proxy which has raised the error
                            main.writeLog(JSON.stringify({error: "code is:  " + code + "  , total proxies:" +main.proxies.length, removed: removed}));
                            // TODO: Extract this in a setting
                            await main.getAnonymousQuery(query, commandsInput, true, true);
                        } else {
                            // We have failed too many times, quit
                            reject({code: code, error: 'Command failed with code:::: ' + code});
                        }
                    } else {
                        let output;
                        try {
                            //console.log("output: ", stdout);
                            output = JSON.parse(stdout);
                            
                        } catch (err) {
                            output = stdout;
                            if (output.length === 0) {
                                // Removed proxy which failed getting the proper response
                                main.proxies.splice(main.currentProxyIndex, 1);
                                console.log("Error in query:" + commandsInput[0]);
                                resolve(undefined);
                            }
                        }
                        if (output.status && output.status === 'fail') {
                            main.writeLog("ERROR IN QUERY:::");
                            console.log(output);
                            //main.writeLog(commands.join(' '));
                            try {
                                main.writeLog(JSON.stringify(output));
                            } catch (err) {
                                main.writeLog(output);
                            }
                            main.totalErrors += 1;
                            //console.log(main.totalErrors , main.proxies.length, commands[0]);
                            main.writeLog("Waiting " + main.CONFIG.timeout_anon + " seconds...");
                            await main.delay(main.CONFIG.timeout_anon);
                            await main.getAnonymousQuery(query, commandsInput, true, true);
                            
                        }
                        resolve(output);
                    }
                });
            });
        } catch (err) {
            this.writeLog('Error in getanonymousquery!');
            this.writeLog(err);
            reject(err);
        }
        
    }



    /**
     * Retrieves a list of proxies and populates CONFIG proxies array.
     */
    async getProxies () {
        //https://www.sslproxies.org/
        //https://getproxylist.com/#the-api
        //https://www.proxy-list.download/api/v1
        // https://github.com/snail007/goproxy
        console.log("Using get proxies");
        let query = "curl";
        let commands = [
            'https://www.proxy-list.download/api/v1/get?type=http&anon=elite&country=CA'
        ];
        let data = <any>await this.getAnonymousQuery(query, commands, false);
        
        let proxyList = data.toString().split('\n');
        for (let proxy of proxyList) {
            try {
                if (proxy.length > 0) {
                    let ip = proxy.split(':')[0];
                    let port = proxy.split(':')[1].replace('\r','');
                    this.proxies.push({
                        "ip_address": ip,
                        "port": port
                    });
                }
            } catch (err) {
                this.writeLog("Error in getting proxies: ");
                this.writeLog(err);
            }
            
        }
    }

    /**
     * Retrieves proxies which work with AWS.
     */
    async getProxiesWorkingWithAWS () {
        console.log("Using get proxy with aws");
        let query = 'curl';
        let commands = [
            'https://proxy11.com/api/proxy.json?key=MTA0Mw.XniA8g.kcgNckkS_P1L3u8XCITYSANwA3o'
        ];
        let result = <any>await this.getAnonymousQuery(query, commands, false);
        for (let proxy of result.data) {
            
            this.proxies.push({
                ip_address: proxy.ip,
                port: proxy.port
            });
        }
        
       
    }

    /**
     * Returns a list of instagram compatible user agent strings
     */
    async getUserAgentString () {
        //Mozilla/5.0 (Windows NT 10.0.17134.165; osmeta 10.3.6848) AppleWebKit/602.1.1 (KHTML, like Gecko) Version/9.0 Safari/602.1.1 osmeta/10.3.6848 Build/6848 Instagram 41.1.1.14.90 (Spin SP513-52N 0000000000000000; osmeta/Windows 10_3_6848; en_US; en-US; scale=1.50; gamut=normal; 1920x972)
        //var iPhoneUserAgent = 'Instagram 19.0.0.27.91 (iPhone6,1; iPhone OS 9_3_1; en_US; en; scale=2.00; gamut=normal; 640x1136) AppleWebKit/420+';
        // https://www.handsetdetection.com/device-detection-database/apps/instagram/
        if (this.userAgents.length === 0) {
            let userAgents = await fs.readFileSync("instagram_user_agents.txt", 'utf8').split('\n');
            for (let ua of userAgents) {
                this.userAgents.push(ua.replace('\r',''));
            }
        }
        let index = Math.floor((Math.random() * this.userAgents.length - 2) + 1);
        let randomUa = this.userAgents[index];
        return randomUa;
    }

    /**
     * Checks and fetches if there is already an existing status.
     * @param data id and actionType strings, object.
     */
    async getStatus (data: object) {
        let status = <IStatus>await Status.findOne(data).exec();
        return status;
    }

    /**
     * Writes file to local fs.
     * @param fileName string with location on fs of file
     * @param data information to save in a file
     * @param log sets whether to print or not to the console.
     */
    async writeFile (fileName: string, data: any, log: boolean = false) {
        if (log) {
            this.writeLog("Saving file " + fileName);
        }
        await fs.writeFileSync(fileName, data);
    }

    /**
     * Converts timestamp to date.
     * @param statsReelItem timestamp to convert to normal date.
     */
    timestampToDate (statsReelItem: number) {
        //statsReelItem = statsReelItem * 1000;
        let reelDate = new Date(0); // The 0 there is the key, which sets the date to the epoch
        reelDate.setUTCSeconds(statsReelItem).toLocaleString();
        let reelDateString = reelDate.toISOString(); 
        //this.writeLog(reelDateString); 
        return reelDateString;
    }

    /**
     * Connects to mongodb instance.
     */
    connectToDB (environment: number = 0) {
        let main = this;
        let connectOptions = {
            poolSize: 50,
            useNewUrlParser: true, 
            useFindAndModify: false,
            useUnifiedTopology: true,
            socketTimeoutMS: 4000000,
            keepAlive: true,
            //reconnectTries: 30000
        };
        const connect = async function () {
            if (environment === 1) {
                mongoose.connect(
                    main.CONFIG.mongo_uri,
                    connectOptions
                );
                main.env = "pi";
                // TODO: Add creating ssh tunnel on connection error and retry
                // TODO: Handle connection on error scenario
                mongoose.connection.on('open', async () => {
                    console.info('Connected to Mongo REMOTE.');
                    //User.createIndex({"interactions.timestamp": 1, "interactions.score": 1, "interactions.engagement_with": 1});
                    if (main.CONFIG.env !== 'default') {
                        main.env = main.CONFIG.env;
                    }
                    main.setDbObject(mongoose.connection.db);
                    
                    return mongoose.connection.db;
                });
            } else {
                console.log("Connecting to: " + main.CONFIG.mongo_uri_local);
                
                mongoose.connect(
                    main.CONFIG.mongo_uri_local,
                    connectOptions
                );
                main.env = "aws";
                mongoose.connection.on('open', async () => {
                    console.info('Connected to Mongo LOCAL.');
                    //User.createIndex({"interactions.timestamp": 1, "interactions.score": 1, "interactions.engagement_with": 1});
                    if (main.CONFIG.env !== 'default') {
                        main.env = main.CONFIG.env;
                    }
                    main.setDbObject(mongoose.connection.db);
                    return mongoose.connection.db;
                });
                
            }
        }
        connect();
        mongoose.connection.on('error', (err: any) => {
            console.error(err);
            console.log("[connectToDB] Reconnecting");
            connect();
        });
        mongoose.connection.on('disconnected', () => {
            setTimeout(connect, 5000);
        });
    }

    async setDbObject (dbObject: any) {
        this.db = dbObject;
        this.writeLog("db object set!");
    }

    /**
     * Adds a fixed delay.
     * @param s Time to sleep
     */
    async delay (s: number) {
        let ms = s * 1000;
        //this.writeLog("Sleeping for "+this.SLEEP_TIMEOUT + " seconds...");
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    /**
     * Find in db.
     * @param model DB instance to operate on.
     * @param searchKey Identifier for the key to search in the model.
     * @param searchValue Identifier for the value to search the key with.
     */
    async find (model: any, searchKey: string, searchValue: string) {
        //this.writeLog("Checking for " + searchKey + ": " + searchValue);
        try {
            let found = await model.findOne()
            .where(searchKey).in([searchValue])
            .exec();
            return found;
        } catch (err) {
            this.writeLog("ERROR" + err);
            return null;
        }
        
    }

    /**
     * Finds docs based on complex queries.
     * @param model Db schema to use for search.
     * @param searchObject Query to search for in db.
     */
    async complexFind (model: any, searchObject: object) {
        try {
            let found = await model.find(searchObject).exec();
            return found;
        } catch (err) {
            this.writeLog("Error in complex find", err);
        }
    }

    /**
     * Either creates or updates a record in the db.
     * @param model Schema model to search in
     * @param searchKey Key in the model to search for
     * @param searchValue Identifier to search record for
     * @param updateData New or updated data to insert
     */
    async createOrUpdate (model: any, searchKey: any, searchValue: string, updateData: object) {
        //this.writeLog("Updating or creating: " + searchKey + ": " + searchValue);
        let searchObject = {};
        if (typeof searchKey === 'string') {
            searchObject[searchKey] = searchValue;
        } else if (typeof searchKey === 'object') {
            searchObject = searchKey;
        }
        let realUpdateData = {};
        for(let prop in updateData) {
            if(updateData[prop] && typeof updateData[prop] !== 'function' && prop !== 'id') {
                
                if ( typeof updateData[prop] === 'number' ) {
                    realUpdateData[prop] = updateData[prop];
                }
                // TODO: remove hack
                if ( (prop === 'info' || prop === 'engagement') && (typeof model === typeof User) ) {
                    try {
                        updateData[prop].length = 1;
                    } catch (err) {
                        //realUpdateData[prop] = updateData[prop];
                    }
                }
                 
                if (updateData[prop]['length'] && updateData[prop].length > 0 ) {
                    realUpdateData[prop] = updateData[prop];
                }   
            }
        }
        try {
            let saved = await model.findOneAndUpdate(
                searchObject,
                realUpdateData,
                { upsert: true, new: true }
            );
            return saved;
        } catch (err) {
            //this.writeLog(searchKey, {"search_value":searchValue}, updateData);
            //this.writeLog("Error in findOneAndUpdate: " + err);
            //this.writeLog(realUpdateData)
            return null;
        }
    }

    /**
     * Tries to recover from error of "please wait" by switching logged in user.
     * @param err String with error info.
     */
    async checkErrorAndRecover (err: any) {
        //this.writeLog("Before: currentAccountIndex: " + this.currentAccountIndex + ", CONFIG.expandables.length: " + this.CONFIG.expandables.length + ", currentRetries: " + this.currentRetries + ", maxRetries: " + this.maxRetries);
        let solution = true;
        if (err.toString().indexOf("Bad string") !== -1) {
            this.writeLog("ERROR: Bad Syntax");
        } else if (err.toString().indexOf("Please wait") !== -1) {
            this.writeLog("ERROR: Blocked by Instagram");
            // switch user
            solution = false;
        }
        return solution;
    }


    /**
     * Sets DB information on status of analysis.
     * @param data Contains information on what to save, object.
     * @param actionType Specific type of status setting: story or followers or posts.
     */
    async setStatus (data: object, actionType: string) {
        let statusObject = new Status();
        statusObject.next_max_id = data['nextMaxId'];
        statusObject.user_id = data['id'];
        if (this.STATUSES.indexOf(actionType) !== -1) {
            statusObject.action_type = actionType;
            statusObject.save();
            //this.writeLog("Created new status object with maxid: " + statusObject.next_max_id);
        } else {
            this.writeLog("Action type unknown: " + actionType );
        }
        
    }

    /**
     * Use the next_max_id property to resume getting info.
     * @param data Instance of request object.
     * @param actionType Whether its followers, story.
     */
    async checkToResumeFetchingFromAPoint (data: any, actionType: string) {
        //this.writeLog("checking to continue");
        let state = JSON.parse(data.serialize()); // You can serialize feed state to have an ability to continue get next pages.
        
        let searchObject = {
            "$and": [
                { "user_id": data['id'] },
                { "action_type": actionType }
            ]
        };
        let foundStatus = await this.complexFind(Status, searchObject);
        if (foundStatus.length > 0) {
            if ( state['nextMaxId'] && ( parseInt(state['nextMaxId'].split('_')[0]) >  parseInt(foundStatus[0]['next_max_id'].split('_')[0]) ) ) {
                state['nextMaxId'] = foundStatus[0]['next_max_id'];
                // TODO: Improve update mechanism, its counting double
                //await this.removeLoadedStatus(foundStatus[0]._id);
                let updatedStatus = await this.createOrUpdate(Status, searchObject, "", state);

                this.writeLog("Updated next max id: " + updatedStatus['next_max_id']);
            }
            
        } else {
            if (state['nextMaxId']) {
                await this.setStatus(data, actionType);
            } else {
                this.writeLog("No max id found", state);
            }
        }
        data.deserialize(JSON.stringify(state));
        return data;
    }

    /**
     * Create the next_max_id property using the latest saved post and the user id to resume getting info.
     * @param data Instance of request object.
     */
    async createMaxId (userFeed: any) {
        let state = JSON.parse(userFeed.serialize());
        let searchObject = { 
            "user_id": userFeed['id'] 
        };
        let specificFields = {
            "post_id": -1
        };
        let found = null;
        this.writeLog("Getting max id for " + userFeed['id']);
        try {
            found = await Post.find(searchObject, specificFields).sort(specificFields).setOptions({allowDiskUse: true}).limit(1).exec();
            this.writeLog(JSON.stringify(found));
        } catch (err) {
            this.writeLog("Error in finding latest post");
            this.writeLog(JSON.stringify(err, null, 4));
        }
        if (found && found.length >= 1) {
            state['nextMaxId'] = found[0]['post_id'] + "_" + userFeed['id'];
            this.writeLog("Updated next max id: " + state['nextMaxId']);
        } else {
            this.writeLog("No max id found to update");
        }
        userFeed.deserialize(JSON.stringify(state));
        return userFeed;
    }

    async loadImage (imageFilePath: string) {
        const fs = require('fs');
        let imageBase64 = await fs.readFileSync(imageFilePath, { encoding: 'base64' });
        const imageBuffer = Buffer.from(imageBase64, 'base64');
        return imageBuffer;
    }

    async downloadImageMedia (postData: IPost) {
        let mediaData = <any>[];
        for (let x=0; x <= postData.media.length; x++) {
            try {
                let imageBase64 = await fs.readFileSync(postData.media[x], { encoding: 'base64' });
                this.writeLog(
                "[downloadMedia] Downloaded image: " + postData.media[x] + " from " + postData.username + " with size: " + imageBase64.length
                );
                if (mediaData && mediaData.length > 0) {
                    mediaData.push([postData.media[x], imageBase64]);
                } else {
                    mediaData = [[postData.media[x], imageBase64]];
                }

            } catch (err) {
                continue;
            }
        }
        return mediaData;
    }

    isItTimeToPost (datetime: string, maxTimeDifferenceInMinutes: number = 5, timeOffest: number = 7200000) {
        let timestampToPost = Date.parse(datetime) - timeOffest;// .toLocaleString().getTime();
        let timeNow = Date.now();
        let timeDifference = (Math.abs(timestampToPost - timeNow) / 1000) / 60;
        console.log("Parsed datetime: " + timestampToPost + " now: " + timeNow + " difference: " + timeDifference);
        if (timeDifference < maxTimeDifferenceInMinutes) {
            return true;
        } else {
            return false;
        }
        
    }
}