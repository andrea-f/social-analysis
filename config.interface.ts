export interface CONFIG {
    username: string,
    password: string,
    mongo_uri: string,
    mongo_uri_local: string,
    login: boolean,
    env: string,
    use_proxy: boolean,
    expandables: Array<{
        username: string,
        password: string
    }>,
    to_analyse: Array<string>,
    timeout: number,
    timeout_anon: number,
    proxies: Array<{
        ip_address: string,
        port: number
    }>,
    min_feature_confidence: number,
    min_feature_confidence_celebrities: number,
    min_feature_occurrences: number,
    posting_users: Array<{
        username: string,
        password: string
    }>
};


export interface ANALYSIS_CONFIG {
    profiles: {
        username:  {
            analysis: {
                limits: {
                    max_posts: number,
                    max_posts_per_tag: number,
                    max_interactors_pool: number,
                    max_interactors_batch_size: number,
                    user_info_batch_size: number,
                    max_followers: number
                },
                limit_interactors: boolean,
                get_userinfo_while_calculating_engagement: boolean,
                date_from: string,
                date_to: string
            },
            order: number,
            fetching: {
                max_likers: number,
                max_comments: number,
                ignore_max_id: boolean
            }
        }
    }
};