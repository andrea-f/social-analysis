
import {Document, Schema, Model, model} from 'mongoose';

 
//const genders = ['male', 'female'] as const;


 
export const UserSchema: Schema = new Schema (
    {
        user_id: {type: Number, required: true, unique: true},
        username: { type: String, required: true, unique: true, index: true },
        profile_pic: {type: String},
        full_name: {type: String},
        email: {type: String},
        phone: {type: Number},
        followers: {type: Array},
        media: {type: Array},
        comments: <any>[],
        following: {type: Array},
        stories: {type: Array},
        gender: {type: String},
        liked: [],
        likers: [],
        scrape_timings_stories_started: {type:Number},
        scrape_timings_stories_finished: {type:Number},
        scrape_timings_posts_started: {type:Number},
        scrape_timings_posts_finished: {type:Number},
        scrape_timings_followers_started: {type:Number},
        scrape_timings_followers_finished: {type:Number},
        scrape_timings_following_started: {type:Number},
        scrape_timings_following_finished: {type:Number},
        posts: <any>[],
        info: <any>{},
        votes_given: <any>[],
        reels_viewed: {type: Array},
        follower_count: {type:Number},
        following_count: {type:Number},
        interacted_with: <any>[],
        interacted_with_engagement: <any>[],
        engagement: <any>{},
        media_count: {type: Number},
        interactions: <object>[],
        image_labels: {type: Array},
        mentions: {type: Array},
        hashtags: {type: Array}, 
        follower_count_history: {type: Array},
        posted: {type: Array}
        //interactions: { type: Array, required: true, index: true }
    },
    {_id: true, timestamps: true }
);

// Commented since already created.
//UserSchema.index({"username": 1, "interactions.score": 1, "interactions.timestamp": 1, "interactions.engagement_with": 1}, {unique: true});
//UserSchema.index({"interactions.score": 1, "interactions.timestamp": 1, "interactions.engagement_with": 1});
//UserSchema.index({"interactions.timestamp": 1, "interactions.engagement_with": 1});
//UserSchema.index({"interactions.timestamp": 1, "interactions.engagement_with": 1});
export const HashtagSchema: Schema = new Schema (
  {
    name: { type: String, required: true, index: true, unique: true },
    tag_id: {type: Number, unique: true},
    posts: {type: Array},
    usernames: <any>{}, // 
    stats: {type: Array},
    total_posts: {type: Array},
    timestamp: Date,
    info: <any>{},
    description: {type: String},
    profile_pic: {type: String},
    related_tags: {type: Array},
    posters: {type: Array},
    engagement: {type: Object}
  },
  { _id: true, timestamps: true }
);

export const FollowersSchema: Schema = new Schema (
  {
    username: { type: String, required: true, index: true },
    followers: {type: Array, required: true},
    batch_hash: {type: String, required: true}, // 
    scan_dates: {type: Array}
  },
  { _id: true, timestamps: true }
);

// A story is a collection of reels
export const StorySchema: Schema = new Schema (
  {
    username: { type: String, required: true, index: true },
    user_id: {type: Number, required: true},
    story_id: {type: String, required: true, unique: true},
    reels: <any>[],
    info: <any>{},
    timestamp: {type: Date}
  },
  { _id: true, timestamps: true }
)

export const ReelSchema: Schema = new Schema (
    {
      reel_id: {type: String, required: true, unique: true},
      info: <any>{},
      timestamp: {type: Number},
      viewers_count: {type: Number},
      viewers: <any>[],
      voters: <any>[]
    },
    { _id: true, timestamps: true }
)

export const PostSchema: Schema = new Schema (
    {
        post_id: {type: String, required: true, unique: true},
        info: <any>{},
        timestamp: {type: Number},
        media: <any>[],
        location: <any>{},
        user_id: {type: Number, required: true},
        username: {type: String, required: true, index: true},
        like_count: {type: Number},
        comment_count: {type: Number},
        text: {type: String},
        likers: <any>[],
        comments: <any>[],
        image_details: <any>[],
        typename: {type: String},
        engagement: {type: Number},
        image_analysis: <object>[],
        hashtags: <any>[],
        mentions: <any>[],
        shortcode: {type: String},
        media_data: {type: Array}

    },
    { _id: true, timestamps: true }
)

export const PostedSchema: Schema = new Schema (
  {
      post_id: {type: String, required: true, unique: true},
      info: <any>{},
      timestamp: {type: Number},
      media: <any>[],
      location: <any>{},
      user_id: {type: Number, required: true},
      username: {type: String, required: true, index: true},
      like_count: {type: Number},
      comment_count: {type: Number},
      text: {type: String},
      likers: <any>[],
      comments: <any>[],
      image_details: <any>[],
      typename: {type: String},
      engagement: {type: Number},
      image_analysis: <object>[],
      hashtags: <any>[],
      mentions: <any>[],
      shortcode: {type: String, required: true, unique: true, index: true}
  },
  { _id: true, timestamps: true }
)
//PostSchema.index({"username": 1});
//PostSchema.index({"username": 1, "engagement": 1});
//PostSchema.index({"username": 1, "timestamp": 1});

export const StatusSchema: Schema = new Schema (
  {
    action_type: {type: String, required: true},
    next_max_id: {type: String},
    user_id: {type: Number, required: true},
    timestamp: {type: Number}
  },
  { _id: true, timestamps: true }
)

export interface IStatus extends Document {
  user_id: number;
  next_max_id: string;
  action_type: string;
  timestamp: number;
}

export interface IUser extends Document {
  user_id: number;
  username: string;
  full_name: string;
  profile_pic: string;
  following: Array<any>;
  followers: Array<any>;
  stories: Array<any>;
  votes_given: Array<any>;
  liked: Array<any>;
  posts: Array<any>;
  comments: Array<any>;
  email: string;
  phone: number;
  media: Array<any>;
  reels_viewed: Array<any>;
  scrape_timings_stories_started: number;
  scrape_timings_stories_finished: number;
  scrape_timings_posts_started: number;
  scrape_timings_posts_finished: number;
  scrape_timings_followers_started: number;
  scrape_timings_followers_finished: number;
  scrape_timings_following_started: number;
  scrape_timings_following_finished: number;
  info: object;
  follower_count: number;
  following_count: number;
  interacted_with: Array<any>;
  interacted_with_engagement: Array<any>;
  engagement: object;
  media_count: number;
  interactions: Array<object>;
  image_labels: Array<object>;
  follower_count_history: Array<object>;
  posted: Array<object>;
}

export interface IVoter extends Document {
  reel_id: string;
  story_id: string;
  poll_id: string;
  vote_option: object;
  vote: number;
  timestamp: number;
}

export interface IPost extends Document {
  post_id: string;
  shortcode: string;
  info: object;
  timestamp: number;
  media: Array<any>;
  location:  Array<any>;
  user_id: number;
  username: string,
  like_count: number,
  comment_count: number,
  likers: Array<any>,
  comments: Array<any>,
  text: string,
  image_details: Array<any>,
  typename: string,
  engagement: number,
  image_analysis: Array<object>,
  hashtags: Array<string>,
  mentions: Array<string>,
  media_data: Array<any>
}

export interface IPosted extends Document {
  post_id: string;
  shortcode: string;
  info: object;
  timestamp: number;
  media: Array<any>;
  location:  Array<any>;
  user_id: number;
  username: string,
  like_count: number,
  comment_count: number,
  likers: Array<any>,
  comments: Array<any>,
  text: string,
  image_details: Array<any>,
  typename: string,
  engagement: number,
  image_analysis: Array<object>,
  hashtags: Array<string>,
  mentions: Array<string>
}

export interface IStory extends Document {
  story_id: string;
  info: object;
  timestamp: Date;
  reels: Array<IReel>;
  username: string;
  user_id: number;
}

export interface IHashtag extends Document {
  tag_id: string;
  name: string;
  total_posts: Array<object>;
  info: object;
  timestamp: Date;
  usernames: object;
  posts: Array<number>;
  stats: object;
  description: string;
  profile_pic: string;
  related_tags: Array<string>;
  posters: Array<object>;
  engagement: object;
}

export interface IReel extends Document {
  reel_id: string;
  info: object;
  timestamp: number;
  viewers_count: number;
  viewers: Array<any>;
  voters: Array<IVoter>;
}
 
export interface IFollowers extends Document {
  username: string;
  followers: Array<string>;
  batch_hash: string;
  scan_dates: Array<number>;
}



export const User: Model<IUser> = model<IUser>('User', UserSchema);


export const Voter: Model<IVoter> = model<IVoter>('Voter', UserSchema);
export const Reel: Model<IReel> = model<IReel>('Reel', ReelSchema);
export const Story: Model<IStory> = model<IStory>('Story', StorySchema);
export const Post: Model<IPost> = model<IPost>('Post', PostSchema);
export const Followers: Model<IFollowers> = model<IFollowers>('Follower', FollowersSchema);
export const Status: Model<IStatus> = model<IStatus>('Status', StatusSchema);
export const Hashtag: Model<IHashtag> = model<IHashtag>('Hashtag', HashtagSchema);
export const Posted: Model<IPosted> = model<IPosted>('Posted', PostedSchema);