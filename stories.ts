import { User, Story, IUser, Reel, IStory, IReel, Voter } from './db-schema';
import { CONFIG, ANALYSIS_CONFIG } from './config.interface';
import { Utils } from './utils';
import { IgApiClient, ReelsMediaFeed } from 'instagram-private-api';



export class Stories {


    constructor () {
        this.utils = new Utils;
        this.IG = new IgApiClient();
        this.CONFIG = this.utils.CONFIG;
        this.ANALYSIS_CONFIG = this.utils.ANALYSIS_CONFIG;
    }

    utils: Utils;
    IG: IgApiClient;
    CONFIG: CONFIG;
    ANALYSIS_CONFIG: ANALYSIS_CONFIG;
    // STORIES

    /**
     * Gets all voters for a reel.
     * @param storyId identifier string for story with poll ID.
     * @param pollId identifier string for poll in a reel.
     */
    async getVoters (storyId: string, pollId: string) {
        let votersFeed = this.IG.feed.storyPollVoters(storyId, pollId);
        let wholeResponse = await votersFeed.request();
        let totalVoters = wholeResponse['voter_info']['voters'];
        while (wholeResponse['voter_info']['more_available'] === true) {
            await this.utils.delay(this.utils.SLEEP_TIMEOUT);
            wholeResponse = await votersFeed.request();
            this.utils.writeLog("Adding " + wholeResponse['voter_info']['voters'].length + " voters")
            totalVoters = totalVoters.concat(wholeResponse['voter_info']['voters']);
        }
        this.utils.writeLog("Fetched " + totalVoters.length + " total voters for storyId: " + storyId + " with pollId: " + pollId);
        return totalVoters;
    }

    /**
     * Retrieves archived stories for logged in user.
     */
    async getArchivedStories () {
        // modified saved.feed.js
        this.utils.writeLog("Getting archived stories...");
        const archivedStoriesFeed = this.IG.feed.saved();
        const wholeResponse = await archivedStoriesFeed.request();
        let items = wholeResponse.items;
        this.utils.writeLog("Found "+ items.length +" archived stories...");
        return items;
    }

    /**
     * Creates DB record for currently active story reels.
     * @param activeStoriesFeed IG list of reels published under 24 hours ago, array.
     * @param person IUser instance of profile being analysed, IUser.
     * @param timestamp Moment of when the story is getting analysed, number.
     */
    async createActiveStoryRecord (activeStoriesFeed: ReelsMediaFeed, person: IUser, timestamp: number) {
        let story = new Story();
        story.story_id = activeStoriesFeed.userIds[0] + "-" + timestamp;
        story.username = person.username;
        story.user_id = person.user_id;
        
        let foundStory = await this.utils.find(Story, "story_id", story.story_id);
        let saveStory = false;
        let storyExists = false;
        if (foundStory && Object.keys(foundStory.info).length > 1) {
            story = foundStory;
            storyExists = true;
            saveStory = true;
            this.utils.writeLog("Story with id: " + story.story_id + " already exists.");
        } else {
            story.info = await activeStoriesFeed.request();
            if (Object.keys(story.info).length > 1) {
                await story.save();
                saveStory = true;
                this.utils.writeLog("Saved story with id: " + story.story_id);
            } else {
                this.utils.writeLog("No active story for: " + person.username);
            }
        }
        return [story, storyExists, saveStory];
    }

    /**
     * Gets public story active if any.
     * @param username Identifier for for the username to fetch stories for.
     * @param userId Identifier / private key for the username.
     */
    async getActiveStories (username: string, userId: number = 0) {
        let person = new User();
        person.username = username;
        let foundUser = await this.utils.find(User, "username", username);
        
        if (foundUser) {
            person = foundUser;
        } else {
            if (userId === 0) {
                this.utils.writeLog("Searching for user id for " + username);
                const igAccount = await this.IG.user.searchExact(username);
                userId = igAccount.pk;
                this.utils.writeLog("Found user id " + userId + " for " + username);
            }
            person.user_id = userId;
        }
        const activeStoriesFeed = this.IG.feed.reelsMedia({
            userIds: [person.user_id]
        });
        let reels = await activeStoriesFeed.items();
        this.utils.writeLog("Fetched " + reels.length + " active reels for " + person.username);
        await this.utils.delay(this.utils.SLEEP_TIMEOUT);
        // Go through available reels
        let c = 0;
        let story;
        let saveStory = false;
        let storyExists = false;
        let storyObject = null;
        for (let item of reels) {
            let reel = new Reel();
            reel.reel_id = item['pk'];
            reel.timestamp = item['taken_at'];
            if (storyObject === null) {
                storyObject = await this.createActiveStoryRecord(activeStoriesFeed, person, reel.timestamp);
            }
            
            story = <IStory>storyObject[0];
            storyExists = <boolean>storyObject[1];
            saveStory = <boolean>storyObject[2];
            let foundReel = await this.utils.find(Reel, "reel_id", reel.reel_id);
            let exists = false;
            if (foundReel) {
                reel = foundReel;
                exists = true;
            }
            this.utils.writeLog("Processing reel: " + reel.reel_id + " exists: " + exists);
            // Get viewers
            if (item['viewers']) {
                try {
                    reel.viewers_count = item['viewer_count'];
                    this.utils.writeLog("Viewers count: " + reel.viewers_count);
                    // Add initial viewers in the story preview (max 5 viewers)
                    let viewersObject = await this.processViewers(reel, item['viewers'], item['viewers'], person);
                    reel = <IReel>viewersObject[0];
                    item['viewers'] = <any[]>viewersObject[1];
                    // Now fetch the next batch of viewers
                    let moreViewers = this.IG.feed.listReelMediaViewers(item['pk']);
                    let wholeResponse = await moreViewers.request();
                    
                    viewersObject = await this.processViewers(reel, wholeResponse.users, item['viewers'], person);
                    reel = <IReel>viewersObject[0];
                    item['viewers'] = <any[]>viewersObject[1];
                    await this.utils.delay(this.utils.SLEEP_TIMEOUT);
                    
                    // Keep fetching viewers as they are available
                    let count = 2;
                    while (wholeResponse.next_max_id) {
                        wholeResponse = await moreViewers.request();
                        this.utils.writeLog("Extracted: " + wholeResponse.users.length + " more viewers");
                        viewersObject = await this.processViewers(reel, wholeResponse.users, item['viewers'], person);
                        reel = <IReel>viewersObject[0];
                        item['viewers'] = <any[]>viewersObject[1];
                        count += 1;
                        this.utils.writeLog(count + " - Total viewers: " + reel.viewers.length);
                        await this.utils.delay(this.utils.SLEEP_TIMEOUT);
                    }
                    this.utils.writeLog(c + " / "+ reels.length +"\nTotal extracted users: "+ item['viewers'].length + " out of " + item["viewer_count"]);
                } catch (err) {
                    this.utils.writeLog("Fetching active stories error");
                    this.utils.writeLog(err);
                }
            }
            reel.info = item;
            let savedReel = await this.utils.createOrUpdate(Reel, "reel_id", reel.reel_id, reel);
            if (!exists) {
                story.reels.push(savedReel);
            } else {
                story.reels[c] = savedReel;
            }
            c += 1;
        }
        if (saveStory && story) {
            let savedStory = await this.utils.createOrUpdate(Story, "story_id", story.story_id, story);
            if (storyExists) {
                // Update story information for the user
                for (let c = 0; c < person.stories.length; c++) {
                    if (person.stories[c]['story_id'] === savedStory.story_id) {
                        //this.utils.writeLog("Found story matching")
                        let storyReels = person.stories[c]['reels'];
                        for (let t = 0; t < storyReels.length; t++) {
                            for (let storyReel of savedStory.reels) {
                                //this.utils.writeLog("Current story reels: " + storyReels[t]['reel_id'] + " new: " + storyReel['reel_id']);
                                if (storyReels[t]['reel_id'] === storyReel['reel_id']) {
                                    //this.utils.writeLog("Story reel viewers: " + person.stories[c]['reels'][t].viewers + " New: "+ storyReel['viewers']);
                                    person.stories[c]['reels'][t].viewers = storyReel['viewers'];
                                    person.stories[c]['reels'][t].viewers_count = storyReel['viewers_count'];
                                }
                            }
                        }
                    }
                }
            } else {
                person.stories.push(story);
            }
        }
        let savedPerson = await this.utils.createOrUpdate(User, "username", person.username, person); 
        if (story) {
            this.utils.writeLog("Saved active stories for " + savedPerson.username + " with " + story.reels.length + " reels.");
        }
        
        return savedPerson;
    }

    /**
     * Creates and updates records for viewers.
     * Returns the reel with all viewers added.
     * @param reel Schema model Reel instance.
     * @param viewers List of new viewers (as returned from the API) to process.
     * @param allViewers List of all viewers for current reel.
     * @param person Schema model User instance whose reel belongs to.
     */
    async processViewers (reel: IReel, viewers: Array<any>, allViewers: Array<any> = [], person: IUser) {
        for (let v = 0; v < viewers.length; v++) {
            let viewerData = viewers[v];
            if (reel.viewers.indexOf(viewerData['username']) === -1) {
                reel.viewers.push(viewerData['username']);
                let viewer = new User();
                viewer.username = viewerData['username'];
                viewer.user_id = viewerData['pk'];
                viewer.full_name = viewerData['full_name']
                viewer.profile_pic = viewerData['profile_pic_url'];
                let viewerFound = await this.utils.find(User, "username", viewer.username);
                if (viewerFound) {
                    viewer = viewerFound;
                }
                if (viewer.reels_viewed && viewer.reels_viewed.length > 0) {
                    let uniqueViewedReels = [];
                    for (let v = 0; v < viewer.reels_viewed.length; v++) {
                        uniqueViewedReels.push(viewer.reels_viewed[v]);
                    }
                    if (uniqueViewedReels.indexOf(reel.reel_id) === -1) {
                        viewer.reels_viewed.push({
                            reel_id: reel.reel_id,
                            reel_creator_username: person.username,
                            reel_creator_user_id: person.user_id,
                            timestamp: reel.timestamp
                        });
                    }
                } else {
                    viewer.reels_viewed = [{
                        reel_id: reel.reel_id,
                        reel_creator_username: person.username,
                        reel_creator_user_id: person.user_id,
                        timestamp: reel.timestamp
                    }];
                }
                let savedViewer = await this.utils.createOrUpdate(User, "username", viewer.username, viewer);
                if (savedViewer) {
                    this.utils.writeLog("Saved viewer: " + savedViewer.username); 
                }
                allViewers.push(viewerData);
            }
        } 
        this.utils.writeLog("Total viewers: " + reel.viewers.length);
        return [reel, allViewers];
    }

    /**
     * Fetches all archieved stories, if there is a poll, also the story voters.
     * @param username identifier string of user.
     * @param user_id number of primary key pk of user.
     */
    async getAllArchiviedStoriesWithVoters (username: string, userId: number = 0) {
        this.utils.connectToDB();
        let person = new User();
        person.username = username;
        let foundUser = await this.utils.find(User, "username", person.username);
        if (foundUser) {
            person = foundUser;
        } else {
            if (userId === 0) {
                this.utils.writeLog("Searching for user id for " + username);
                const igAccount = await this.IG.user.searchExact(username);
                userId = igAccount.pk;
                this.utils.writeLog("Found user id " + userId + " for " + username);
            }
            person.user_id = userId;
        }
        person.scrape_timings_stories_started = Date.now() * 1000;
        
        let stories = await this.getArchivedStories();
        this.utils.writeLog("Getting archived stories for " + person.username + " total: " + stories.length);
        // cycle through all archived stories
        for (let c = 0; c < stories.length; c++) {
            let story = new Story();
            let storyId = stories[c]['id'];
            // Check in db if we have already processed this story and to which point
            let found = await this.utils.find(Story, "story_id", storyId);
            let reels = <any>[];
            if (found) {
                reels = found.reels;
                this.utils.writeLog("Story: " + storyId + " exists.");
            } else {
                let storyInfo = this.IG.feed.reelsMedia({
                    userIds: [
                        storyId
                    ]
                });
                story.username = username;
                story.user_id = userId;
                story.story_id = storyId;
                this.utils.writeLog("Fetching story with id: " + storyId + " " + c + "/" + stories.length);
                await this.utils.delay(this.utils.SLEEP_TIMEOUT);
                const wholeResponse = await storyInfo.request();
                story.info = wholeResponse;
                // TODO: Save story here
                story.save();
                reels = await wholeResponse.reels[storyId].items;
            }
            
            this.utils.writeLog("Going through " + reels.length + " reels...");
            let count = 0;
            // for each reel extract the id and poll id and initial voters
            for (let reel of reels) {
                let reelId = (reel['id']) ? reel['id']: reel['reel_id'];
                if (!reelId) {
                    this.utils.writeLog("BREAKING " + story.story_id);
                    break;
                }
                let reelData = await this.utils.find(Reel, "reel_id", reelId);
                let 
                    storyPollVoterInfos = null,
                    storyPolls = null;
                if (reelData) {
                    // Check if its a poll we havent fetched yet
                    if (
                        reelData.info['story_poll_voter_info'] && 
                        reelData.info['story_polls'] &&
                        reelData.voters.length === 0
                        //&& ( (reelData.info['story_poll_voter_info']['latest_poll_vote_time'] * 1000) - Date.now()) < 3600
                    ) {
                        storyPollVoterInfos = reelData.info['story_poll_voter_info'];
                        storyPolls = reelData.info['story_polls'];
                    } 
                } else {
                    // Create a new reel
                    reelData = new Reel();
                    reelData.reel_id = reelId;
                    reelData.info = reel;
                    reelData.viewers_count = reel['viewer_count'];
                    reelData.timestamp = reel['taken_at'];
                    if (reel['story_poll_voter_infos'] && reel['story_polls']) {
                        storyPollVoterInfos = reel['story_poll_voter_infos'];
                        storyPolls = reel['story_polls'];
                    }
                    reelData.save();
                }
                let activeStoryFound = await this.utils.find(Story, "story_id", userId.toString() + "-" + reelData.timestamp);
                if (activeStoryFound) {
                    story = activeStoryFound;
                    story.story_id = storyId;
                    let savedStory = await this.utils.createOrUpdate(Story, "story_id", activeStoryFound.story_id, story);
                }
                // If this reel has a poll
                if (storyPollVoterInfos && storyPolls) {
                    this.utils.writeLog("Reel " + count + "/" + reels.length);
                    reelData = await this.processPoll(reel, reelId, storyId, reelData);
                } else {
                    this.utils.writeLog("No voters to process in story(" + storyId + ") reel with id: " + reelId + " " + count + " / " + reels.length);
                }
                count += 1;
                let uniqueReels = [];
                for (let storyReel of story.reels) {
                    uniqueReels.push(storyReel.reel_id);
                }   
                if (uniqueReels.indexOf(reelData.reel_id) === -1) {
                    story.reels.push(reelData);
                }
            }
            let savedStory = await this.utils.createOrUpdate(Story, "story_id", storyId, story);
            let uniqueStories = [];
            for (let storyPerson of person.stories) {
                uniqueStories.push(storyPerson.story_id);
            }
            if (uniqueStories.indexOf(savedStory.story_id) === -1) {
                person.stories.push(savedStory);
            }
            this.utils.writeLog("Processed stories for " + person.username + ": " +c + "/" + person.stories.length);
            let savedPerson = await this.utils.createOrUpdate(User, "username", person.username, person);
            this.utils.writeLog("Saved stories for " + savedPerson.username + ": " + savedPerson.stories.length);
        }
        person.scrape_timings_stories_finished = Date.now() * 1000;
        let savedPersonFinal = await this.utils.createOrUpdate(User, "username", person.username, person);
        this.utils.writeLog("Saved person: " + savedPersonFinal.username + " stories: " + savedPersonFinal.stories.length);
        return person;
    }

    /**
     * Processes a poll of voters in a reel.
     * @param reel Object with sub story information.
     * @param reelId Identifier for the reel.
     * @param storyId Identifier for the story
     * @param reelData Object IReel holds substory info.
     */
    async processPoll (reel: object, reelId: string, storyId: string, reelData: IReel) {
        let processedVoters = <any>[];
        try {
            let pollId = reel['story_poll_voter_infos'][0]['poll_id'];
            // how many voters?
            this.utils.writeLog("Found " + reel['story_poll_voter_infos'][0]['voters'].length + " voters in story(" + storyId + ") reel id: " + reelId);
            processedVoters = await this.processVoters(
                reel['story_poll_voter_infos'][0]['voters'], 
                reelId, 
                storyId, 
                pollId,
                reel['story_polls'][0]['poll_sticker']['tallies'],
                reel['user']['pk']
            );
            reelData.voters = reelData.voters.concat(processedVoters);
            await this.utils.delay(this.utils.SLEEP_TIMEOUT);
            // get more voters
            let voters = await this.getVoters(reelId, pollId);
            this.utils.writeLog("Fetched " + voters.length + " additional voters");
            processedVoters = await this.processVoters(
                voters, 
                reelId, 
                storyId, 
                pollId,
                reel['story_polls'][0]['poll_sticker']['tallies'],
                reel['user']['pk']
            );
            reelData.voters = reelData.voters.concat(processedVoters);
            this.utils.writeLog("Total voters fetched for story:" + storyId + " : " + reelData.voters.length);     
        } catch (err) {
            this.utils.writeLog(err);
            //await writeFile(fn+"_errored.json", JSON.stringify(person));
        }
        return reelData;
    }

    /**
     * Creates new users for voters or updates existing.
     * @param voters Array of user and voting information
     * @param reelId Identifier for the reel
     * @param storyId Identifier for the story
     * @param pollId Identifier for the poll
     * @param pollOptions Array with information on vote options in story_polls->[0]->poll_sticker->tallies
     */
    async processVoters (voters: Array<any>, reelId: string, storyId: string, pollId: string, pollOptions: Array<object>, pollCreatorUserId: number) {
        let processedVoters = <any>[];
        for (let voterData of voters) {
            let voter = new User();
            voter.username = voterData['user']['username'];
            voter.user_id = voterData['user']['pk'];
            voter.profile_pic = voterData['user']['profile_pic_url'];
            voter.full_name = voterData['user']['full_name'];
            let found = await this.utils.find(User, "username", voter.username);
            if (found) {
                voter = found;
            }
            let voteData = new Voter();
            voteData.reel_id = reelId;
            voteData.story_id = storyId;
            voteData.poll_id = pollId;
            voteData.vote = voterData['vote'];
            voteData.timestamp= voterData['ts'];
            voteData.vote_option = pollOptions[voteData.vote];
            let voteObject = {
                user_id: voter.user_id,
                username: voter.username,
                vote: voteData.vote,
                vote_option: pollOptions[voteData.vote],
                timestamp: voteData.timestamp,
                poll_id: voteData.poll_id,
                reel_id: voteData.reel_id,
                poll_creator_user_id: pollCreatorUserId
            };
            voter.votes_given.push(voteObject);
            processedVoters.push(voteObject);
            let savedVoter = await this.utils.createOrUpdate(User, "username", voter.username, voter);  
        }
        return processedVoters;
    }


}