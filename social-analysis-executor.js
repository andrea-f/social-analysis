"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var social_analysis_1 = require("./social-analysis");
var tags_1 = require("./tags");
function batchAnalysis() {
    return __awaiter(this, void 0, void 0, function () {
        var environment, commands, action, username, tag, sa, tags;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    environment = 0;
                    commands = [];
                    try {
                        commands = process.argv.slice(3);
                        environment = parseInt(process.argv[2]);
                    }
                    catch (err) {
                        commands = process.argv.slice(2);
                    }
                    console.log("environment: " + environment, "commands: ", commands);
                    sa = new social_analysis_1.SocialAnalysis();
                    tags = new tags_1.Tags();
                    sa.utils.connectToDB(environment);
                    if (!(commands.length > 0)) return [3 /*break*/, 14];
                    action = commands[0];
                    if (!(action === "bulkuserinfo")) return [3 /*break*/, 3];
                    console.log("Doing bulkuserinfo for all profiles in db!");
                    return [4 /*yield*/, sa.profile.populateAllUserInfo()];
                case 1:
                    _a.sent();
                    console.log("Finished doing bulkuserinfo for all profiles in db!");
                    return [4 /*yield*/, sa.utils.writeLog("Finished doing bulkuserinfo for all profiles in db!")];
                case 2:
                    _a.sent();
                    return [2 /*return*/];
                case 3:
                    if (typeof commands[1] !== "undefined") {
                        tag = commands[1];
                    }
                    else {
                        console.log("No specific input either profile or tag");
                        return [2 /*return*/];
                    }
                    if (!(action === "explore")) return [3 /*break*/, 5];
                    return [4 /*yield*/, tags.getExplorePagePostsByTag(tag)];
                case 4:
                    _a.sent();
                    console.log("Finished exploring tag: " + tag);
                    return [3 /*break*/, 14];
                case 5:
                    if (!(action === "posters")) return [3 /*break*/, 7];
                    console.log("Starting extracting posters for " + tag);
                    return [4 /*yield*/, sa.stats.uniqueTagPosters(tag)];
                case 6:
                    _a.sent();
                    console.log("Finished extracting posters for " + tag);
                    return [3 /*break*/, 14];
                case 7:
                    if (!(action === "tagengagement")) return [3 /*break*/, 10];
                    console.log("Calculating tag engagement for " + tag);
                    return [4 /*yield*/, sa.stats.calculatePostsEngagement(tag, "hashtag")];
                case 8:
                    _a.sent();
                    return [4 /*yield*/, sa.stats.exportPostsEngagementToCSV(tag, "hashtag")];
                case 9:
                    _a.sent();
                    return [3 /*break*/, 14];
                case 10:
                    if (typeof commands[1] !== "undefined") {
                        username = commands[1];
                    }
                    if (!(action === "all")) return [3 /*break*/, 12];
                    return [4 /*yield*/, sa.analyseUser(username)];
                case 11:
                    _a.sent();
                    return [3 /*break*/, 14];
                case 12: return [4 /*yield*/, sa.analyseUser(username, [action])];
                case 13:
                    _a.sent();
                    _a.label = 14;
                case 14: return [2 /*return*/];
            }
        });
    });
}
batchAnalysis();
//# sourceMappingURL=social-analysis-executor.js.map