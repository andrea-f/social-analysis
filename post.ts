
import {User, IUser, Post, IPost, Hashtag, IStatus} from './db-schema';
import { IgApiClient } from 'instagram-private-api';
import { CONFIG, ANALYSIS_CONFIG } from './config.interface';
import {Utils} from './utils';
import { ProfileAnalysis } from './profile';
let twitter = require('twitter-text');


export class PostAnalysis {

    constructor () {
        this.utils = new Utils;
        this.IG = new IgApiClient();
        this.CONFIG = this.utils.CONFIG;
        this.ANALYSIS_CONFIG = this.utils.ANALYSIS_CONFIG;
        this.profile = new ProfileAnalysis();
    }

    utils: Utils;
    IG: IgApiClient;
    CONFIG: CONFIG;
    ANALYSIS_CONFIG: ANALYSIS_CONFIG;
    profile: ProfileAnalysis;

    /**
     * Creates curl command options for fetching posts not authed.
     * @param username identifier for user to export data of, string.
     * @param ownerId user id identifier of who the post belongs to, number.
     * @param nextMaxId alphanumeric string to fetch next set of results, string.
     * @param numberOfResults max number of posts to try to fetch, usually max is 50, number.
     */
    async getPostsAnonymousQuery (username: string, ownerId: number, nextMaxId: string = "", numberOfResults: number = 1000) {
        let query = "curl";
        let commands = [
            'https://www.instagram.com/graphql/query/?query_hash=e769aa130647d2354c40ea6a439bfc08&variables=%7B%22id%22%3A%22'+ownerId+'%22%2C%22first%22%3A' + numberOfResults + '%2C%22after%22%3A%22' + nextMaxId + '%22%7D',
            '-H', 'authority: www.instagram.com',
            //'-H', 'x-mid: 1pagwgj1smkwyj5cu04p11rucz4sy9xbj31lzv356hdoa1wxp7ur',
            '-H', 'x-ig-www-claim: 0',
            //'-H', 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/538.36 (KHTML, like Gecko) Chrome/70.0.3987.106 Safari/538.36',
            '-H', 'accept: */*',
            '-H', 'sec-fetch-dest: empty',
            '-H', 'x-requested-with: XMLHttpRequest',
            //'-H', 'x-csrftoken: rJ4hGSDFN7sNbrhZmfH3YcdTm14F7fn7',
            '-H', 'x-ig-app-id: 936619743392459',
            '-H', 'sec-fetch-site: same-origin',
            '-H', 'sec-fetch-mode: cors',
            '-H', 'referer: https://www.instagram.com/'+username+'/',
            '-H', 'accept-language: en-GB,en-US;q=0.9,en;q=0.8,it;q=0.7',
            '--compressed'
        ];
        let results = await this.utils.getAnonymousQuery(query, commands);
        return results;
    }

    /**
     * Gets posts with possible max 1000 likers and comments.
     * @param username Identifier of which user to fetch posts of.
     * @param pk User Id primary key for user to get posts for.
     */
    async getPosts (username: string, pk: number = 0) {
        if (this.profile.logged_in === false) {
            this.utils.writeLog("Logging in....");
            const loggedInUser = await this.profile.login();
            
        }
        let person = await this.utils.find(User, "username", username);
        if (person && person.pk) {
            pk = person.pk;
        }
        if (pk === 0 && username.length > 0) {
            const igAccount = await this.profile.IG.user.searchExact(username);
            pk = igAccount.pk;
            this.utils.writeLog("Found account id: " + pk);
        }

        if (!person) {
            let person = new User();
            person.username = username;
            person.user_id = pk;
        }
        if (!person.info) {
            this.utils.writeLog("Getting userinfo: " + pk);
            person = await this.profile.getUserInfo(username);
        }
        let totalPosts = (person.media_count) ? person.media_count: person.info['media_count'];
        let started = Date.now();
        this.utils.writeLog("Getting posts for " + username);
        let userFeed;
        try {
            userFeed = this.profile.IG.feed.user(pk);
            this.utils.writeLog("Fetched userfeed");
        } catch (err) {
            this.utils.writeLog(JSON.stringify(err, null, 4));
            console.log(err);
        }
        
        
        let posts = <any>[];
        try {
            this.utils.writeLog("Creating max id");
            if (userFeed) {
                posts = await userFeed.items();
                this.utils.writeLog("Retrived posts: " + posts.length);
            }
            
        } catch (err) {
            this.utils.writeLog("GET POSTS ERROR");
            this.utils.writeLog(JSON.stringify(err, null, 4));
            console.log(err);
            /*let retry = await this.utils.checkErrorAndRecover(err);
            try {
                userFeed = await this.utils.createMaxId(userFeed);
                posts = await userFeed.items();
                this.utils.writeLog("Found " + posts.length + " latest posts for " + person.username);
            } catch (err) {
                if (retry === true) {
                    await this.getPosts(username, pk);
                } else {
                    return person;
                }
            }*/
        }
        if (posts.length === 0) {
            this.utils.writeLog("No posts found for " + person.username);
            return person;
        }
        
        // TODO: the person key might not be necessary
        let personWithPosts = null;
        try {
            this.utils.writeLog("Processing posts: " + posts.length);
            personWithPosts = await this.processPosts(posts, person);
            const ignoreMaxId = this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].fetching.ignore_max_id;
            if (ignoreMaxId == false) {
                try {
                    userFeed = await this.utils.createMaxId(userFeed);
                } catch (err) {
                    this.utils.writeLog("error in creating maxid");
                    this.utils.writeLog(JSON.stringify(err));
                    console.log("EERRRRR", err);
                }
                userFeed = await this.utils.checkToResumeFetchingFromAPoint(userFeed, "posts");
                await this.utils.delay(this.utils.SLEEP_TIMEOUT);
            }
            while (userFeed['moreAvailable']) {
                let newItems = <any>[];
                try {
                    //userFeed = await this.createMaxId(userFeed);
                    newItems = await userFeed.items();
                    this.utils.writeLog("Found " + newItems.length + " posts. Total fetched: " + person.posts.length + "/" + totalPosts) ;
                    personWithPosts = await this.processPosts(newItems, person);
                    await this.utils.delay(this.utils.SLEEP_TIMEOUT);
                } catch (err) {
                    this.utils.writeLog("GET MORE POSTS ERROR");
                    this.utils.writeLog(JSON.stringify(err));
                    /*let retry = await this.utils.checkErrorAndRecover(err);
                    if (retry === true) {
                        await this.getPosts(username, pk);
                    } else {
                        return person;
                    }*/
                }
            }
            this.utils.writeLog("Scraped " + personWithPosts.posts.length + " posts for " + personWithPosts.username);
            personWithPosts.scrape_timings_posts_started = started;
            personWithPosts.scrape_timings_posts_finished = Date.now();
        } catch (err) {
            this.utils.writeLog("Error in fetching more posts::");
            this.utils.writeLog(JSON.stringify(err));
            //TODO: Enable mechanism with proxies
            /*let retry = await this.utils.checkErrorAndRecover(err);
            if (retry === true) {
                await this.getPosts(username, pk);
            } else {
                this.utils.writeLog("Returning person...");
                return person;
            }*/
        }
        try {
            if (personWithPosts) {
                let savedPerson = await this.utils.createOrUpdate(User, "username", personWithPosts.username, personWithPosts);
                return savedPerson;
            }
        } catch (err) {
            this.utils.writeLog("Error in saving person in get posts", err.toString().substr(0, 150));
            return personWithPosts;
        }
    }

    /**
     * Fetches posts for a specific public user.
     * @param username identifier for user to export data of, string.
     */
    async getPostsAnonymous (username: string) {
        let person = new User();
        person.username = username;
        let totalProcessedPosts = 0;
        let personWithPosts;
        let personFound = await this.utils.find(User, "username", username);
        if (personFound) {
            person = personFound;
        }
        if (!person.info) {
            try {
                person = await this.profile.getUserInfo(username);
                console.log("Found: " + person.username);
            } catch (err) {
                this.utils.writeLog("Error in getPostsAnonymous userinfo");
                try {
                    this.utils.writeLog(JSON.stringify(err));
                } catch (err1) {
                    this.utils.writeLog(err);
                }
            }
        }
        try {
            this.utils.writeLog("Getting profile for user: " + username);
            let userProfile = <any>await this.profile.getProfileAnonymousQuery(username);
            this.utils.totalQueries += 1;
            let totalPosts = userProfile['graphql']['user']['edge_owner_to_timeline_media']['count'];
            person.media_count = totalPosts;
            person.user_id = userProfile['graphql']['user']['id'];
            person.follower_count = userProfile['graphql']['user']['edge_followed_by']['count'];
            person.following_count = userProfile['graphql']['user']['edge_follow']['count'];
            person = await this.profile.updateFollowersFollowingCounts(person);

            let postsData = userProfile['graphql']['user']['edge_owner_to_timeline_media'];

            let posts = postsData['edges'];
            this.utils.writeLog("Found " + posts.length + " posts for " + username);
            personWithPosts = await this.processPosts(posts, person, true, true);
            let started = Date.now();
            totalProcessedPosts += posts.length;
            // TODO: check if status exists

            // Check if an existing status exists, if so load it
            // Do after the first request to make sure all the new posts are fetched
            let statusObject = <IStatus>await this.utils.getStatus({
                user_id: person.user_id,
                action_type: "posts"
            });
            let data = {
                id: person.user_id,
                nextMaxId: postsData.page_info.end_cursor,
                timestamp: started
            };
            if (statusObject && statusObject.next_max_id && statusObject.next_max_id.length > 0 && statusObject.next_max_id.indexOf('_') === -1) {
                postsData.page_info.has_next_page = true;
                postsData.page_info.end_cursor = statusObject.next_max_id;
            } else {
                await this.utils.setStatus(data, "posts");
            }
            while (postsData.page_info.has_next_page === true) {
                try {
                    let userFeed = <any>await this.getPostsAnonymousQuery(username, personWithPosts.user_id, postsData.page_info.end_cursor);
                    postsData = userFeed['data']['user']['edge_owner_to_timeline_media'];
                    //this.writeLog(postsData);
                    posts = postsData['edges'];
                    totalProcessedPosts += posts.length;
                    personWithPosts = <IUser>await this.processPosts(posts, personWithPosts, true, true);
                    data = {
                        id: person.user_id,
                        nextMaxId: postsData.page_info.end_cursor,
                        timestamp: Date.now()
                    };
                    await this.utils.setStatus(data, "posts");
                    this.utils.writeLog("Found " + posts.length + " more posts, total processed: " + totalProcessedPosts + "/"+ totalPosts +" for " + username);
                } catch (err) {
                    this.utils.writeLog("ERROR in ferching more posts anon");
                    try {
                        this.utils.writeLog(JSON.stringify(err));
                    } catch (err) {
                        this.utils.writeLog(err);
                    }
                }
            }
            this.utils.writeLog("total processed posts: " + totalProcessedPosts + " for " + username);
            try {
                if (personWithPosts) {
                    let savedPerson = await this.utils.createOrUpdate(User, "username", personWithPosts.username, personWithPosts);
                    return savedPerson;
                }
            } catch (err) {
                this.utils.writeLog("Error in saving person in get posts");
                this.utils.writeLog(err.toString().substr(0, 150));
            }
        } catch (err) {
            this.utils.writeLog("Error getAnonymousPostsQuery");
            console.log("Failed getting posts anonymously", err);
            throw err;
        }
        if (person.info) {
            person = await this.utils.createOrUpdate(User, "username", person.username, person);
            
        } 
        return person;
    }

    /**
     * Creates the curl commands to Fetch the likers of a post.
     * @param shortcode identifier for post to fetch likers of, string.
     * @param nextMaxId alphanumeric string to fetch next set of results, string.
     * @param numberOfResults max number of posts to try to fetch, usually max is 50, number.
     */
    async getLikersAnonymousQuery (shortcode: string, nextMaxId: string, numberOfResults: number = 1000) {
        let query = "curl";
        let commands = [
            'https://www.instagram.com/graphql/query/?query_hash=d5d763b1e2acf209d62d22d184488e57&variables=%7B%22shortcode%22%3A%22'+shortcode+'%22%2C%22include_reel%22%3Atrue%2C%22first%22%3A'+numberOfResults+'%2C%22after%22%3A%22'+nextMaxId+'%22%7D',
            '-H', 'authority: www.instagram.com',
            '-H', 'accept: */*',
            '-H', 'x-ig-www-claim: 0',
            '-H', 'x-requested-with: XMLHttpRequest',
            //'-H', 'x-csrftoken: DffiKxIg6GGiBYbq7cCk86kktPdNRJuM',
            '-H', 'x-ig-app-id: 936619743392459',
            '-H', 'sec-fetch-site: same-origin',
            '-H', 'sec-fetch-mode: cors',
            '-H', 'referer: https://www.instagram.com/p/' + shortcode + '/',
            '-H', 'accept-encoding: gzip, deflate, br',
            '-H', 'accept-language: en-GB,en-US;q=0.9,en;q=0.8,it;q=0.7',
            //'-H', 'cookie: ig_cb=1; ig_did=71BD46FE-60A8-48A7-B356-9C469A8853E8; csrftoken=DffiKxIg6GGiBYbq7cCk86kktPdNRJuM; mid=XimjvAAEAAF9eU8_7xjwtSWi3VhN; rur=ASH; ',
            '--compressed'
        ];
        let results = await this.utils.getAnonymousQuery(query, commands);

        return results;
    }

    async getPostLikersLoggedIn (postData: IPost, processedLikers: number = 0) {
        let likers = await this.profile.IG.media.likers(postData.post_id);
        processedLikers += await this.processLikers(likers.users, postData);
        postData.likers = postData.likers.concat(likers.users);
        let savedPost = await this.utils.createOrUpdate(Post, "post_id", postData.post_id, postData);
        await this.utils.delay(this.utils.SLEEP_TIMEOUT);
        return postData;
    }

    async getPostLikersAnon (postData: IPost, shortcode: string, processedLikers: number = 0) {
        let likerIds = <any>[];
        let likers = <any>await this.getLikersAnonymousQuery(shortcode, '', 1000);
        if (!likers) {
            return postData;
        }
        if (likers.data == undefined) {
            console.log("FIRST",likers);
        }
        let likersData = likers.data.shortcode_media.edge_liked_by;
        processedLikers += await this.processLikers(likersData.edges, postData);
        // TODO: This introduces inconsistency in data
        for (let l of likersData.edges) {
            if (likerIds.indexOf(l.node.id) === -1) {
                postData.likers.push(l.node);
                likerIds.push(l.node.id);
            }
        }
        this.utils.totalQueries += 1;
        // TODO: Save status here of end cursor so can be picked up later
        try {
            postData = await this.utils.createOrUpdate(Post, "post_id", postData.post_id, postData);
        } catch (err) {
            this.utils.writeLog("Error in [getPostLikersAnon]createOrUpdate for " + shortcode);
            this.utils.writeLog(err);
        }
        while (likersData.page_info.has_next_page === true) {
            likers = await this.getLikersAnonymousQuery(shortcode, likersData.page_info.end_cursor, 1000);
            if (!likers) {
                return postData;
            }
            if (likers.data == undefined) {
                console.log("SECOND",likers);
            }
            // TODO: Save status here of end cursor so can be picked up later
            likersData = likers.data.shortcode_media.edge_liked_by;
            processedLikers = await this.processLikers(likersData.edges, postData);
            //postData.likers = postData.likers.concat(likersData.edges);
            for (let l of likersData.edges) {
                if (likerIds.indexOf(l.node.id) === -1) {
                    postData.likers.push(l.node);
                    likerIds.push(l.node.id);
                }
            }
            this.utils.totalQueries += 1;
            postData = await this.utils.createOrUpdate(Post, "post_id", postData.post_id, postData);
            if (postData.likers.length > this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].fetching.max_likers) {
                break;
            }
        }
        return postData;
    }

    async getPostCommentsLoggedIn (postData: IPost) {
        this.utils.writeLog("Getting all comments LOGGED IN for post " + postData.post_id);
        let commentsResponse = await this.profile.IG.feed.mediaComments(postData.post_id);
        let comments = await commentsResponse.items();
        //console.log(comments);
        let c = 0;
        for (let comment of comments) {
            postData.comments.push(comment);
            c += 1;
        }
        this.utils.writeLog("Processed " + c + " comments for " + postData.post_id);
        // = postData.comments.concat(comments[0]);    
        //console.log(comments);
        //await this.delay(this.SLEEP_TIMEOUT);
        return postData;
    }

    async getPostCommentsAnon (postData: IPost, shortcode: string, processedComments: number = 0) {
        let comments = <any>[];
        let commentIds = <any>[];
        let postDetail = await this.getPostAnonymous(shortcode, postData.typename);
        if (!postDetail.data.shortcode_media) {
            return postData;
        }
        let commentsData = postDetail.data.shortcode_media.edge_media_to_parent_comment;
        //this.writeLog(commentsData);
        //this.writeLog("======");
        processedComments += await this.processComments(commentsData.edges, postData);
        this.utils.totalQueries += 1;
        let added = 0
        for (let c of commentsData.edges) {
            if (commentIds.indexOf(c.node.id) === -1) {
                postData.comments.push(c.node);
                commentIds.push(c.node.id);
                added += 1;
            }
        }
        if (added > 0) {
            postData = await this.utils.createOrUpdate(Post, "post_id", postData.post_id, postData);
        }
        added = 0;
        while (commentsData.page_info.has_next_page === true && commentsData.page_info.end_cursor) {
            comments = <any>await this.getCommentsAnonymousQuery(shortcode, commentsData.page_info.end_cursor, 1000);
            //this.writeLog(comments);
            commentsData = comments.data.shortcode_media.edge_media_to_parent_comment;
            processedComments += await this.processComments(commentsData.edges, postData);
            for (let c of commentsData.edges) {
                if (commentIds.indexOf(c.node.id) === -1) {
                    postData.comments.push(c.node);
                    commentIds.push(c.node.id);
                    added += 1;
                } else {
                    this.utils.writeLog("id already present: " + c.node.id + " " + c.node.owner.username + " " + c.node.text);
                }
            }
            this.utils.totalQueries += 1;
            if (added > 0) {
                postData = await this.utils.createOrUpdate(Post, "post_id", postData.post_id, postData);
            }
            if (postData.comments.length > this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].fetching.max_comments || postData.comments.length === postData.comment_count) {
                break;
            }
        }  
        return postData;      
    }

    /**
     * Creates post information in db and updates user.
     * @param posts List with post objects to analyse.
     * @param person Posts owner and creator.
     * @param getAllLikers Either true or false to fetch all additional likers (max 1000).
     * @param getAllComments Either true or false to fetch all comments.
     */
    async processPosts (posts: Array<any>, person: IUser, getAllLikers: boolean = true, getAllComments: boolean = true) {
        let savedPersonFinal = person;
        let c = 0;
        for (let post of posts) {
            try {
                let postData = new Post();
                if (post['node']) {
                    post = post['node'];
                }
                if (post['pk']) {
                    postData.post_id = post['pk'];
                } else {
                    postData.post_id = post['id'].toString();
                }
                // TODO: add better check if we need to get this post or get more information on it
                let postFound = await this.utils.find(Post, "post_id", postData.post_id);
                //this.writeLog(post);
                if (!postFound) {
                    postData.info = post;
                    try {
                        postData.user_id = post['user']['pk'];
                    } catch (err) {
                        postData.user_id = post['owner']['id'];
                    }
                    //console.log("post", post);
                    try {
                        postData.username = post['user']['username'];
                    } catch (err) {
                        postData.username = post['owner']['username'];
                    }
                    if (!postData.username) {
                        postData.username = person.username;//await this.getUsernameFromId(postData.user_id);
                    }
                    if (post['taken_at']) {
                        postData.timestamp = post['taken_at'];
                    } else {
                        postData.timestamp = post['taken_at_timestamp'];
                    }

                    if (post['location']) {
                        postData.location = post['location'];
                    } 

                    if (post['like_count']) {
                        postData.like_count = post['like_count'];
                    } else {
                        try {
                            postData.like_count = post['edge_liked_by']['count'];
                        } catch (err) {
                            postData.like_count = post['edge_media_preview_like']['count'];
                        }
                    }
                    if (post['comment_count']) {
                        postData.comment_count = post['comment_count'];
                    } else {
                        try {
                            postData.comment_count = post['edge_media_to_comment']['count'];
                        } catch (err) {
                            postData.comment_count = 0;
                        }
                    }
                    if (post['caption']) {
                        postData.text = post['caption']['text'];
                    } else if (post['edge_media_to_caption'] && post['edge_media_to_caption']['edges'][0]) {
                        postData.text = post['edge_media_to_caption']['edges'][0]['node']['text'];
                    } else {
                        postData.text = "";
                    }
                    // TODO: What about facepile likers?
                    if (post['image_versions2']) {
                        postData.media.push(post['image_versions2']['candidates'][0]);
                    } else if (post['carousel_media']) {
                        for (let c_media of post['carousel_media']) {
                            postData.media.push(c_media['image_versions2']['candidates'][0]);
                        }
                    } else if (post['display_url']) {
                        postData.media.push(post['display_url']);
                    }
                    // Save picture here
                    if (postData.media.length > 0) {
                        postData.media_data = await this.utils.downloadImageMedia(postData);
                    }
                    if (post['accessibility_caption']) {
                        postData.image_details.push(post['accessibility_caption']);
                    } 
                    postData.typename = post['__typename'];
                    
                    let shortcode = post['shortcode'];
                    
                    try {
                        postData.engagement = (postData.comment_count + postData.like_count) / person.follower_count;
                    } catch (err) {
                        this.utils.writeLog("ENGAGEMENT not saved: " + postData.post_id);
                        this.utils.writeLog(err);
                    }
                    try {
                        postData = await this.extractTagsAndMentionsFromPostText(postData);
                    } catch (err) {
                        this.utils.writeLog("Error in extractingtags in process posts");
                        this.utils.writeLog(err);
                    }
                    
                    this.utils.writeLog("Found " + postData.hashtags.length + " hashtags in " + postData.post_id);
                    
                    postData.save();
                    this.utils.writeLog("Fetching likers/comments logged in? " + this.profile.logged_in);
                    if (getAllLikers) {
                        //this.writeLog("Getting all likers for post " + postData.post_id);
                        if (this.profile.logged_in === true) {
                            postData = await this.getPostLikersLoggedIn(postData);
                        } else {
                            try {
                                postData = <IPost>await this.getPostLikersAnon(postData, shortcode);
                            } catch (err) {
                                this.utils.writeLog("Error in getPostLikersAnon for " + shortcode);
                                this.utils.writeLog(JSON.stringify(err));
                            }
                        }
                    }
                    if (getAllComments) {
                        // TODO: Think about comments logged in.
                        if (this.profile.logged_in === true) {
                            postData = await this.getPostCommentsLoggedIn(postData);
                        } else {
                            try {
                                postData = await this.getPostCommentsAnon(postData, shortcode);    
                            } catch (err) {
                                this.utils.writeLog("Error in getPostCommentsAnon for " + shortcode);
                                this.utils.writeLog(err);
                            }
                        }
                    }
                    this.utils.writeLog("Fetched " + postData.likers.length + " likers, comments: " + postData.comments.length + " " + postData.post_id + " of " + person.username +" "+ person.posts.length + "/" + person.media_count);
                    person.posts.push(postData.post_id);
                    savedPersonFinal = await this.utils.createOrUpdate(User, "username", person.username, person);
                } else {
                    this.utils.writeLog("Post " + postData.post_id + " already processed.");
                }
            } catch (err) {
                try {
                    this.utils.writeLog("Error in processing post: " + (post['pk'] ? post['pk']: post['id']) + " " + c + "/" + posts.length);
                } catch (err1) {
                    this.utils.writeLog("Error processing post:: ");
                    this.utils.writeLog(post);
                }
                console.log("Process post error:",err);
            }
            c += 1;
        }
        return savedPersonFinal;
    }

    /**
     * Creates curl commands list and kicks off request to get comments for a post.
     * @param username identifier for user created the post, string.
     * @param shortcode identifier for the post to fetch, string.
     * @param endCursor last max id of results, to fetch next batch, string.
     * @param numberOfResults max number of posts to try to fetch, usually max is 50, number.
     */
    async getCommentsAnonymousQuery (shortcode: string, endCursor: string, numberOfResults: number = 1000) {
        let query = "curl";
        //472f257a40c653c64c666ce877d59d2b
        //2cc8bfb89429345060d1212147913582
        //97b41c52301f77ce508f55e66d17620e
        //3b38775b9f92c2c0b13e0303ca55d34e
        //bc3296d1ce80a24b1b6e40b1e72903f5
        let commands = [
            '-H', 'authority: www.instagram.com',
            //'-H', 'x-ig-www-claim: hmac.AR3-XFOCFEmaQfHW5kBrpC_SVAY4IvB2JgTZtj22SyTxRfe4',
            //'-H', 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/538.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/538.36',
            '-H', 'accept: */*',
            '-H', 'sec-fetch-dest: empty',
            '-H', 'x-requested-with: XMLHttpRequest',
            //'-H', 'x-csrftoken: TpyNBf8cXIHWbYowmmpWlPsED67K4ojb',
            '-H', 'x-ig-app-id: 936619743392459',
            '-H', 'sec-fetch-site: same-origin',
            '-H', 'sec-fetch-mode: cors',
            '-H', 'referer: https://www.instagram.com/p/'+shortcode+'/',
            '-H', 'accept-language: en-GB,en-US;q=0.9,en;q=0.8,it;q=0.7',
            //'-H', 'cookie: ig_cb=1; ig_did=E24801BD-E2AC-49F5-A18A-D06C3C7AA111; mid=XmAB1gAEAAExZlaT5xGLnG4GebVe; rur=FRC; csrftoken=TpyNBf8cXIHWbYowmmpWlPsED67K4ojb; shbid=4765; shbts=1583458244.244561; ds_user_id=28448878251; sessionid=28448878251%3A8x7JXA3joaAGRA%3A8; urlgen="{\"2001:b07:6463:6a6b:e105:aa3d:598f:b03d\": 12874}:1jAC0w:NJBzZsr1igdVvcPUgc8UaSP3ftQ"',
            '--compressed'
        ];
        let cachedCommentsCursor = undefined;
        try {
            cachedCommentsCursor = JSON.parse(endCursor).cached_comments_cursor;
        } catch (err) {

        }
        if (cachedCommentsCursor !== undefined) {
            //https://i.instagram.com
            //https://www.instagram.com
            commands.unshift('https://i.instagram.com/graphql/query/?query_hash=bc3296d1ce80a24b1b6e40b1e72903f5&variables=%7B%22shortcode%22%3A%22'+shortcode+'%22%2C%22first%22%3A12%2C%22after%22%3A%22%7B%5C%22cached_comments_cursor%5C%22%3A+%5C%22'+cachedCommentsCursor+'%5C%22%2C+%5C%22bifilter_token%5C%22%3A+%5C%22KJUBARYAwACIAGAAQAAwACAAGAAQABAACAAIABrdrioFVrc7TIccOkz2eK0ZOk377FHWMX7QYKCVzVP_r2kZj2ucyWg3D-_NnuBhi-PMsVQanROwk77AZbdgy9m2J2OsMydv9HpQxen3-X__7g_NaEAHYOm3DM2SdhZe7zkTylloFtKJToFpHLZCQE4Cf1Aw0s0HEGgQOCAA%5C%22%7D%22%7D');
        } else {
            commands.unshift('https://i.instagram.com/graphql/query/?query_hash=bc3296d1ce80a24b1b6e40b1e72903f5&variables=%7B%22shortcode%22%3A%22'+shortcode+'%22%2C%22first%22%3A50%2C%22after%22%3A%22'+endCursor+'%22%7D');
        }

        // de0e95df5be2a6d17a21af5e0213dc23

        // more comments carousel: {"shortcode":"B7tuzwJI7ZZ","first":13,"after":"QVFDVE5Nc21GbW0zUy1xRFlLQVN3VWU5WkRTcWJrMzI5NnlleVl6aVIwOE9ObzI3VkVaa09mcDBYcnhRQ2tXNGVydzY2R0Znak83R19MU0NYaUhQZ3VHLQ=="}
        
        let results = <any>await this.utils.getAnonymousQuery(query, commands);
        return results;
    }

    /**
     * Creates curl list of commands and performs request to fetch a specific post, not authed.
     * @param shortcode identifier for the post to fetch, string.
     * @param postType type of post - image, video, carousel - determines options in commands list, string.
     */
    async getPostAnonymous (shortcode: string, postType: string) {
        let query = "curl";
            // e769aa130647d2354c40ea6a439bfc08 -> nextmaxid, first response for post, 
            // ea0f07e73ad28955150d066bd22ef843 -> cached_comments_cursor, first response for post, bc3296d1ce80a24b1b6e40b1e72903f5 for second comments request
        //de0e95df5be2a6d17a21af5e0213dc23  image  
        // 6b838488258d7a4820e48d209ef79eb1 video
        // 6b838488258d7a4820e48d209ef79eb1 carousel variables: {"has_threaded_comments":true}

        let commands = [
            
            '-H', 'authority: www.instagram.com',
            //'-H', 'x-ig-www-claim: hmac.AR3-XFOCFEmaQfHW5kBrpC_SVAY4IvB2JgTZtj22SyTxRfe4',
            //'-H', 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/538.36 (KHTML, like Gecko) Chrome/70.0.3987.106 Safari/538.36',
            '-H', 'accept: */*',
            '-H', 'sec-fetch-dest: empty',
            '-H', 'x-requested-with: XMLHttpRequest',
            //'-H', 'x-csrftoken: TpyNBf8cXIHWbYowmmpWlPsED67K4ojb',
            '-H', 'x-ig-app-id: 936619743392459',
            '-H', 'sec-fetch-site: same-origin',
            '-H', 'sec-fetch-mode: cors',
            '-H', 'referer: https://www.instagram.com/p/'+shortcode+'/',
            '-H', 'accept-language: en-GB,en-US;q=0.9,en;q=0.8,it;q=0.7', 
            //'-H', 'cookie: ig_cb=1; ig_did=E24801BD-E2AC-49F5-A18A-D06C3C7AA111; mid=XmAB1gAEAAExZlaT5xGLnG4GebVe; rur=FRC; csrftoken=TpyNBf8cXIHWbYowmmpWlPsED67K4ojb; shbid=4765; shbts=1583458244.244561; ds_user_id=28448878251; sessionid=28448878251%3A8x7JXA3joaAGRA%3A8;',
            '--compressed'
        ];

        switch (postType) {
            case "GraphVideo":
                commands.unshift('https://www.instagram.com/graphql/query/?query_hash=6b838488258d7a4820e48d209ef79eb1&variables=%7B%22shortcode%22%3A%22'+shortcode+'%22%2C%22has_threaded_comments%22%3Atrue%7D');
                //commands.unshift('https://www.instagram.com/graphql/query/?query_hash=6b838488258d7a4820e48d209ef79eb1&variables=%7B%22has_threaded_comments%22%3Atrue%7D');
                break;
            case "GraphSidecar":
            case "GraphImage":
                commands.unshift('https://www.instagram.com/graphql/query/?query_hash=de0e95df5be2a6d17a21af5e0213dc23&variables=%7B%22shortcode%22%3A%22'+shortcode+'%22%2C%22child_comment_count%22%3A3%2C%22fetch_comment_count%22%3A60%2C%22parent_comment_count%22%3A60%2C%22has_threaded_comments%22%3Atrue%7D');
                break;
        }

        let result = <any>await this.utils.getAnonymousQuery(query, commands);
        return result;
    }

    /**
     * Saves each liker in the database.
     * @param likers Array with users objects that liked a post
     * @param postData Information about the post
     */
    async processLikers (likers: Array<object>, postData: IPost) {
        let totalProcessedLikers = 0;
        //this.writeLog("Processing liker....");
        
        for (let liker of likers) {
            let person = new User();
            // TODO: Improve assigning of variable
            if (liker['node']) {
                liker = liker['node'];
            } 
            person.username = liker['username'];
            if ( liker['pk'] ) {
                person.user_id = liker['pk'];
            } else {
                person.user_id = liker['id'];
            }
            person.profile_pic = liker['profile_pic_url'];
            person.full_name = liker['full_name'];
            let likerFound = await this.utils.find(User, "username", person.username);
            let likedObject = {
                post_creator_user_id: postData.user_id,
                post_creator_username: postData.username,
                post_id: postData.post_id,
                post_timestamp: postData.timestamp,
                post_media: postData.media
            }
            if (likerFound) {
                //this.writeLog("liker found...." + likerFound.username + " " + likerFound.user_id);
                try{
                    likerFound.liked.push(likedObject);
                    person = await this.utils.createOrUpdate(User, "username", likerFound.username, likerFound);
                } catch (err) {
                    //this.writeLog("err in process likers", err.toString().substr(0, 50));
                    //this.writeLog("err in process likers", err.toString().substr(0, 150));
                }
            } else {
                try {
                    person.liked.push(likedObject);
                    await person.save();
                    //this.writeLog("Created liker....");
                } catch (err) {
                    //this.writeLog("Error in saving liker" + err.errmsg);
                    //this.writeLog(err);
                    //this.writeLog(JSON.stringify(liker));
                    //this.writeLog("Error in saving liker", err.toString().substr(0, 50));
                    try {
                        person = await this.utils.createOrUpdate(User, "user_id", person.user_id.toString(), person);
                    } catch (err) {
                        this.utils.writeLog("Second Error in saving liker");
                        this.utils.writeLog(err);
                    }
                }
            }
            totalProcessedLikers += 1;
        }
        return totalProcessedLikers;
    }

    /**
     * Saves each commenter in the database.
     * @param comments Array with users objects that commented on a post
     * @param postData Information about the post
     */
    async processComments (comments: Array<object>, postData: IPost) {
        let totalProcessedComments = 0;
        //this.writeLog("Processing liker....");
        
        for (let comment of comments) {
            let person = new User();
            // TODO: Improve assigning of variable
            if (comment['node']) {
                comment = comment['node'];
            }
            if (comment['user']) {
                person.username = comment['user']['username'];
                person.user_id = comment['user']['id'];
            } else {
                person.username = comment['owner']['username'];
                person.user_id = comment['owner']['id'];
            }
            
            if (comment['user']) {
                person.profile_pic = comment['user']['profile_pic_url'];
                if (comment['user']['full_name']) {
                    person.full_name = comment['user']['full_name'];
                }
            } else {
                person.profile_pic = comment['owner']['profile_pic_url'];
                if (comment['owner']['full_name']) {
                    person.full_name = comment['owner']['full_name'];
                }
            }
            
           
            let commenterFound = await this.utils.find(User, "username", person.username);
            let commentObject = {
                post_creator_user_id: postData.user_id,
                post_creator_username: postData.username,
                post_id: postData.post_id,
                post_timestamp: postData.timestamp,
                post_media: postData.media,
                comment_text: comment['text'],
                comment_timestamp: comment['created_at']
            };
            if (commenterFound) {
                //this.writeLog("liker found...." + likerFound.username + " " + likerFound.user_id);
                try{
                    commenterFound.comments.push(commentObject);
                    person = await this.utils.createOrUpdate(User, "username", commenterFound.username, commenterFound);
                } catch (err) {
                    //this.writeLog("err in process likers", err.toString().substr(0, 50));
                    //this.writeLog("err in process likers", err.toString().substr(0, 150));
                }
            } else {
                try {
                    person.comments.push(commentObject);
                    await person.save();
                    //this.writeLog("Created commenter " + person.username);
                } catch (err) {
                    //this.writeLog("Error in saving liker", err.toString().substr(0, 150));
                    //this.writeLog("Error in saving commenter");
                    
                    try {
                        person = await this.utils.createOrUpdate(User, "username", person.username, person);
                        //this.writeLog("Saved person " + person.username);
                    } catch (err) {
                        this.utils.writeLog("Second Error in saving commenter");
                        this.utils.writeLog(err);
                    }
                }
            }
            totalProcessedComments += 1;
        }
        return totalProcessedComments;
    }

    async extractTagsAndMentionsFromPostText (post: IPost) {
        let postInfo = this.extractPostTextData(post.text);
        if (postInfo.mentions && postInfo.mentions.length > 0) {
            post.mentions = postInfo.mentions;
        } else {
            post.mentions = [];
        }
        if (postInfo.hashtags && postInfo.hashtags.length > 0) {
            // TODO: Add new hashtag
            this.utils.writeLog("Extracted " + postInfo.hashtags.length + " hashtags from post: " + post.post_id);
            for (let tag of postInfo.hashtags) {
                //this.writeLog("Checking hashtag: " + tag);
                tag = tag.toLowerCase().replace('#', '');
                let hashtag = await this.utils.find(Hashtag, "name", tag);
                
                if (hashtag) {
                    if (hashtag.posts.indexOf(post.post_id) === -1) {
                        hashtag.posts.push(post.post_id);
                    }
                    let savedHashtag = await this.utils.createOrUpdate(Hashtag, "name", hashtag.name, hashtag);
                } else {
                    hashtag = new Hashtag();
                    
                    hashtag.name = tag;
                    
                    //this.writeLog("Saving tag: " + tag);
                    try {
                        hashtag.posts.push(post.post_id);
                    } catch (err) {
                        hashtag.posts = [post.post_id];
                    }
                    await hashtag.save();
                }
            }
            post.hashtags = postInfo.hashtags;
        } else {
            post.hashtags = [];
        }
        return post;
    }

    async extractAllTagsAndMentionsFromPostsText (username: string, posts: Array<IPost> = []) {
        if (posts.length === 0) {
            posts = await this.profile.getUserSavedPosts(username);
        }
        let user = await this.utils.find(User, 'username', username);
        if (user == undefined) {
            this.utils.writeLog("User " + username + " not found");
            return [];
        }
        let c = 0;
        let mentionsObject = {};
        let tagsObject = {};
        for (let post of posts) {
            if (!post.engagement) {
                post.engagement = ( post.like_count + post.comment_count ) / ( (user.follower_count) ? user.follower_count: -1);
                let savedPost = await this.utils.createOrUpdate(Post, "post_id", post.post_id, post);
                this.utils.writeLog("Calculated engagement for::: " + savedPost.post_id + "(" + username + ") of " + savedPost.engagement);
            }
            try {
                post = await this.extractTagsAndMentionsFromPostText(post);
                let savedPost = await this.utils.createOrUpdate(Post, "post_id", post.post_id, post);
            
                this.utils.writeLog(post.hashtags.length + " hashtags, Saved " + post.mentions.length + " mentions for post " + post.post_id + " (" + username + ") " + c + "/" + posts.length);
            } catch (err) {
                this.utils.writeLog("Error in extracting text");
                this.utils.writeLog(err);
                post.mentions = [];
                post.hashtags = [];
            }
            for (let mention of post.mentions) {
                mention = mention.toLowerCase();
                if (mentionsObject[mention]) {
                    mentionsObject[mention].push(post.engagement);
                } else {
                    mentionsObject[mention] = [post.engagement];
                }
            }
            for (let tag of post.hashtags) {
                tag = tag.toLowerCase();
                if (tagsObject[tag]) {
                    tagsObject[tag].push(post.engagement);
                } else {
                    tagsObject[tag] = [post.engagement];
                }
            }

            c += 1;
        }
        let tagsStats = <any>[];
        for (let t of Object.keys(tagsObject)) {
            const sum = tagsObject[t].reduce((a: any, b: any) => a + b, 0);
            const avg = (sum / tagsObject[t].length) || 0;
            tagsStats.push({
                "hashtag": t,
                "engagement": avg,
                "occurrences": tagsObject[t].length
            });
        }
        let tagsItem = {
            "hashtags": tagsStats,
            "timestamp": this.utils.timestampToDate(Date.now() / 1000)
        }
        
        if (!user.hashtags) {
            user.hashtags = [tagsItem];
        } else {
            user.hashtags.push(tagsItem);
        }
        
        let mentionsStats = <any>[];
        for (let t of Object.keys(mentionsObject)) {
            const sum = mentionsObject[t].reduce((a: any, b: any) => a + b, 0);
            const avg = (sum / mentionsObject[t].length) || 0;
            mentionsStats.push({
                "mention": t,
                "engagement": avg,
                "occurrences": mentionsObject[t].length
            });
        }
        let mentionsItem = {
            "mentions": mentionsStats,
            "timestamp": this.utils.timestampToDate(Date.now() / 1000)
        }
        if (!user.mentions) {
            user.mentions = [mentionsItem];
        } else {
            user.mentions.push(mentionsItem);
        }
        
        let savedUser = await this.utils.createOrUpdate(User, "username", user.username, user);

        this.utils.writeLog("Saved " + Object.keys(tagsObject).length + " tags and " + Object.keys(mentionsObject).length + " mentions for " + username);
        let savedFiles = await this.exportMentionsAndTagsToCSV(username);
        return savedFiles;
    }

    async exportMentionsAndTagsToCSV (username: string)  {
        let user = await this.utils.find(User, 'username', username);
        let mentionsStats = (user.mentions && user.mentions.length > 0) ? user.mentions[user.mentions.length-1].mentions: [];
        let tagsStats = (user.hashtags && user.hashtags.length > 0) ? user.hashtags[user.hashtags.length-1].hashtags: [];
        let mentionsEngagement = <any>[[
            "Mention",
            "Average engagement",
            "Occurrences"
        ].join(',')];
        for (let s of mentionsStats) {
            mentionsEngagement.push([
                s.mention,
                s.engagement,
                s.occurrences
            ].join(','));
        }
        let tagsEngagement = <any>[[
            "Hashtag",
            "Average engagement",
            "Occurrences"
        ].join(',')];
        for (let s of tagsStats) {
            tagsEngagement.push([
                s.hashtag,
                s.engagement,
                s.occurrences
            ].join(','));
        }
        let fn = username + "_mentions_" + this.utils.timestampToDate(Date.now() / 1000) + ".csv";
        await this.utils.writeFile(fn, mentionsEngagement.join('\n'));
        let fn1 = username + "_tags_" + this.utils.timestampToDate(Date.now() / 1000) + ".csv";
        await this.utils.writeFile(fn1, tagsEngagement.join('\n'));
        this.utils.writeLog("Wrote: " + fn + " and " + fn1);
        return [fn, fn1];
    }

    /**
     * Extracts either the hashtags (#WORD), mentions (@username) from the post text. 
     * @param text Text of post, string.
     * @param dataType What is being extracted, string.
     */
    extractPostTextData (text: string) {
        text = twitter.htmlEscape(text);
        let tags = text.match(/#[\p{L}]+/ugi);
        let mentions = twitter.extractMentions(text);
        return {
            "hashtags": tags,
            "mentions": mentions
        };
    }









}