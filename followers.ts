import {User, IUser, Followers} from './db-schema';
import { IgApiClient } from 'instagram-private-api';
import { CONFIG, ANALYSIS_CONFIG } from './config.interface';

import crypto from 'crypto';
import {Utils} from './utils';

export interface IFollowerObject {
    followers: string[];
    timestamp: number;
}


export class FollowersAnalysis  {

    constructor () {
        this.utils = new Utils;
        this.IG = new IgApiClient();
        this.CONFIG = this.utils.CONFIG;
        this.ANALYSIS_CONFIG = this.utils.ANALYSIS_CONFIG;
    }

    utils: Utils;
    IG: IgApiClient;
    CONFIG: CONFIG;
    ANALYSIS_CONFIG: ANALYSIS_CONFIG;

    /**
     * Analyses followers list.
     * @param items List of followers to analyse.
     * @param followerObject Contains current followers list with timestamp.
     * @param person User whose followers are being tallied up.
     */
    async processFollowers (items: Array<any>, person: IUser) {
        for (let item of items) {
            try {
                let follower = new User();
                follower.username = item['username'];
                follower.user_id = item['pk'];
                let foundFollower = await this.utils.find(User, "username", follower.username);
                if (foundFollower) {
                    follower = foundFollower;
                }
                follower.full_name = item['full_name'];
                follower.profile_pic = item['profile_pic_url'];
                // Check if follower already exists in list
                if (follower.following.indexOf(person.username) === -1) {
                    follower.following.push(person.username);
                }
                let savedFollower = await this.utils.createOrUpdate(User, 'username', follower.username, follower);
                //if (savedFollower && savedFollower['username']) this.writeLog("Updated follower: " + savedFollower['username']);
            } catch (err) {
                this.utils.writeLog(item);
                this.utils.writeLog("Error in process followers: " + err);
            }
        };
        this.utils.writeLog("Total followers analysed: " + items.length);
    }


    /**
     * Retrieves followers given a username and user private key (id).
     * @param username Name of user to get followers for
     * @param pk User Id for that username
     */
    async getFollowers (username: string, pk: number = 0) {
        this.utils.connectToDB();
        if (pk === 0) {
            this.utils.writeLog("Searching for user id for " + username);
            const igAccount = await this.IG.user.searchExact(username);
            pk = igAccount.pk;
            this.utils.writeLog("Found user id " + pk + " for " + username);
        }
        let person = new User();
        person.username = username;
        person.user_id = pk;
        person.scrape_timings_followers_started = <any>Date.now();
        person = await this.utils.createOrUpdate(User, 'username', username, person);
        this.utils.writeLog("Getting followers for " + person.username);
        let followersFeed = this.IG.feed.accountFollowers(pk);
        // Check if we have already processed it
        
        let items = <any>[];
        try {
            items = await followersFeed.items();
        } catch (err) {
            this.utils.writeLog("ERROR GET FOLLOWERS");
            this.utils.writeLog(err);
            let retry = await this.utils.checkErrorAndRecover(err);
            if (retry === true) {
                await this.getFollowers(username, pk);
            } else {
                return person;
            }
        }
        followersFeed = await this.utils.checkToResumeFetchingFromAPoint(followersFeed, "followers");
        let followersRetrieved = items.length;       
        let startDate = Date.now() / 1000;
        
        let index = 0;
        if (person.followers && person.followers.length > 0) {
            index = person.followers.length;   
        }
        // Going through the first set of followers
        let followers = new Followers();
        followers.username = person.username;
        let followersData = <any>[];
        for (let item of items) {
            followersData.push(item.username);
        }
        followers.followers = followersData;
        followers.batch_hash = crypto.createHash("sha256").update(followersData.toString()).digest('hex');
        let foundFollowers = await this.utils.find(Followers, "batch_hash", followers.batch_hash);
        if (foundFollowers) {
            this.utils.writeLog("Batch hash " + followers.batch_hash + " exists");
            followers = foundFollowers;
        } else {
            await this.processFollowers(items, person);
            followers.save();
            this.utils.writeLog("Saved batch " + followers.batch_hash + " with " + items.length + " items.");
        }
        followers.scan_dates.push(startDate);
        person.followers.push(startDate);
        await this.utils.createOrUpdate(User, 'username', username, person);
        await this.utils.delay(this.utils.SLEEP_TIMEOUT);
        let count = 1;
        while (followersFeed['moreAvailable']) {
            let newItems = <any>[];
            followersFeed = await this.utils.checkToResumeFetchingFromAPoint(followersFeed, "followers");
            try {
                newItems = await followersFeed.items();
            } catch (err) {
                this.utils.writeLog("ERROR GET MORE FOLLOWERS", err);
                let retry = await this.utils.checkErrorAndRecover(err);
                if (retry === true) {
                    await this.getFollowers(username, pk);
                } else {
                    return person;
                }
            }
            followersRetrieved += newItems.length;
            let newFollowersData = <any>[];
            for (let followerFound of newItems) {
                newFollowersData.push(followerFound.username);
            }
            let newFollowers = new Followers();
            newFollowers.username = person.username;
            let followersData = <any>[];
            for (let item of items) {
                followersData.push(item.username);
            }
            newFollowers.followers = newFollowersData;
            newFollowers.batch_hash = crypto.createHash("sha256").update(newFollowersData.toString()).digest('hex');
            let foundNewFollowers = await this.utils.find(Followers, "batch_hash", newFollowers.batch_hash);
            if (foundNewFollowers) {
                newFollowers = foundNewFollowers;
            } else {
                await this.processFollowers(newItems, person);
                newFollowers.save();
                this.utils.writeLog("Saved batch " + newFollowers.batch_hash + " with " + newItems.length + " items.");
            }
            newFollowers.scan_dates.push(startDate);
            this.utils.writeLog("Retrieved followers: " + followersRetrieved);
            count +=1;
            await this.utils.delay(this.utils.SLEEP_TIMEOUT);        
        }
        //person.followers[index].followers = person.followers[index].followers.concat(followerObject.followers);
        person.scrape_timings_followers_finished = <any>Date.now();
        //this.writeLog("Retrieved " + followersRetrieved + " followers, followersObject: " + person.followers[index].followers.length);
        let savedPersonFinal = await this.utils.createOrUpdate(User, 'username', username, person);
        this.utils.writeLog("Fetched " + followersRetrieved + " followers for " + savedPersonFinal.username);
        return savedPersonFinal;
    }

        /**
     * Retrieve followers from DB.
     * @param username profile to analyse, string.
     */
    async getUserFollowersFromDB (username: string) {
        let maxFollowers: number = 100000;
        if (this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].analysis.limits.max_followers) {
            maxFollowers = this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].analysis.limits.max_followers;
        }
        let allFollowersListOfLists = <any>[];
        let followers = <any>[];
        try {
            // TODO: Add further filter to only get the latest followers scan date, not all.
            allFollowersListOfLists = await Followers.find({'username': username}).limit(maxFollowers).exec();
            for (let followerList of allFollowersListOfLists) {
                followers = followers.concat(followerList.followers);
                if (followers.length >= maxFollowers) {
                    break;
                }
            }
        } catch (err) {
            this.utils.writeLog("Error in getting all followers");
            this.utils.writeLog(err);
        }
        
        /*let followersIds = <any>[];
        for (let f of followers) {
            let user = await this.find(User, "username", f);
            followersIds.push(user.user_id);
        }*/
        let uniqueFollowers = Array.from(new Set(followers));
        this.utils.writeLog("Found " + uniqueFollowers.length + " followers for " + username);
        return uniqueFollowers;
    }

    /**
     * Checks to see if a username is a follower of the passed in user instance.
     * @param username identifier of profile to check if its a follower.
     * @param user person instance to check if username is follower of.
     */
    async checkIfUsernameIsFollower (username: string, user: IUser) {
        let follower = {
                "$and": [
                { "username": user.username },
                { "scan_dates": user.followers[user.followers.length-1]},
                { "followers": {
                    "$in": [
                        username
                    ]}
                }
            ]
        };
        let followersData = await this.utils.complexFind(Followers, follower);
        //this.writeLog(followersData);
        if (followersData) {
            return true;
        } else {
            return false;
        }
    }
}