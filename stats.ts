import { User,  IUser, Post, Hashtag, IHashtag } from './db-schema';
import { CONFIG, ANALYSIS_CONFIG } from './config.interface';
import { Utils } from './utils';
import { ProfileAnalysis } from './profile';
import { PostAnalysis } from './post';
import { IgApiClient } from 'instagram-private-api';
import { FollowersAnalysis } from './followers';
import mongoose from 'mongoose';
import { ImageAnalysis } from './image-analysis';
import { Tags } from './tags';


export class Stats {

    constructor () {
        this.utils = new Utils;
        this.IG = new IgApiClient();
        this.profile = new ProfileAnalysis();
        this.tags = new Tags();
        this.CONFIG = this.utils.CONFIG;
        this.ANALYSIS_CONFIG = this.utils.ANALYSIS_CONFIG;
        this.followers = new FollowersAnalysis();
        this.FAN_POINTS = {
            like: 2,
            comment: 5,
            vote: 3,
            follower: 0.5,
            view: 1
        };
        this.post = new PostAnalysis;
        this.image_analysis = new ImageAnalysis();
    }

    utils: Utils;
    IG: IgApiClient;
    CONFIG: CONFIG;
    ANALYSIS_CONFIG: ANALYSIS_CONFIG;
    FAN_POINTS: {
        like: number,
        comment: number,
        vote: number,
        follower: number,
        view: number
    };
    db: any;
    profile: ProfileAnalysis;
    tags: Tags;
    followers: FollowersAnalysis;
    post: PostAnalysis;
    image_analysis: ImageAnalysis;
    
    /**
     * Calculates engagement for a user based on unique interactors.
     * @param username Identifier of the user to calculate engagements stats for, string.
     * @param profiles List of profiles to analyze against user data (optional), array.
     */
    async calculateMostEngagedUsers (username: string, profiles: Array<any> = []) {
        let user = await this.utils.find(User, "username", username);
        if (!user) {
            this.utils.writeLog("User " + username + " not found.");
            return "User " + username + " not found.";
        }
        //let interactedUsersWithData = <any>[];
        let posts = await this.profile.getUserSavedPosts(username);
        this.utils.writeLog("Getting unique interacted users:: " + username);
        let interactedWith;
        if (profiles.length === 0) {
            interactedWith = await this.getUniqueInteractedUsers(username);
        } else {
            interactedWith = profiles;
        }
        

        // TODO: Split into different function and call from getUniqueInteractedUsers
        this.utils.writeLog("Found " + interactedWith.length + " users who interacted with " + username);
        let engageTime = Date.now();
        let count = 0;
        if (interactedWith.length > 0) {
            if (this.profile.logged_in === false) {
                await this.profile.login();
            }

            for (let interactor of interactedWith) {
                //this.utils.writeLog("Checking interactions for " + interactor);
                if (interactor === null) continue;
                let userEngagement = {
                    username: <string>interactor,
                    views: 0,
                    votes: 0,
                    likes: 0,
                    follower: 0,
                    comments: 0,
                    score: 0,
                    timestamp: <number>engageTime,
                    engagement_with: <string>username

                };
                try {
                    let interactorIsFollower = await this.followers.checkIfUsernameIsFollower(interactor, user);
                    if (interactorIsFollower) {
                        userEngagement.follower += 1;
                    }
                } catch (err) {
                    this.utils.writeLog("Checking if username is follower error");
                    this.utils.writeLog(err);
                }
                
                for (let post of posts) {
                    if (post.likers && post.likers.length > 0) {
                        for (let liker of post.likers) {
                            if (liker.username === interactor) {
                                userEngagement.likes += 1
                            }
                        }
                    }
                    if (post.comments && post.comments.length > 0) {
                        try {
                            let comments = post.comments[0].comments;
                            if (comments === undefined) {
                                comments = post.comments;
                            }
                            if (comments && comments.length >0) {
                                for (let comment of comments) {
                                    let commenterUsername = (comment.user) ? comment.user.username: comment.owner.username;
                                    if (commenterUsername === interactor) {
                                        userEngagement.comments += 1;
                                    } 
                                }
                            }
                            
                        } catch (err) {
                            this.utils.writeLog("Counting comments error", err);
                        }
                    }
                }
                if (user.stories) {
                    // TODO: Add here max limit to stories?
                    for (let story of user.stories) {
                        for (let reel of story.reels) {
                            if (reel.viewers) {
                                for (let viewer of reel.viewers) {
                                    // TODO: check that viewer is a user id
                                    if (viewer === interactor) {
                                        userEngagement.views += 1;
                                    }
                                }
                            }
                            if (reel.voters) {
                                for (let voter of reel.voters) {
                                    if (voter.username === interactor) {
                                        userEngagement.votes += 1;
                                    }
                                }
                            }
                        }
                    }
                }
                userEngagement.score = ( (userEngagement.comments * this.FAN_POINTS.comment) + (userEngagement.views * this.FAN_POINTS.view) + (userEngagement.likes * this.FAN_POINTS.like) + (userEngagement.votes * this.FAN_POINTS.vote) + (userEngagement.follower * this.FAN_POINTS.follower));
                this.utils.writeLog("Calculated engagement score of " + userEngagement.score + " for " + userEngagement.username + " " + count + "/" + interactedWith.length);
                // TODO: Filter here by people who have at least X score?
                try {
                    let userInfo = await this.utils.find(User, "username", userEngagement.username);
                    if (!userInfo) {
                        this.utils.writeLog("User " + userEngagement.username + " not found, will fetch it...");
                        try {
                            userInfo = await this.profile.getUserInfo(userEngagement.username, 0, -1);
                        } catch (err) {
                            this.utils.writeLog("Error in creating user and fetching userinfo for " + username);
                            this.utils.writeLog(err);
                        }
                    }
                    if (userInfo && !userInfo.info && this.ANALYSIS_CONFIG.profiles[username].analysis.get_userinfo_while_calculating_engagement === true) {
                        // customSleep = -1 so its not waiting any delay.
                        try {
                            userInfo = await this.profile.getUserInfo(userInfo.username, userInfo.user_id, -1);
                        } catch (err) {
                            this.utils.writeLog("Error in fetching userinfo for " + username);
                            this.utils.writeLog(err);
                        } 
                    }
                    if (userInfo && userInfo.info && (userInfo.email || userInfo.info.public_email)) {
                        userEngagement['email'] = (userInfo.email) ? userInfo.email: userInfo.info.public_email;
                    }
                    if (userInfo && userInfo.info && (userInfo.phone || userInfo.info.public_phone_number)) {
                        userEngagement['phone'] = (userInfo.phone) ? userInfo.phone: userInfo.info.public_phone_number;
                    }
                    if (userInfo && userInfo.info) {
                        userEngagement['follower_count'] = userInfo.follower_count;
                    }
                    if (userInfo && userInfo.username) {
                        userEngagement['username'] = userInfo.username;
                    }
                    if (!userInfo.interactions) {
                        userInfo.interactions = <any>[];
                    }
                    try {
                        // Save calculate user engagment in list for that specific user
                        userInfo.interactions.push(userEngagement);
                        let savedInteractor = await this.utils.createOrUpdate(User, "username", userInfo.username, userInfo);
                    } catch (err) {
                        this.utils.writeLog("Saving interactor " + userInfo.username + " error");
                        this.utils.writeLog(err);
                    }
                } catch (err) {
                    this.utils.writeLog("Error in getting and updating interactor info for " + userEngagement.username);
                    this.utils.writeLog(err);
                }
                count += 1;
            }
            user['interacted_with_engagement'].push(engageTime);
            let savedUser = await this.utils.createOrUpdate(User, "username", user.username, user);
            this.utils.writeLog("Finished extracting most engaged users from "+ posts.length +" posts for " + username);
            return savedUser;
        }
    }

    /**
     * Calculates engagement for user based on posts and stories.
     * @param username Identifier for the user to calculate engagement for.
     */
    async calculatePostsEngagement (item: string, itemType: string = 'user') {
        let itemDbObject;
        if (itemType === 'user') {
            itemDbObject = <IUser>await this.utils.find(User, "username", item);
        } else if (itemType === 'hashtag'){
            itemDbObject = <IHashtag>await this.utils.find(Hashtag, "name", item);
        } else {
            this.utils.writeLog("[calculatePostsEngagement] Unknown item type: " + itemType);
            console.log("[calculatePostsEngagement] Unknown item type: " + itemType);
            return;
        }
        
        if (!itemDbObject) {
            this.utils.writeLog("[calculatePostsEngagement] Item "+ item +"("+itemType+") not found");
            return;
        }
        if (!itemDbObject.info && itemType === 'user') {
            itemDbObject = await this.profile.getUserInfo(item);
        }
        let stats = <any>{};
        
        if (itemDbObject.posts && itemDbObject.posts.length > 0) {
            // Calculate engagement for posts
            let overallPostsEngagement = 0,
                postsWithEngagement = <any>[],
                posts = <any>[];
                let totalFollowers;
            if (itemType === 'user') {
                posts = await this.profile.getUserSavedPosts(item);
                totalFollowers = itemDbObject.follower_count;
            } else if (itemType === 'hashtag') {
                posts = await this.tags.getHashtagSavedPosts(item);
            } else {
                this.utils.writeLog("Item type " + itemType + " unknown");
                console.log("Item type " + itemType + " unknown");
                return;
            }
            this.utils.writeLog("[calculatePostsEngagement] Calculating engagement for "+ posts.length +" posts...");
            let c = 0;
            for (let post of posts) {
                let taggerUsername, errored = false;
                if (itemType === "hashtag") {
                    let user = <IUser>await this.utils.find(User, "user_id", post.user_id);
                    if (user && !user.info) {
                        try {
                            this.utils.writeLog("[calculatePostsEngagement] Getting userinfo for: " + user.username);
                            user = await this.profile.getUserInfo(user.username);
                            // Sleep for X seconds
                            let sleepTime = 3.3;
                            this.utils.writeLog("[calculatePostsEngagement] Sleeping for: " + sleepTime + " after userinfo of "+ user.username);
                            await this.utils.delay(sleepTime);

                        } catch (err) {
                            this.utils.writeLog("[calculatePostsEngagement] Error in gettting user info while calculating enagement for " + item + " (" +itemType +")") ;
                            console.log("[calculatePostsEngagement] Error in gettting user info while calculating enagement for " + item + " (" +itemType +")", err);
                            totalFollowers = 0;
                        }
                    }
                    try {
                        
                        taggerUsername = user.username;
                        if (!user.follower_count || user.follower_count == undefined) {
                            totalFollowers = 0;
                        } else {
                            totalFollowers = user.follower_count;
                        }
                    } catch (err) {
                        console.log(user);
                        taggerUsername = post.username;
                        errored = true;
                    }
                }
                let engagement = 0;
                if (!post.comment_count || post.comment_count == undefined) {
                    post.comment_count = 0;
                }
                if (!post.like_count || post.like_count == undefined) {
                    post.like_count = 0;
                }
                if (errored === false) {
                    engagement = ( (post.like_count + post.comment_count) / totalFollowers ) * 100;
                }

                let postTime = (post.timestamp.toString().length > 10) ? post.timestamp / 1000: post.timestamp,
                    postEngagementData = {
                        likes: post.like_count,
                        comments: post.comment_count,
                        media: (post.media[0].url) ? post.media[0].url: post.media[0],
                        engagement: engagement,
                        post_id: post.post_id,
                        timestamp: this.utils.timestampToDate(postTime),
                        text: post.text,
                        shortcode: post.info.code
                    };
                if (itemType === "hashtag" && taggerUsername) {
                    postEngagementData['username'] = taggerUsername;
                    if (errored === false) {
                        postEngagementData['total_followers'] = totalFollowers;
                    } else {
                        postEngagementData['total_followers'] = -1;
                    }
                }
                postsWithEngagement.push(postEngagementData);
                this.utils.writeLog("Processed "+item+" post " + c + "/" +posts.length);
                c += 1;
            }
            for (let eng of postsWithEngagement) {
                overallPostsEngagement += eng.engagement;
            }
            overallPostsEngagement = overallPostsEngagement / postsWithEngagement.length;
            stats.posts = {
                posts_engagement: overallPostsEngagement.toFixed(3),
                posts_with_engagement: postsWithEngagement
            };
            this.utils.writeLog("[calculatePostsEngagement] Calculated overall posts engagement for " + item + ": " + overallPostsEngagement);
        }
        if (itemDbObject.stories && itemDbObject.stories.length > 0) {
            // Calculate engagement for stories
            let storiesWithEngagement = <any>[],
                reelsWithEngagement = <any>[],
                reelsActiveEngagement = <any>[];
            this.utils.writeLog("[calculatePostsEngagement] Calculating stories engagement...");
            // TODO: Add here max limit to stories?
            for (let story of itemDbObject.stories) {
                for (let reel of story.reels) {
                    if (!reel.info.total_viewer_count) {
                        continue;
                    }
                    let reelEngagement = (reel.info.total_viewer_count / itemDbObject.follower_count) * 100,
                        reelActiveEngagement = 0;
                    if (reel.voters.length > 0) {
                        reelActiveEngagement = (reel.voters.length / reel.info.total_viewer_count) * 100;
                        reelsActiveEngagement.push(reelActiveEngagement);
                    }
                    let reelEngagementData = {
                        views: reel.info.total_viewer_count,
                        story_id: story.story_id,
                        votes: reel.voters.length,
                        media: reel.info.image_versions2.candidates[0].url,
                        reel_id: reel.reel_id,
                        timestamp: this.utils.timestampToDate(reel.timestamp),
                        engagement: reelEngagement,
                        active_engagement: reelActiveEngagement
                    };
                    reelsWithEngagement.push(reelEngagementData);
                }
                let storyEngagement = 0,
                    storyActiveEngagement = 0;
                for (let eng of reelsWithEngagement) {
                    storyEngagement += eng.engagement;
                    storyActiveEngagement += eng.active_engagement; 
                }
                storyEngagement = storyEngagement / reelsWithEngagement.length;
                storyActiveEngagement = storyActiveEngagement / reelsActiveEngagement.length;
                let storyEngagementData = {
                    story_id: story.story_id,
                    media: story.reels[0].info.image_versions2.candidates[0].url,
                    timestamp: await this.utils.timestampToDate(story.reels[0].timestamp),
                    engagement: storyEngagement,
                    active_engagement: storyActiveEngagement
                };
                storiesWithEngagement.push(storyEngagementData);
            }
            let overallStoriesActiveEngagement = 0;
            let overallStoriesEngagement = 0;
            let storiesWithActiveEngagement = 0;
            for (let story of storiesWithEngagement) {
                overallStoriesEngagement += story.engagement;
                //this.utils.writeLog("story.active_engagement", story.active_engagement);
                if (!isNaN(story.active_engagement)) {
                    overallStoriesActiveEngagement += story.active_engagement;
                    storiesWithActiveEngagement += 1;
                }
            }
            if (storiesWithActiveEngagement > 0) {
                overallStoriesActiveEngagement = (overallStoriesActiveEngagement / storiesWithActiveEngagement);
            } 
            overallStoriesEngagement = overallStoriesEngagement / storiesWithEngagement.length;
            stats.stories = {
                reels_with_engagement: reelsWithEngagement,
                stories_with_engagement: storiesWithEngagement,
                stories_engagement: overallStoriesEngagement.toFixed(3),
                stories_active_engagement: overallStoriesActiveEngagement.toFixed(3)
            };
            this.utils.writeLog("[calculatePostsEngagement] Calculated stories engagement for " + itemDbObject.username + ": " + overallStoriesEngagement);
            this.utils.writeLog("[calculatePostsEngagement] Calculated stories active engagement for " + itemDbObject.username + ": " + overallStoriesActiveEngagement);
        }
        itemDbObject.engagement = stats;
        let savedItem;
        if (itemType === "user") {
            savedItem = await this.utils.createOrUpdate(User, "username", item, itemDbObject);
        } else {
            savedItem = await this.utils.createOrUpdate(Hashtag, "name", item, itemDbObject);
        }
        //this.utils.writeLog("Generated stats for " + savedPerson.username, savedPerson.engagement);
        return savedItem;
    }

    /**
     * Exports posts engagement for a specific user.
     * @param itsm identifier for user/hashtag to export data of, string.
     * @param itemType specify whether its a user or a hashtag, string.
     */
    async exportPostsEngagementToCSV (item: string, itemType: string = 'user') {
        let itemDbObject;
        if (itemType === 'user') {
            itemDbObject = <IUser>await this.utils.find(User, 'username', item);
        } else {
            itemDbObject = <IHashtag>await this.utils.find(Hashtag, "name", item);
        }
        
        let savedFiles = <any>[];
        if (itemDbObject.engagement) {
            if (itemDbObject.engagement['posts'] && itemDbObject.engagement['posts'].posts_with_engagement) {
                let tableHeaders = [
                    "Post ID",
                    "Post Link",
                    "Date",
                    "Likes",
                    "Comments",
                    "Engagement",
                    "Text"
                ];
                if (itemType === 'hashtag') {
                    tableHeaders.splice(2, 0, "Username");
                    tableHeaders.splice(3, 0, "Total username followers");
                }
                let postsEngagement = <any>[tableHeaders.join(',')];
                let totalLikes = 0;
                let totalComments = 0;
                
                for (let post of itemDbObject.engagement['posts'].posts_with_engagement) {
                    let cleanedText = "";
                    try {
                        cleanedText = post.text.replace( /[\r\n\t]+/gm, ' ' ).replace(',',' ').replace('"', '').replace("'",' ');
                    } catch (err) {

                    }
                    let dataRow = [
                        post.post_id,
                        "https://www.instagram.com/p/" + post.shortcode,
                        post.timestamp,
                        post.likes,
                        post.comments,
                        post.engagement,
                        cleanedText
                    ];
                    if (itemType === 'hashtag') {
                        dataRow.splice(2, 0, post.username);
                        dataRow.splice(3, 0, post.total_followers);
                    }
                    postsEngagement.push(dataRow.join(','));
                    totalLikes += post.likes;
                    totalComments += post.comments;
                }
                let bottomStats = [
                    "Overall posts engagement",
                    " ",
                    " ",
                    totalLikes,
                    totalComments,
                    itemDbObject.engagement['posts'].posts_engagement,
                    " "
                ];
                if (itemType === 'hashtag') {
                    bottomStats.splice(2, 0, " ");
                    bottomStats.splice(3, 0, " ");
                }
                postsEngagement.push(bottomStats.join(','));
                let identifier;
                if ( itemType === 'user' ) {
                    identifier = itemDbObject['username'];
                } else if ( itemType ==='hashtag' ) {
                    identifier = itemDbObject['name']
                }
                let fn = identifier + "_posts_engagement_" + this.utils.timestampToDate(Date.now() / 1000) + ".csv";
                await this.utils.writeFile(fn, postsEngagement.join('\n'));
                this.utils.writeLog("[exportPostsEngagementToCSV] Saved file " + fn);
                savedFiles.push(fn);
            }
        }
        return savedFiles;
    }

    /**
     * Saves user analysed information to file system in CSV.
     * @param username identifier for user to export data of, string.
     */
    async exportInteractorsDataToCSV (username: string) {
        let savedFiles = <any>[];
        let maxInteractorsPool: number = 250;
        if (this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].analysis.limits.max_interactors_pool) {
            maxInteractorsPool = this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].analysis.limits.max_interactors_pool;
        }
        let maxBatchSize: number = 100000
        if (this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].analysis.limits.max_interactors_batch_size) {
            maxBatchSize = this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].analysis.limits.max_interactors_batch_size;
        }
        
        let user = await this.utils.find(User, 'username', username);

        if (user.interacted_with_engagement && user.interacted_with_engagement.length > 0) {
            this.utils.writeLog("Exporting most active users for " + user.username +"...");
            let interactedDate = user.interacted_with_engagement[user.interacted_with_engagement.length-1]; // TODO: might change to a loop at some point
            
            try {
                
                if (!this.db) {
                    this.utils.writeLog("db object not found");
                    this.db = await this.utils.connectToDB();
                }
                this.utils.writeLog("Executing query");
                // TODO: Add here possibility of consuming next batch
                let interactedUsers = await mongoose.connection.db.collection('users').aggregate([
                    {"$project" : {"interactions": 1, "username": 1, "user_id": 1}},
                    {"$unwind" : "$interactions"}, 
                    {"$match" : {"interactions.timestamp":interactedDate, "interactions.engagement_with":username}},
                    {"$sort": {"interactions.score": -1}},
                    {"$limit": maxInteractorsPool}
                ], { allowDiskUse: true, cursor: { batchSize: maxBatchSize} }).toArray();

                this.utils.writeLog("retrieved: " + interactedUsers.length);
            
                // TODO: Check user info for top users
                

                if (interactedUsers.length > 0 ) {
                    let interactedEngagement = <any>[[
                        "Username",
                        "Score",
                        "Likes",
                        "Comments",
                        "Views",
                        "Votes",
                        "Is Follower?",
                        "Followers count",
                        "Email",
                        "Phone"
                    ].join(',')];
                    for (let interactorData of interactedUsers) {
                            let interaction = interactorData.interactions;
                                let interactorItem = [
                                    interaction['username'],//interactorData.username,
                                    interaction['score'],
                                    interaction['likes'],
                                    interaction['comments'],
                                    interaction['views'],
                                    interaction['votes'],
                                    (interaction['follower'] === 1) ? "Yes": "No"
                                ];
                                if (interaction['follower_count']) {
                                    interactorItem.push(interaction['follower_count']);
                                } else {
                                    interactorItem.push("");
                                }
                                if (interaction['email']) {
                                    interactorItem.push(interaction['email']);
                                } else {
                                    interactorItem.push("");
                                }
                                if (interaction['phone']) {
                                    interactorItem.push(interaction['phone']);
                                }
                                // TODO: Add here minimum score of engagement
                                interactedEngagement.push(interactorItem.join(','));
                                //break;
                            //}
                    }
                    //this.utils.writeLog("processed: " + interactedEngagement.length);
                    
                    let fn2 = user.username + "_people_interacted_with_" + this.utils.timestampToDate(Date.now() / 1000) + ".csv";
                    //this.utils.writeLog("Wrote file: " + fn2);
                    this.utils.writeLog("processed: " + interactedEngagement.length+" entries. Wrote file: " + fn2);
                    await this.utils.writeFile(fn2, interactedEngagement.join('\n'));
                    savedFiles.push(fn2);
                }
                 
            } catch (err) {
                this.utils.writeLog("error in query!");
                this.utils.writeLog(err);
                this.utils.writeLog(err);
            }
        } else {
            this.utils.writeLog("No user " + user.interacted_with_engagement);
        }

        return savedFiles;
    }

    /**
     * Creates a unique list of users that a profile interacted with.
     * @param username Contains identifier about the user to build relationship network for.
     */
    async getUniqueInteractedUsers (username: string) {
        let user = await this.utils.find(User, "username", username);
        if (!user) {
            return "User " + username + " not found.";
        }
        let usersInteractedWith = <any>[];
        // Go through followers
        let followers = <any>[];
        this.utils.writeLog("Getting followers for " + username);
        if (user.followers && user.followers.length > 0) {
            try {
                followers = await this.followers.getUserFollowersFromDB(user.username);
            } catch (err) {
                this.utils.writeLog("Error in getting followers from db for a user");
                this.utils.writeLog(err);
            }
            for (let follower of followers) {
                if (usersInteractedWith.indexOf(follower) === -1) {
                    usersInteractedWith.push(follower);
                }
            }
        }
        this.utils.writeLog("Getting likers from post....");
        let posts = await this.profile.getUserSavedPosts(username);
        this.utils.writeLog("Total retrieved posts: " + posts.length);
        if (posts.length > 0) {
            let c = 0;
            // TODO: Split into separate function to be called from getUserSavedPosts()
            for (let post of posts) {
                if (post.likers && post.likers.length > 0) {
                    //this.utils.writeLog("Total likers: " + post.likers.length);
                    for (let liker of post.likers) {
                        let userFound = await this.utils.find(User, 'username', liker.username);
                        if (!userFound) {
                            //this.utils.writeLog("Saving liker: " + liker.username);
                            let total = await this.post.processLikers([liker], post);
                            //let userFound = await this.find(User, 'username', liker.username);
                            //this.utils.writeLog( total + " processed like user " + userFound.username );
                        }
                        if (usersInteractedWith.indexOf(liker.username) === -1) {
                            usersInteractedWith.push(liker.username);
                        }
                    }
                }
                if (post.comments && post.comments.length > 0) {
                    try {
                        let comments = post.comments[0].comments;
                        if (comments === undefined) {
                            comments = post.comments;
                        }
                        if (comments && comments.length > 0) {
                            for (let comment of comments) {
                                let commenterUsername = (comment.user) ? comment.user.username: comment.owner.username;
                                let commenterUserId = (comment.user) ? comment.user.id: comment.owner.id;
                                // TODO: Hack, remove at some point
                                let user = await this.utils.find(User, 'username', commenterUsername);
                                if (!user) {
                                    //this.utils.writeLog("Saving user: " + commenterUsername);
                                    let total = await this.post.processComments([comment], post);
                                    //let userFound = await this.find(User, 'username', commenterUsername);
                                    //this.utils.writeLog( total + " processed comment user " + userFound.username );
                                }
                                if (usersInteractedWith.indexOf( commenterUsername ) === -1) {
                                    usersInteractedWith.push(commenterUsername);
                                } 
                            }
                        }
                    } catch (err) {
                        this.utils.writeLog("Counting comments error", post.comments);
                        this.utils.writeLog(err);
                    }
                }
                this.utils.writeLog(c + "/"+ posts.length +" Processing post: " + post.post_id + " of " + post.username + ", interactors: " + usersInteractedWith.length);
                c += 1;
            }
        }

        if (user.stories) {
            for (let story of user.stories) {
                for (let reel of story.reels) {
                    if (reel.viewers) {
                        for (let viewer of reel.viewers) {
                            if (usersInteractedWith.indexOf(viewer) === -1) {
                                usersInteractedWith.push(viewer);
                            }
                        }
                    }
                    if (reel.voters) {
                        for (let voter of reel.voters) {
                            if (usersInteractedWith.indexOf(voter.username) === -1) {
                                usersInteractedWith.push(voter.username);
                            }
                        }
                    }
                }
            }
        }
        //user.interacted_with = usersInteractedWith;
        // TODO: Add here different saving method
        this.utils.writeLog("Counted total interactors " + usersInteractedWith.length);
        if (usersInteractedWith.length > this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].analysis.limits.max_interactors_batch_size &&
            this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].analysis.limit_interactors === true) {
            usersInteractedWith = usersInteractedWith.slice(0, this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].analysis.limits.max_interactors_batch_size);
        }
        return usersInteractedWith;
    }

    /**
     * Calculates engagement for users connected to a profile for which there are contact details (email or phone or both).
     * @param username identifier of profile to calculate engagement for already analysed users, string.
     */
    async calculateEngagementAndExportContactInfoToCSV (username: string) {
        this.utils.writeLog("calculateEngagementAndExportContactInfoToCSV for " + username);
        let findParameters = [
            {
                'liked.post_creator_username': username,
            },
            {
                'comments.post_creator_username': username
            }
        ];
        let dateFrom = "";
        let dateTo = "";
        try {
            dateFrom = this.ANALYSIS_CONFIG.profiles[username].analysis.date_from;
        } catch (err) {
            
        }
        try {
            dateTo = this.ANALYSIS_CONFIG.profiles[username].analysis.date_to;
        } catch (err) {

        }
        if (dateFrom && dateFrom.length !== 0) {
            findParameters[0]['liked.post_timestamp']= {
                '$gte': Math.round(new Date(dateFrom).getTime()/1000)
            };
            findParameters[0]['comments.post_timestamp']= {
                '$gte': Math.round(new Date(dateFrom).getTime()/1000)
            };
        }
        if (dateTo && dateTo.length !== 0) {
            if (findParameters[0]['liked.post_timestamp'] !== undefined) {
                findParameters[0]['liked.post_timestamp']['$lt'] = Math.round(new Date(dateTo).getTime()/1000);
            } else {
                findParameters[0]['liked.post_timestamp'] = {
                    '$lt': Math.round(new Date(dateTo).getTime()/1000)
                };
            }
            if (findParameters[0]['comments.post_timestamp'] !== undefined) {
                findParameters[0]['comments.post_timestamp']['$lt'] = Math.round(new Date(dateTo).getTime()/1000);
            } else {
                findParameters[0]['comments.post_timestamp'] = {
                    '$lt': Math.round(new Date(dateTo).getTime()/1000)
                };
            }
        }
        let contacts = await User.find({
            $or:[{ email: {$exists:true} }, { phone: {$exists:true} }], 
            $and: [{$or: findParameters}]
        }).exec();
        this.utils.writeLog("Extracted " + contacts.length + " contacts for " + username);
        let contactsData = <any>[];
        contactsData.push(
            [
                "Username",
                "Profile pic url",
                "Engagement score",
                "Total likes",
                "Total comments",
                "Full name",
                "Total followers",
                "Email",
                "Phone"
            ].join(',')
        );
        let c = 0;
        for (let contact of contacts) {

            let totalLikes = 0;
            for (let like of contact.liked) {
                if (like.post_creator_username === username) {
                    totalLikes += 1;
                }
            }
            let totalComments = 0;
            for (let comment of contact.comments) {
                if (comment.post_creator_username === username) {
                    totalComments += 1;
                }
            }
            let totalCommentsEmbedded = await mongoose.connection.db.collection('posts').aggregate(
                [
                    {
                        $match: {
                            '$and':[
                                {'username': username}, 
                                {'comments.1':{
                                    $exists: false 
                                }},
                                {'comments.0':{
                                    $exists: true 
                                }}
                            ]
                        }
                    },
                    {
                        $project: {'comments':1}
                    },
                    {
                        $unwind: "$comments"
                    },
                    {
                        $unwind: "$comments.comments"
                    },
                    {
                        $match: {'comments.comments.user.username': contact.username}
                    },
                    {
                        $count: "total_embedded_comments"
                    }
                ], { allowDiskUse: true }
            ).toArray();
            let comments = ( totalComments + (totalCommentsEmbedded[0]) ? totalCommentsEmbedded[0].total_embedded_comments:0);
            let score = ( (totalLikes * this.FAN_POINTS.like) + (comments * this.FAN_POINTS.comment) );
            this.utils.writeLog("Found: " + totalLikes + " total likes, " + comments + " total comments for " + contact.username + " " + c + "/" + contacts.length);
            let engagementObject = {
                'total_likes': totalLikes,
                'total_comments': comments,
                'total_score': score,
                'engagement_with': username,
                'timestamp': this.utils.timestampToDate(Date.now() / 1000)
            };
            contact.interacted_with.push(engagementObject);
            let savedPerson = await this.utils.createOrUpdate(User, "username", contact.username, contact);
            this.utils.writeLog("Saved contact: " + savedPerson.username + " with score: " + score);
            try {
                contactsData.push(
                    [
                        contact.username,
                        contact.profile_pic,
                        score,
                        totalLikes,
                        comments,
                        (contact.full_name)?contact.full_name.replace( /[\r\n]+/gm, ' ' ).replace(',',' ').replace('"', '').replace("'",''):"",
                        contact.follower_count,
                        contact.email,
                        contact.phone
                    ].join(',')
                );
                c += 1;
            } catch (err) {
                this.utils.writeLog("Error in pushing data: ");
                console.log(err, contact.username);
            }
        }
        
        let fn2 = username + "_contacts_interacted_with_" + this.utils.timestampToDate(Date.now() / 1000) + ".csv";
        //this.utils.writeLog("Wrote file: " + fn2);
        
        await this.utils.writeFile(fn2, contactsData.join('\n'));
        this.utils.writeLog("processed: " + contactsData.length+" entries. Wrote file: " + fn2);
        return contactsData.length;
    }

    /**
     * Extracts most engaging labels after image analysis has been performed.
     * @param username detail of profile to analyse, string.
     */
    async calculateMostEngagingLabels (username: string) {
        let posts = await this.profile.getUserSavedPosts(username);
        let user = await this.utils.find(User, "username", username);
        let labels = {};
        for (let post of posts) {
            if (post.engagement === undefined) {
                post.engagement = ((post.comment_count + post.like_count) / user.follower_count) * 100;
                let savedPost = await this.utils.createOrUpdate(Post, "post_id", post.post_id, post);
                this.utils.writeLog("Calculated post engagement for: " + savedPost.post_id + " of " + savedPost.engagement);
                
            }
            if (post.image_analysis) {
                for (let imgAnalysis of post.image_analysis) {
                    if (imgAnalysis['analysis'] !== null) {
                        let postImageLabels = imgAnalysis['analysis'].labels.Labels;
                        labels = this.image_analysis.processLabels(postImageLabels, labels, post, "labels");
                        let postCelebrities = imgAnalysis['analysis'].celebrities.CelebrityFaces;
                        labels = this.image_analysis.processLabels(postCelebrities, labels, post, "celebrities");
                        let postImageText = imgAnalysis['analysis'].text.TextDetections;
                        labels = this.image_analysis.processLabels(postImageText, labels, post, "text");
                        let postFaces = imgAnalysis['analysis'].faces.FaceDetails;
                        labels = this.image_analysis.processLabels(postFaces, labels, post, "faces");
                    }
                    
                }
            }
        }

        user.image_labels = {
            "labels": labels, 
            "timestamp": this.utils.timestampToDate(Date.now()/1000)
        }
        let savedUser = await this.utils.createOrUpdate(User, "username", user.username, user);
        await this.exportLabelsEngagementToCSV(savedUser.username);
        
        // TODO: Covnert dict to CSV
        return labels;
    }

    /**
     * Exports already calculated most engaging lables of a users to CSV.
     * @param username identifier of profile to extract most used tags in posts, string.
     */
    async exportLabelsEngagementToCSV (username: string) {
        let user = await this.utils.find(User, 'username', username);
        if (user.image_labels) {
            let labelsEngagement = <any>[[
                "Label",
                "Engagement",
                "Occurrences",
                "Type",
                "Date"
            ].join(',')];
            let latestCalculatedLabels = user.image_labels[user.image_labels.length - 1];
            for (let label of Object.keys(latestCalculatedLabels.labels)) {
                let values = latestCalculatedLabels.labels[label];
                const sum = values.reduce((a:any, b:any) => a + b, 0);
                const avgEngagement = (sum / values.length) || 0;
                let name = label.split("__")[1];
                let labelType = label.split("__")[0];
                if (labelType === "celebrities" && values.length <= this.CONFIG.min_feature_occurrences) {
                    // Do nothing....
                } else if (labelType === "text") {
                    // Don't export
                } else {
                    labelsEngagement.push([
                        name,
                        avgEngagement,
                        values.length,
                        labelType,
                        latestCalculatedLabels.timestamp
                    ].join(','));
                }
                
            }
            let fn = user.username + "_labels_engagement_" + this.utils.timestampToDate(Date.now() / 1000) + ".csv";
            await this.utils.writeFile(fn, labelsEngagement.join('\n'));
            this.utils.writeLog("Wrote: " + fn);
            return fn;
        } else {
            this.utils.writeLog("No image_labels on user " + user.username);
        }

    }

    /**
     * Calculates unique users posting posts with a specific tag.
     * @param tag key to use to build index of unique posters, string.
     */
    async uniqueTagPosters (tag: string, maxBatchSize: number = 1000) {
        this.utils.writeLog("Analysing users of tag " + tag);
        let hashtag = await this.utils.find(Hashtag, "name", tag);
        
        
        let results = <any>[];
        if (hashtag) {
            this.utils.writeLog("Found " + hashtag.posts.length + " posts for " + tag);
            //console.log(hashtag.posts)
            let docs = [];
            const fullPostsQuery = await mongoose.connection.db.collection('posts').aggregate([
                {
                    "$match": {
                        "post_id":{ 
                                "$in": hashtag.posts
                            }  
                        }
                },
                {
                    "$project" : {
                        "comment_count": 1, "username": 1, "user_id": 1, "like_count": 1, "engagement": 1, "timestamp": 1,
                    }
                },
                {
                    "$group": {
                        _id: { 
                            user_id: "$user_id", 
                            username: ('$username') 
                        }, 
                        count: {
                            $sum: 1
                        }, 
                        total_likes: {
                            $sum: "$like_count"
                        },
                        total_comments: {
                            $sum: "$comment_count"
                        }

                    }
                },
                {
                    "$sort": {
                        "count": -1
                    }
                }
            ], { allowDiskUse: true, cursor: { batchSize: maxBatchSize} });
            while (await fullPostsQuery.hasNext()) {
                docs.push(await fullPostsQuery.next());
                if (docs.length >= maxBatchSize) {
                    results = results.concat([...docs]);
                    docs = [];
                    
                }
            }
            if (results.length === 0) {
                results = results.concat(await fullPostsQuery.toArray())
            }
            //console.log(results)
            this.utils.writeLog("Total posters for "+ tag + ": "+results.length);
            for (let i = 0; i < results.length; i++) {
                let poster = results[i];
                // Update posters with missing username
                if (!poster._id.username) {
                    this.utils.writeLog("Getting username for " + poster._id.user_id);
                    let foundPerson = await this.utils.find(User, "user_id", poster._id.user_id);
                    let username;
                    if (!foundPerson || !foundPerson.username) {
                        
                        let usernameResponse = await this.profile.getUsernameFromId(poster._id.user_id, false);
                        if (!usernameResponse.data) {
                            // Use proxy if without the request failed.
                            usernameResponse = await this.profile.getUsernameFromId(poster._id.user_id);
                        }
                        //console.log(usernameResponse.data.user.reel.user.username);
                        username = usernameResponse.data.user.reel.user.username;
                        let userProfilePic = usernameResponse.data.user.reel.user.profile_pic_url;
                        try {
                            // If any username is found, then update the user by the userid
                            let savedPerson = await this.utils.createOrUpdate(User, "user_id", poster._id.user_id, {
                                username: username,
                                profile_pic: userProfilePic,
                                user_id: poster._id.user_id
                            });
                            this.utils.writeLog("Saved username: " + savedPerson.username);
                        } catch (err) {
                            this.utils.writeLog("Error in tag op saving username for new person: " + poster._id.user_id);
                            this.utils.writeLog(err);
                        }
                    } else {
                        username = foundPerson.username;
                    }
                    results[i]._id.username = username;
                    this.utils.writeLog("Set username for tag poster: " + results[i]._id.username + " " + username);
                }
            }
            if (!hashtag.posters) {
                hashtag.posters = [
                    {
                        "users": results,
                        "timestamp": this.utils.timestampToDate(Date.now() / 1000),
                        "analysed_posts": hashtag.posts
                    }
                ];
            } else {
                hashtag.posters.push({
                    "users": results,
                    "timestamp": this.utils.timestampToDate(Date.now() / 1000),
                    "analysed_posts": hashtag.posts
                });
            }
            if (hashtag.posters.length > 0) {
                let savedHashtag = await this.utils.createOrUpdate(Hashtag, "name", hashtag.name, hashtag);
                this.utils.writeLog("Saved " + savedHashtag.posters.length + " posters for " + savedHashtag.name);
                // TODO: Export tag posters to CSV
                let saved = await this.exportSortedTagPostersToCSV(hashtag.name);
                console.log("Saved posters file for " + tag + " " + saved);
            }
        } else {
            this.utils.writeLog("Tag " + tag + " not found");
        }
        return results;
    }

    async exportSortedTagPostersToCSV (tag: string) {
        this.utils.writeLog("Exporting top posters for " + tag);
        let hashtag = await this.utils.find(Hashtag, "name", tag);
        if (hashtag) {
            if (!hashtag.posters) {
                this.utils.writeLog("Top posters not found for " + tag + " returning...");
                return;
            } else {
                let latestPosters = hashtag.posters[hashtag.posters.length - 1].users;
                let totalPostsForTag;
                try {
                    totalPostsForTag = <number>hashtag.total_posts[hashtag.total_posts.length - 1].posts;
                } catch (err) {
                    totalPostsForTag = 0;
                }
                    
                
                
                let postersList = [
                    [
                        "User Id",
                        "Username",
                        "Total posts",
                        "Total likes",
                        "Total comments",
                        "% of total analysed posts",
                        "% of tag posts"
                    ].join(",")
                ];
                let totalCountedPosts = 0;
                for (let poster of latestPosters) {
                    let postsByUserPercentageOfTotalAnalysedPosts = (poster.count / hashtag.posts.length) * 100;
                    let postsByUserPercentageOfTotalPosts = (poster.count / totalPostsForTag) * 100;
                    postersList.push([
                        poster._id.user_id,
                        poster._id.username,
                        poster.count,
                        poster.total_likes,
                        poster.total_comments,
                        postsByUserPercentageOfTotalAnalysedPosts,
                        postsByUserPercentageOfTotalPosts
                    ].join(","));
                    totalCountedPosts += poster.count;
                }
                let percentageOfTotalCountedPostsOfTotalPosts = (totalCountedPosts / totalPostsForTag) * 100;
                postersList.push([
                    "Total users:",
                    latestPosters.length,
                    totalCountedPosts,
                    totalPostsForTag.toString(),
                    percentageOfTotalCountedPostsOfTotalPosts.toString()
                ].join(","));
                postersList.push("Related tags");
                postersList.push(hashtag.related_tags.join("\n"));
                let fn = tag + "_posters_" + this.utils.timestampToDate(Date.now() / 1000) + ".csv";
                await this.utils.writeFile(fn, postersList.join('\n'));
                let msg = "Saved file:: " + fn + " with " + (postersList.length - 2).toString();
                this.utils.writeLog(msg);
                return fn;
            }
        }
    }
}