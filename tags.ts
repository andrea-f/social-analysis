import { User, Post, Hashtag } from './db-schema';
import { IgApiClient } from 'instagram-private-api';
import { CONFIG, ANALYSIS_CONFIG } from './config.interface';
import {Utils} from './utils';
import { ProfileAnalysis } from './profile';
import { PostAnalysis } from './post';
import mongoose from 'mongoose';

export class Tags {

    constructor () {
        this.utils = new Utils;
        this.IG = new IgApiClient();
        this.CONFIG = this.utils.CONFIG;
        this.ANALYSIS_CONFIG = this.utils.ANALYSIS_CONFIG;
        this.profile = new ProfileAnalysis();
        this.post = new PostAnalysis;
    }

    utils: Utils;
    IG: IgApiClient;
    CONFIG: CONFIG;
    ANALYSIS_CONFIG: ANALYSIS_CONFIG;
    profile: ProfileAnalysis;
    post: PostAnalysis;

    /**
     * Saves post and related information after having extracted it from the explore page.
     * @param post contains information on the retrieved post, object.
     */
    async savePostAndCreateProfileFromExploreTag (post: object) {
        if (post['node']['id']) {
            let personFound = await this.utils.find(Post, "post_id", post['node']['id']);
            if (personFound) {
                this.utils.writeLog("Post for tag already processed: " + post['node']['id']);
                return;
            }
        }
        let person = new User();
        let postData = post['node'];
        let userId = postData.owner.id;
        person.user_id = userId;
        let personFound = await this.utils.find(User, "user_id", userId);
        if (personFound) {
            //console.log("Found user " + personFound.username);
            person = personFound;
        } else {
            let profileData = await this.profile.getUsernameFromId(userId);
            let username = profileData['data']['user']['reel']['owner']['username'];
            person.username = username;
            person.profile_pic = profileData['data']['user']['reel']['owner']['profile_pic_url'];
            postData.owner.username = username;
            person.save();
            //this.writeLog("Saved username " + username + " for user_id: " + userId);
        }
        // TODO: How to avoid this call??
        let profilePosts = <any>[];
        if (!person.follower_count) {
            let userProfile = <any>await this.profile.getProfileAnonymousQuery(person.username);
            userProfile = userProfile['graphql']['user'];
            let totalPosts = userProfile['edge_owner_to_timeline_media']['count'];
            if (userProfile['edge_followed_by']['count'] !== person.follower_count || person.following_count !== userProfile['edge_follow']['count']) {
                person = await this.profile.updateFollowersFollowingCounts(person);
            }
            person.follower_count = userProfile['edge_followed_by']['count'];
            person.following_count = userProfile['edge_follow']['count'];
            profilePosts = userProfile['edge_owner_to_timeline_media']['edges'];
            person.media_count = totalPosts;
        }
        profilePosts.push(postData);
        let personWithProfilePosts = await this.post.processPosts(profilePosts, person, true, true);
        //this.writeLog(this.totalQueries + " Processed for " + personWithProfilePosts.username + " " + profilePosts.length + " posts, total posts for user: " + personWithProfilePosts.posts.length);
        
        return personWithProfilePosts;
    }

    /**
     * Saves posts containing and hashtag.
     * @param tag word to search and retrieve from the explore page, string.
     */
    async getExplorePagePostsByTag (tag: string) {
        this.utils.writeLog("Fetching tag " + tag + " in explore...");
        let initialExplorePageResponse = await this.getExplorePageAnonymousQuery(tag);
        if (!initialExplorePageResponse.graphql) {
            this.utils.writeLog("Tag " + tag + " not found.");
            return;
        }
        let explorePosts = initialExplorePageResponse.graphql.hashtag;
        let hashtag = new Hashtag();
        hashtag.tag_id = explorePosts.id;
        hashtag.name = explorePosts.name;
        hashtag.description = explorePosts.description;
        hashtag.profile_pic = explorePosts.profile_pic_url;
        hashtag.info = explorePosts;
        let totalPostsForTag = explorePosts.edge_hashtag_to_media.count;
        this.utils.writeLog("Found hashtag: " + hashtag.tag_id + " " + hashtag.name + " total posts: " + totalPostsForTag);
        let hashtagFound = await this.utils.find(Hashtag, "name", hashtag.name);
        if (hashtagFound) {
            hashtag = hashtagFound;
        } else {
            await hashtag.save();
        }
        hashtag.total_posts.push({
            posts: totalPostsForTag,
            timestamp: new Date().getTime()/1000
        });
        let relatedTags = explorePosts.edge_hashtag_to_related_tags.edges;
        for (let relatedTagObject of relatedTags) {
            let relatedTag = relatedTagObject.node.name;
            //this.writeLog("Processing related tag: " + relatedTag);
            let hashtagRelatedFound = await this.utils.find(Hashtag, "name", relatedTag);
            if (hashtagRelatedFound) {
                if (hashtagRelatedFound.related_tags.indexOf(relatedTag) === -1) {
                    hashtagRelatedFound.related_tags.push(relatedTag);
                    // TODO: Use increments with mongodb
                    let savedHashtag = await this.utils.createOrUpdate(Hashtag, "name", hashtagRelatedFound.name, hashtagRelatedFound);
                }
            } else {
                let newHashtag = new Hashtag();
                newHashtag.name = relatedTag;
                ///this.writeLog("Saving related tag: " + relatedTag);
                newHashtag.save();
                newHashtag.related_tags.push(hashtag.name);
            }
            if (hashtag.related_tags.indexOf(relatedTag) === -1 && relatedTag !== tag) {
                hashtag.related_tags.push(relatedTag);
            }
        }
        let savedHashtag = await this.utils.createOrUpdate(Hashtag, "name", hashtag.name, hashtag);
        let posts = explorePosts.edge_hashtag_to_media.edges.concat(explorePosts.edge_hashtag_to_top_posts.edges);
        // Initial call
        let c = 0;
        for (let post of posts) {
            this.utils.writeLog("Processing post " + post['node']['shortcode'] + " for tag " + tag + " " + c + "/" + totalPostsForTag);
            try {
                let savedPersonWithPost = await this.savePostAndCreateProfileFromExploreTag(post);
            } catch (err) {
                this.utils.writeLog("Error in exploring tag in getting post");
                this.utils.writeLog(err);
            }
            c += 1;
            if (hashtag.posts.indexOf(post['node']['id']) === -1) {
                hashtag.posts.push(post['node']['id']);
                savedHashtag = await this.utils.createOrUpdate(Hashtag, "name", hashtag.name, hashtag);
                this.utils.writeLog("Saved hashtag (" +savedHashtag.name + ") with " + savedHashtag.posts.length + " posts."); 
            }
               
        }
        
        // TODO: Add loading status here
        //console.log("Has next page: " + explorePosts.edge_hashtag_to_media.page_info.has_next_page + " tag " + tag);
        while (explorePosts.edge_hashtag_to_media.page_info.has_next_page === true) {
            try {

                explorePosts = <any>await this.getExplorePageMorePostsAnonymousQuery(tag, explorePosts.edge_hashtag_to_media.page_info.end_cursor);
                explorePosts = (explorePosts.graphql) ? explorePosts.graphql.hashtag: explorePosts.data.hashtag;
                //console.log(explorePosts);
                let posts = explorePosts.edge_hashtag_to_media.edges;// + explorePageTagFeed.edge_hashtag_to_top_posts.edges;
                for (let post of posts) {
                    //this.writeLog("Processing More post " + post['node']['shortcode'] + " for tag " + tag + " " + c + "/" + totalPostsForTag);
                    let savedPersonWithPost = await this.savePostAndCreateProfileFromExploreTag(post);
                    c += 1;
                    if (hashtag.posts.indexOf(post['node']['id']) === -1) {
                        hashtag.posts.push(post['node']['id']);
                    }
                    if (savedPersonWithPost) {
                        if (!hashtag.usernames) {
                            hashtag.usernames = {};
                        }
                        if (hashtag.usernames[savedPersonWithPost.username]) {
                            if (hashtag.usernames[savedPersonWithPost.username]['post_ids'].indexOf(post['node']['id']) === -1) {
                                hashtag.usernames[savedPersonWithPost.username]['post_ids'].push(post['node']['id']);
                                hashtag.usernames[savedPersonWithPost.username]['count'] += 1;
                            }
                        } else {
                            hashtag.usernames[savedPersonWithPost.username] = {
                                post_ids: [post['node']['id']],
                                count: 1
                            }
                        }
                    }
                }
                await this.utils.createOrUpdate(Hashtag, "name", hashtag.name, hashtag);
                let data = {
                    id: hashtag.tag_id,
                    nextMaxId: explorePosts.edge_hashtag_to_media.page_info.end_cursor,
                    timestamp: Date.now()
                };
                await this.utils.setStatus(data, "hashtags");
                this.utils.writeLog(posts.length + " more posts, total processed: " + c + "/"+ totalPostsForTag +" for " + tag);
                if (c >= this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].analysis.limits.max_posts_per_tag) {
                    break;
                }
            } catch (err) {
                this.utils.writeLog("ERROR in ferching more posts anon");
                this.utils.writeLog(err);
            }
        }
        this.utils.writeLog("Processed totally: " + savedHashtag.posts.length + " posts for "  + hashtag.name);
        return savedHashtag;
    }

    /**
     * Fetches more posts for a tag from the explore page.
     * @param tag word to search and retrieve from the explore page, string.
     * @param endCursor unique alphanumeric text with next page of results, string.
     */
    async getExplorePageMorePostsAnonymousQuery (tag: string, endCursor: string) {
        let url = "https://www.instagram.com/graphql/query/?query_hash=7dabc71d3e758b1ec19ffb85639e427b&variables=%7B%22tag_name%22%3A%22" + tag + "%22%2C%22first%22%3A200%2C%22after%22%3A%22" + encodeURI(endCursor) + "%22%7D";
        let query = "curl";
        let commands = [
            url, 
            '-H', 'authority: www.instagram.com', 
            '-H', 'cache-control: max-age=0', 
            '-H', 'upgrade-insecure-requests: 1', 
            '-H', 'sec-fetch-user: ?1', 
            '-H', 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9', 
            '-H', 'sec-fetch-site: none', 
            '-H', 'sec-fetch-mode: navigate', 
            '-H', 'accept-encoding: gzip, deflate, br', 
            '-H', 'accept-language: en-GB,en-US;q=0.9,en;q=0.8,it;q=0.7', 
            '--compressed'
        ];
        let results = <any>await this.utils.getAnonymousQuery(query, commands);
        if (!results["graphql"] && !results["data"]) {
            try {
                this.utils.writeLog(JSON.stringify(results));
            } catch (err) {
                this.utils.writeLog(results); 
            }
        } else {
            return results;
        }

    }

    /**
     * Fetches first page of results for a tag from the explore page.
     * @param tag word to search and retrieve from the explore page, string.
     */
    async getExplorePageAnonymousQuery (tag: string) {
        //const { exec } = require("child_process");
        let url = "https://www.instagram.com/explore/tags/" + tag + "/?__a=1";
        let query = "curl";
        let commands = [
            url, 
            '-H', 'authority: www.instagram.com', 
            '-H', 'cache-control: max-age=0', 
            '-H', 'upgrade-insecure-requests: 1', 
            '-H', 'sec-fetch-user: ?1', 
            '-H', 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9', 
            '-H', 'sec-fetch-site: none', 
            '-H', 'sec-fetch-mode: navigate', 
            '-H', 'accept-encoding: gzip, deflate, br', 
            '-H', 'accept-language: en-GB,en-US;q=0.9,en;q=0.8,it;q=0.7', 
            '--compressed'
        ];
        let results = <any>await this.utils.getAnonymousQuery(query, commands);
        if (!results["graphql"]) {
            this.utils.writeLog("Tag: " + tag + " not found.");
            //this.writeLog(results); 
        } else {
            return results;
        }
    }

    /**
     * Gets full info for saved posts.
     * @param tag hashtag to get posts for, string.
     * @param maxBatchSize total number of results per query, number
     */
    async getHashtagSavedPosts (tag: string, maxBatchSize: number = 1000) {
        this.utils.writeLog("Calculating hashtag engagement of tag " + tag);
        let hashtag = await this.utils.find(Hashtag, "name", tag);
        let results = <any>[];
        if (hashtag) {
            this.utils.writeLog("Found " + hashtag.posts.length + " posts for " + tag);
            //console.log(hashtag.posts)
            let docs = [];
            const fullPostsQuery = await mongoose.connection.db.collection('posts').aggregate([
                {
                    "$match": {
                        "post_id":{ 
                                "$in": hashtag.posts
                            }  
                        }
                },
                {
                    "$project" : {
                        "comment_count": 1, "username": 1, "user_id": 1, "like_count": 1, "engagement": 1, "timestamp": 1, "post_id": 1, "media": 1, "text": 1
                    }
                }
            ], { allowDiskUse: true, cursor: { batchSize: maxBatchSize} });
            
            /*while (await fullPostsQuery.hasNext()) {
                docs.push(await fullPostsQuery.next());
                if (docs.length >= maxBatchSize) {
                    results = results.concat([...docs]);
                    docs = [];
                }
            }*/
            for await (const post of fullPostsQuery) {
                results.push(post);
            }
            this.utils.writeLog("Fetched total: " + results.length + " for " + tag);

        }
        return results;
    }

    async getLabelsForHashtag (tag: string) {
        
    }
}