import {SocialAnalysis} from './social-analysis';
import { Tags } from './tags';

async function batchAnalysis () {
    let environment = 0;
    let commands = <any>[];
    try {
        commands = process.argv.slice(3);
        environment = parseInt(process.argv[2]);
    } catch (err) {
        commands = process.argv.slice(2);
    }
    console.log("[batchAnalysis] environment: " + environment, "commands: ", commands);
    let action, username, tag;
    let sa = new SocialAnalysis();
    let tags = new Tags();
    sa.utils.connectToDB(environment);
    if (commands.length > 0) {
        action = commands[0];
        if (action === "bulkuserinfo") {
            console.log("[batchAnalysis] Doing bulkuserinfo for all profiles in db!");
            await sa.profile.populateAllUserInfo();
            console.log("[batchAnalysis] Finished doing bulkuserinfo for all profiles in db!");
            await sa.utils.writeLog("[batchAnalysis] Finished doing bulkuserinfo for all profiles in db!");
            return;
        }
        if (typeof commands[1] !== "undefined") {
            tag = commands[1];
        } else {
            console.log("[batchAnalysis] No specific input either profile or tag");
            return;
        }
        if (action === "explore") {
            await tags.getExplorePagePostsByTag(tag);
            console.log("Finished exploring tag: " + tag);
        } else if (action === "posters") {
            console.log("Starting extracting posters for " + tag);
            await sa.stats.uniqueTagPosters(tag);
            console.log("Finished extracting posters for " + tag);
        } else if (action === "tagengagement") {
            console.log("Calculating tag engagement for "+ tag);
            await sa.stats.calculatePostsEngagement(tag, "hashtag");
            await sa.stats.exportPostsEngagementToCSV(tag, "hashtag");
            console.log("Finished exporting tags and posts for "+ tag);
        } else if (action === "tagrek") {
            await sa.image_analysis.getPostsImagesAnalysis(tag, "hashtag");
            //TODO: await sa.stats.calculateMostEngagingLabels(tag, "hashtag");
        } else {
            if (typeof commands[1] !== "undefined") {
                username = commands[1];
            } 
            if (action === "all") {
                await sa.analyseUser(username);
            } else {
                await sa.analyseUser(username,[action]);
            }
        }
    }
}
batchAnalysis();