
export class BatchAnalysis {
    constructor () {
        
    }

    async getUserInfoList (usernames: string[]) {
        let c = 0;
        for (let username of usernames) {
            //this.writeLog("Processing " + username);
            try {
                let user = await this.getUserInfo(username);
                this.writeLog("Processed user: " + user.username + " " + c + "/" + usernames.length);
                c += 1;
            } catch (err) {
                this.writeLog("Error" + err);
            }
        }
        this.writeLog("Total processed users: " + c);
        return c;
    }

    /**
     * Analyses a list of users defined in profiles_to_analyse.config.json.
     * @param action process to perform on a set of profile or on a specific one, string.
     * @param username identifier for user to export data of, string.
     */
    async analyseBatchUsernames (action: string = "", username: string = "") {
        //let usernames = Object.keys(this.ANALYSIS_CONFIG.profiles);
        // Create items array 
        let profiles = this.ANALYSIS_CONFIG.profiles;
        let items: Array<any> = [];
        // TODO: Create default config if it doesnt exist in the config profiles file.
        if (username.length > 0 && this.ANALYSIS_CONFIG.profiles[username]) {
            items = [ [username,0] ];
        } else {
            items = Object.keys(profiles).map(function(key) {
                return [key, profiles[key].order];
            });
            
            // Sort the array based on the second element
            items.sort(function (first, second) {
                return second[1] - first[1];
            });
        }
        
        let count = 0;
        for (let usernameToAnalyse of items) {
            
            try {
                
                this.writeLog("Analysing user: " + usernameToAnalyse[0]);
                if (action.length > 0) {
                    await this.analyseUser(usernameToAnalyse[0], [action]);
                } else {
                    await this.analyseUser(usernameToAnalyse[0]);
                }
                count += 1;
            } catch (err) {
                this.writeLog("Error in batch analysis for " + usernameToAnalyse);
                console.log(err);
                this.writeLog(err);
                if (this.currentRetries <= this.maxRetries) {
                    await this.analyseBatchUsernames(action, usernameToAnalyse[0]);
                }
            }
        }
        return count;
    }
}