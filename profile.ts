import {User, IUser, Post} from './db-schema';
import { IgApiClient } from 'instagram-private-api';
import { CONFIG, ANALYSIS_CONFIG } from './config.interface';
import {Utils} from './utils';
import mongoose from 'mongoose';


export class ProfileAnalysis {

    constructor () {
        this.utils = new Utils();
        this.IG = new IgApiClient();
        this.CONFIG = this.utils.CONFIG;
        this.ANALYSIS_CONFIG = this.utils.ANALYSIS_CONFIG;
        this.logged_in = false;
        this.THIRD_PARTY_PROVIDERS_CONFIG = "./thirdpartyproviders.json";
        
    }

    utils: Utils;
    IG: IgApiClient;
    CONFIG: CONFIG;
    ANALYSIS_CONFIG: ANALYSIS_CONFIG;
    logged_in: boolean;
    THIRD_PARTY_PROVIDERS_CONFIG: string;

    /**
     * Logs user in Instagram.
     * Returns authorized client object.
     */
    async login (index: number = 0, usePostingUsersList: boolean = false, postingUsername: string = "", postingPassword: string = "") {
        let username: string;
        let password: string;
        if (!usePostingUsersList && (index < this.CONFIG.expandables.length)) {
            username = this.CONFIG.expandables[index].username;
            password = this.CONFIG.expandables[index].password;
        } else if (usePostingUsersList && postingUsername.length !== 0 && postingPassword.length !== 0) {
            username = postingUsername;
            password = postingPassword;
        } else {
            this.utils.writeLog("[profile.login] Couldn't log in");
            return;
        }
        try {
            this.IG.state.generateDevice(username);
            await this.IG.simulate.preLoginFlow();
            this.utils.writeLog("[profile.login] Logging in as " + username);
            const auth = await this.IG.account.login(username, password);
            process.nextTick(async () => await this.IG.simulate.postLoginFlow());
            this.utils.writeLog("[profile.login] Logged in as " + auth.username + " ...");
            this.logged_in = true;
            
            return auth;
        } catch (err) {
            this.utils.writeLog("[profile.login] Error in logging in " + username);
            console.log(err);
            
            
            index += 1;
            if (this.IG.state.checkpoint && this.IG.state.checkpoint.message === 'challenge_required') {
                this.utils.writeLog(username, this.IG.state.checkpoint); // Checkpoint info here
                await this.IG.challenge.auto(); // Requesting sms-code or click "It was me" button
                this.utils.writeLog("after auto completion", this.IG.state.checkpoint);
            }
            if (index < this.CONFIG.expandables.length) {
                await this.login(index);
            }
        }
    }

    /**
     * Returns the logged in instance.
     */
    getIgApiClient () {
        return this.IG;
    }

    /**
     * Tries to get user info for all the users in the db.
     */
    async populateAllUserInfo (username: string = "", customSleep: number = 3.3, skip: number = 0, totalProcessed: number = 0) {
        this.utils.connectToDB();
        while (this.logged_in === false) {
            await this.login();
        }        
        let limit = this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].analysis.limits.user_info_batch_size;
        let queryParam;
        if (username.length === 0) {
            queryParam = [
                {
                    $match: {
                        info: {
                            $exists: false
                        }
                    }
                },
                {
                    $skip: skip
                },
                {
                    $limit: limit
                },
                {
                    $project : {
                        username: 1, 
                        _id: 0, 
                        user_id: 1, 
                        info: 1
                    }
                }
            ];
        } else {
            queryParam = [
                {
                    $match: {
                        $and: [
                            {
                                info: {
                                    '$exists': false
                                }
                            },
                            {
                                $or: [
                                    {
                                        'liked.post_creator_username': username
                                    },
                                    {
                                        'comments.post_creator_username': username
                                    }
                                ]
                            }
                        ]
                    }
                },
                {
                    $skip: skip
                },
                {
                    $limit: limit
                },
                {
                    $project : {
                        username: 1, _id: 0, user_id: 1, info: 1
                    }
                }
            ];
        }
        this.utils.writeLog(
            "[profile.populateAllUserInfo] " + JSON.stringify(queryParam, null, 4)
        );

        const queryResults = await this.utils.db.collection('users').aggregate(
            queryParam,  
            { 
                allowDiskUse: true, 
                //cursor: { batchSize: maxUsersInfoBatchSize} 
            }
        ).toArray();
        
        this.utils.writeLog("[profile.populateAllUserInfo] total retrieved users for " + username + " " + queryResults.length );
        for (const user of queryResults) {
            await this.getUserInfo(user.username, user.user_id, customSleep);
            totalProcessed += 1;
        }
        this.utils.writeLog("[profile.populateAllUserInfo] Processed " + totalProcessed + ' profiles.');
        if (queryResults.length > 0) {
            await this.utils.delay(customSleep * 2);
            await this.populateAllUserInfo(username, customSleep, skip + limit, totalProcessed);
        } else {
            return totalProcessed;
        }
        
        
    }



    /**
     * Creates the commands list for fetching a profile, not authed.
     * @param username identifier for user to export data of, string.
     */
    async getProfileAnonymousQuery (username: string) {
        let query = "curl";
        let dataResponse = "";
        let profileData = {};
        let thirdPartyProviders = await require(this.THIRD_PARTY_PROVIDERS_CONFIG);
        this.utils.writeLog("Total providers found: " + thirdPartyProviders.providers.length);
        for (let provider of thirdPartyProviders.providers) {
            let commands = provider.commands;
            if (provider.preflight) {
                let preflightCommands = provider.preflight;
                for (let i=0; i < preflightCommands.length; i++) {
                    preflightCommands[i] = preflightCommands[i].replace(":USERNAME:", username);
                }
                //console.log(preflightCommands);
                let preflightResponse = <any>await this.utils.getAnonymousQuery(query, preflightCommands, false);
                ///console.log(preflightResponse);
                let data = this.utils.extractUrlFromText(preflightResponse);
                let locationUrl = data.url;
                let cookie = data.cookie;
                //this.writeLog("locationUrl " + locationUrl + "\ncookie: " + cookie);
                for (let i=0; i < commands.length; i++) {
                    commands[i] = commands[i].replace(":LOCATIONURL:", locationUrl);
                    commands[i] = commands[i].replace("\r", "");
                }
                if (cookie.length > 0) {
                    commands.push("-H", "cookie: " + cookie);
                }
                //console.log(commands);
            } else {
                for (let i=0; i < commands.length; i++) {
                    commands[i] = commands[i].replace(":USERNAME:", username);
                    commands[i] = commands[i].replace(":ENCODEDUSERNAME:", encodeURI("https://www.instagram.com/"+username+"/?__a=1"));
                }
            }
            let results;
            try {
                results = <any>await this.utils.getAnonymousQuery(query, commands, true);
                //this.writeLog(results);
                //this.writeLog("showed results");
            } catch (err) {
            }
            if (results) {
                //this.writeLog("Type of result for " + provider.name + ": " + typeof results);
                try {
                    if (provider.path.length === 1) {
                        dataResponse = results[provider.path[0]];
                    } else if (provider.path.length === 2) {
                        dataResponse = results[provider.path[0]][provider.path[1]];
                    } else {
                        dataResponse = results;
                    }
                    
                    if (typeof dataResponse === "object") {
                        profileData = dataResponse;
                    } else {
                        profileData = JSON.parse(dataResponse);
                    }
                    
                    this.utils.writeLog("Profile fetch succeded for " + username + " using " + provider.name);
                    break;
                } catch (err) {
                    this.utils.writeLog("Error in parsing to JSON for " + provider.name + " processing: " + username);
                }
            } else {
                this.utils.writeLog(results);
                this.utils.writeLog("Profile requested failed for: " +username + ", provider: "+ provider.name);
            }
        }
        if (!profileData['graphql']) {
            this.utils.writeLog("Failed all providers for fetching: " + username);
            throw "FAILED FETCHING PROFILE " + username;
        }   
        return profileData;
    }

    /**
     * Fetches all public information on a user or also private for logged in user.
     * @param username Identifier for user to get more info on.
     * @param pk Private Key or user id.
     */
    async getUserInfo (username: string, pk: number = 0, customSleep: number = 0) {
        this.utils.writeLog("Checking user info for " + username);
        
        if (this.logged_in === false) {
            const auth = await this.login();
        }
        
        let user = await this.utils.find(User, "username", username);
        if (pk === 0 && username.length > 0) {
            try {
                const igAccount = await this.IG.user.searchExact(username);
                pk = igAccount.pk;
            } catch (err) {
                this.utils.writeLog("Error in searching exact username: " + username);
                this.utils.writeLog(err);
                throw "Username " + username + " not found!";
            }
        }
        if (!user) {
            user = new User();
            user.username = username;
            user.user_id = pk;
            user.save();
            //this.writeLog("Creating new user info for " + pk + " " + username);
        }
        
        if (!user.info) {
            let userInfo;
            this.utils.writeLog("Getting user info for " + pk + " " + username);
            try {
                userInfo = await this.IG.user.info(pk);
            } catch (err) {
                this.utils.writeLog("Error userinfo: " + err.toString());
            }
            if (userInfo) {
                if (userInfo['public_email'] && userInfo['public_email'].length > 0) {
                    user.email = userInfo['public_email'];
                }
                if (userInfo['public_phone_number'] && userInfo['public_phone_number'] > 0) {
                    user.phone = userInfo['public_phone_number'];
                }
                if (userInfo['full_name']) {
                    user.full_name = userInfo['full_name'];
                }
                user.follower_count = userInfo['follower_count'];
                user.following_count = userInfo['following_count'];
                user = await this.updateFollowersFollowingCounts(user);
                user.profile_pic = userInfo['profile_pic_url'];
                user.media_count = userInfo['media_count'];
                user.info = userInfo;
                user = await this.utils.createOrUpdate(User, "username", user.username, user);
                if (customSleep === 0) {
                    await this.utils.delay(this.utils.SLEEP_TIMEOUT);
                } else if (customSleep > 0) {
                    await this.utils.delay(customSleep);
                }
            }
            
        } else {
            this.utils.writeLog("User " + user.username + " already processed.");
        }
        return user;
    }



    /**
     * Retrieves user posts from database.
     * @param username with identifier of which user to fetch in db.
     */
    async getUserSavedPosts (username: string, dateFrom: string = "", dateTo: string = "") {
        //let posts = await Post.find({}).select({ "username": 1, "_id": 0, "user_id": 1, "info": 1}).exec();
        let findObject = {
            'username': username
        };
        try {
            dateFrom = this.ANALYSIS_CONFIG.profiles[username].analysis.date_from;
        } catch (err) {
            
        }
        try {
            dateTo = this.ANALYSIS_CONFIG.profiles[username].analysis.date_to;
        } catch (err) {

        }
        if (dateFrom && dateFrom.length !== 0) {
            
            findObject['timestamp']= {
                '$gte': Math.round(new Date(dateFrom).getTime()/1000)
            };
        }
        if (dateTo && dateTo.length !== 0) {
            if (findObject['timestamp'] !== undefined) {
                findObject['timestamp']['$lt'] = Math.round(new Date(dateTo).getTime()/1000)
            } else {
                findObject['timestamp']= {
                    '$lt': Math.round(new Date(dateTo).getTime()/1000)
                };
            }
        }
        //console.log(findObject);
        try {
            let maxPosts: number = this.ANALYSIS_CONFIG.profiles[this.utils.DEFAULT].analysis.limits.max_posts;
            let items = await Post.find(findObject).limit(maxPosts).setOptions({allowDiskUse: true}).exec();
            this.utils.writeLog("[getUserSavedPosts] Fetched " + items.length + " posts for " + username);
            return items;
        } catch (err) {
            this.utils.writeLog("[getUserSavedPosts] Error in getUserSavedPosts");
            this.utils.writeLog(err);
            return [];
        }
    }

    async getUsernameFromId (userId: number, useProxy: boolean = true) {
        let query = "curl";
        let commands = [
            'https://www.instagram.com/graphql/query/?query_hash=c9100bf9110dd6361671f113dd02e7d6&variables=%7B%22user_id%22%3A%22'+userId+'%22%2C%22include_chaining%22%3Afalse%2C%22include_reel%22%3Atrue%2C%22include_suggested_users%22%3Afalse%2C%22include_logged_out_extras%22%3Afalse%2C%22include_highlight_reels%22%3Afalse%2C%22include_related_profiles%22%3Afalse%7D%0A',
            '--compressed'
        ];
        let results = <any>await this.utils.getAnonymousQuery(query, commands, useProxy);
        return results;
    }

    async updateFollowersFollowingCounts (person: IUser) {
        let followersHistoryObject = {
            follower_count: person.follower_count,
            following_count: person.following_count,
            timestamp: Date.now()
        };
        if (!person.follower_count_history) {
            person.follower_count_history = [followersHistoryObject];
        } else {
            person.follower_count_history.push(followersHistoryObject);
        }
        let savedPerson = await this.utils.createOrUpdate(User, "username", person.username, person); 
        return savedPerson;
    }





}