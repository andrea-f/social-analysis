"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var db_schema_1 = require("./db-schema");
var post_1 = require("./post");
var utils_1 = require("./utils");
var profile_1 = require("./profile");
var image_analysis_1 = require("./image-analysis");
var stats_1 = require("./stats");
var stories_1 = require("./stories");
var followers_1 = require("./followers");
process.on('unhandledRejection', function (reason, p) {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
    // application specific logging, throwing an error, or other logic here
    if (reason)
        console.log(reason['stack']);
});
var SocialAnalysis = /** @class */ (function () {
    function SocialAnalysis() {
        this.post = new post_1.PostAnalysis;
        this.utils = new utils_1.Utils;
        this.profile = new profile_1.ProfileAnalysis();
        this.image_analysis = new image_analysis_1.ImageAnalysis();
        this.stats = new stats_1.Stats();
        this.stories = new stories_1.Stories();
        this.followers = new followers_1.FollowersAnalysis;
    }
    // ENTRY POINT
    /**
     * Fetches all available data for a user.
     * @param username Identifier for the user to analyse.
     * @param actions List of things to do on a profile, array.
     */
    SocialAnalysis.prototype.analyseUser = function (username, actions) {
        if (actions === void 0) { actions = []; }
        return __awaiter(this, void 0, void 0, function () {
            var user, err_1, err_2, err_3, err_4, totalFetched, count, labels, labels;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //this.writeLog(username, actions);
                        if (username.length < 4) {
                            this.utils.writeLog("username: " + username + " is too short, returning.");
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.utils.find(db_schema_1.User, "username", username)];
                    case 1:
                        user = _a.sent();
                        if (!(!user || !user.info)) return [3 /*break*/, 5];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        this.utils.writeLog("Getting analysed user info for " + username);
                        return [4 /*yield*/, this.profile.getUserInfo(username)];
                    case 3:
                        user = _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        err_1 = _a.sent();
                        this.utils.writeLog("Couldnt get userinfo because");
                        this.utils.writeLog(err_1);
                        return [3 /*break*/, 5];
                    case 5:
                        if (actions.length === 0) {
                            actions = [
                                "posts",
                                "engagement",
                                "rek",
                                "stories",
                                "userinfo",
                                "contacts",
                                "followers",
                                "labels",
                                "textinfo"
                            ];
                        }
                        if (!(actions.indexOf("posts") !== -1)) return [3 /*break*/, 9];
                        _a.label = 6;
                    case 6:
                        _a.trys.push([6, 8, , 9]);
                        /*if (this.CONFIG.logged_in === true) {
                            this.writeLog("Getting posts for " + username);
                            user = await this.getPosts(username);
                        } else {*/
                        this.utils.writeLog("Getting anonimously posts for " + username);
                        return [4 /*yield*/, this.post.getPostsAnonymous(username)];
                    case 7:
                        user = _a.sent();
                        this.utils.writeLog("FINISHED GETTING POSTS FOR " + username);
                        console.log("FINISHED GETTING POSTS FOR " + username);
                        return [3 /*break*/, 9];
                    case 8:
                        err_2 = _a.sent();
                        this.utils.writeLog("Error in get posts for " + username);
                        this.utils.writeLog(err_2);
                        return [3 /*break*/, 9];
                    case 9:
                        if (!(actions.indexOf("stories") !== -1)) return [3 /*break*/, 14];
                        this.utils.writeLog("Getting stories for " + username);
                        _a.label = 10;
                    case 10:
                        _a.trys.push([10, 12, , 13]);
                        return [4 /*yield*/, this.stories.getActiveStories(username)];
                    case 11:
                        user = _a.sent();
                        console.log("STORY");
                        return [3 /*break*/, 13];
                    case 12:
                        err_3 = _a.sent();
                        this.utils.writeLog("error in get active stories", err_3.toString().substr(0, 100));
                        return [3 /*break*/, 13];
                    case 13:
                        console.log("Finished fetching stories for " + username);
                        _a.label = 14;
                    case 14:
                        if (!(actions.indexOf("followers") !== -1)) return [3 /*break*/, 19];
                        this.utils.writeLog("Getting followers for " + username);
                        _a.label = 15;
                    case 15:
                        _a.trys.push([15, 17, , 18]);
                        return [4 /*yield*/, this.followers.getFollowers(username)];
                    case 16:
                        user = _a.sent();
                        console.log("FOLLOWERS");
                        return [3 /*break*/, 18];
                    case 17:
                        err_4 = _a.sent();
                        this.utils.writeLog("Error in get followers");
                        this.utils.writeLog(err_4.toString());
                        return [3 /*break*/, 18];
                    case 18:
                        console.log("Finished getting followers for " + username);
                        _a.label = 19;
                    case 19:
                        if (!(username === this.utils.CONFIG.username)) return [3 /*break*/, 21];
                        return [4 /*yield*/, this.stories.getAllArchiviedStoriesWithVoters(username)];
                    case 20:
                        user = _a.sent();
                        _a.label = 21;
                    case 21:
                        if (!(actions.indexOf("engagement") !== -1)) return [3 /*break*/, 25];
                        this.utils.writeLog("Calculating " + username + " engagement for posts...");
                        return [4 /*yield*/, this.stats.calculatePostsEngagement(username)];
                    case 22:
                        user = _a.sent();
                        return [4 /*yield*/, this.stats.exportPostsEngagementToCSV(username)];
                    case 23:
                        _a.sent();
                        return [4 /*yield*/, this.post.extractAllTagsAndMentionsFromPostsText(username)];
                    case 24:
                        _a.sent();
                        console.log("Finished calculating engagement for " + username);
                        _a.label = 25;
                    case 25:
                        if (!(actions.indexOf("userinfo") !== -1)) return [3 /*break*/, 27];
                        this.utils.writeLog("Getting all user info for " + username);
                        return [4 /*yield*/, this.profile.populateAllUserInfo(username)];
                    case 26:
                        totalFetched = _a.sent();
                        this.utils.writeLog("Fetched " + totalFetched + " user info for " + username);
                        console.log("Fetched " + totalFetched + " user info for " + username);
                        _a.label = 27;
                    case 27:
                        if (!(actions.indexOf("textinfo") !== -1)) return [3 /*break*/, 29];
                        this.utils.writeLog("Extracting tags and mentions for " + username);
                        return [4 /*yield*/, this.post.extractAllTagsAndMentionsFromPostsText(username)];
                    case 28:
                        _a.sent();
                        _a.label = 29;
                    case 29:
                        if (!(actions.indexOf("contacts") !== -1)) return [3 /*break*/, 31];
                        this.utils.writeLog("Doing most engaged users analysis on " + username);
                        return [4 /*yield*/, this.stats.calculateEngagementAndExportContactInfoToCSV(username)];
                    case 30:
                        user = _a.sent();
                        console.log("Finished exporting contacts for " + username);
                        _a.label = 31;
                    case 31:
                        if (!(actions.indexOf("rek") !== -1)) return [3 /*break*/, 34];
                        this.utils.writeLog("Doing image analysis for " + username + " for all media...");
                        return [4 /*yield*/, this.image_analysis.getPostsImagesAnalysis(username)];
                    case 32:
                        count = _a.sent();
                        this.utils.writeLog("Analysed " + count + " images for " + username);
                        return [4 /*yield*/, this.stats.calculateMostEngagingLabels(username)];
                    case 33:
                        labels = _a.sent();
                        if (Object.keys(labels).indexOf("labels") !== -1) {
                            this.utils.writeLog("Extracted " + Object.keys(labels['labels']).length + " labels for " + username);
                        }
                        else {
                            this.utils.writeLog("Processed labels for " + username);
                        }
                        console.log("Finished doing rek and extractiing labels for " + username);
                        _a.label = 34;
                    case 34:
                        if (!(actions.indexOf("labels") !== -1)) return [3 /*break*/, 36];
                        return [4 /*yield*/, this.stats.calculateMostEngagingLabels(username)];
                    case 35:
                        labels = _a.sent();
                        this.utils.writeLog("Processed most engaging labels for " + username);
                        console.log("Finished extracting just labels for " + username);
                        _a.label = 36;
                    case 36: return [4 /*yield*/, this.utils.createOrUpdate(db_schema_1.User, "username", user.username, user)];
                    case 37:
                        user = _a.sent();
                        return [2 /*return*/, user];
                }
            });
        });
    };
    return SocialAnalysis;
}());
exports.SocialAnalysis = SocialAnalysis;
//# sourceMappingURL=social-analysis.js.map