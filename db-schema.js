"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
//const genders = ['male', 'female'] as const;
exports.UserSchema = new mongoose_1.Schema({
    user_id: { type: Number, required: true, unique: true },
    username: { type: String, required: true, unique: true, index: true },
    profile_pic: { type: String },
    full_name: { type: String },
    email: { type: String },
    phone: { type: Number },
    followers: { type: Array },
    media: { type: Array },
    comments: [],
    following: { type: Array },
    stories: { type: Array },
    gender: { type: String },
    liked: [],
    likers: [],
    scrape_timings_stories_started: { type: Number },
    scrape_timings_stories_finished: { type: Number },
    scrape_timings_posts_started: { type: Number },
    scrape_timings_posts_finished: { type: Number },
    scrape_timings_followers_started: { type: Number },
    scrape_timings_followers_finished: { type: Number },
    scrape_timings_following_started: { type: Number },
    scrape_timings_following_finished: { type: Number },
    posts: [],
    info: {},
    votes_given: [],
    reels_viewed: { type: Array },
    follower_count: { type: Number },
    following_count: { type: Number },
    interacted_with: [],
    interacted_with_engagement: [],
    engagement: {},
    media_count: { type: Number },
    interactions: [],
    image_labels: { type: Array },
    mentions: { type: Array },
    hashtags: { type: Array },
    follower_count_history: { type: Array },
    posted: { type: Array }
    //interactions: { type: Array, required: true, index: true }
}, { _id: true, timestamps: true });
// Commented since already created.
//UserSchema.index({"username": 1, "interactions.score": 1, "interactions.timestamp": 1, "interactions.engagement_with": 1}, {unique: true});
//UserSchema.index({"interactions.score": 1, "interactions.timestamp": 1, "interactions.engagement_with": 1});
//UserSchema.index({"interactions.timestamp": 1, "interactions.engagement_with": 1});
//UserSchema.index({"interactions.timestamp": 1, "interactions.engagement_with": 1});
exports.HashtagSchema = new mongoose_1.Schema({
    name: { type: String, required: true, index: true, unique: true },
    tag_id: { type: Number, unique: true },
    posts: { type: Array },
    usernames: {},
    stats: { type: Array },
    total_posts: { type: Array },
    timestamp: Date,
    info: {},
    description: { type: String },
    profile_pic: { type: String },
    related_tags: { type: Array },
    posters: { type: Array },
    engagement: { type: Object }
}, { _id: true, timestamps: true });
exports.FollowersSchema = new mongoose_1.Schema({
    username: { type: String, required: true, index: true },
    followers: { type: Array, required: true },
    batch_hash: { type: String, required: true },
    scan_dates: { type: Array }
}, { _id: true, timestamps: true });
// A story is a collection of reels
exports.StorySchema = new mongoose_1.Schema({
    username: { type: String, required: true, index: true },
    user_id: { type: Number, required: true },
    story_id: { type: String, required: true, unique: true },
    reels: [],
    info: {},
    timestamp: { type: Date }
}, { _id: true, timestamps: true });
exports.ReelSchema = new mongoose_1.Schema({
    reel_id: { type: String, required: true, unique: true },
    info: {},
    timestamp: { type: Number },
    viewers_count: { type: Number },
    viewers: [],
    voters: []
}, { _id: true, timestamps: true });
exports.PostSchema = new mongoose_1.Schema({
    post_id: { type: String, required: true, unique: true },
    info: {},
    timestamp: { type: Number },
    media: [],
    location: {},
    user_id: { type: Number, required: true },
    username: { type: String, required: true, index: true },
    like_count: { type: Number },
    comment_count: { type: Number },
    text: { type: String },
    likers: [],
    comments: [],
    image_details: [],
    typename: { type: String },
    engagement: { type: Number },
    image_analysis: [],
    hashtags: [],
    mentions: [],
    shortcode: { type: String }
}, { _id: true, timestamps: true });
exports.PostedSchema = new mongoose_1.Schema({
    post_id: { type: String, required: true, unique: true },
    info: {},
    timestamp: { type: Number },
    media: [],
    location: {},
    user_id: { type: Number, required: true },
    username: { type: String, required: true, index: true },
    like_count: { type: Number },
    comment_count: { type: Number },
    text: { type: String },
    likers: [],
    comments: [],
    image_details: [],
    typename: { type: String },
    engagement: { type: Number },
    image_analysis: [],
    hashtags: [],
    mentions: [],
    shortcode: { type: String, required: true, unique: true, index: true }
}, { _id: true, timestamps: true });
//PostSchema.index({"username": 1});
//PostSchema.index({"username": 1, "engagement": 1});
//PostSchema.index({"username": 1, "timestamp": 1});
exports.StatusSchema = new mongoose_1.Schema({
    action_type: { type: String, required: true },
    next_max_id: { type: String },
    user_id: { type: Number, required: true },
    timestamp: { type: Number }
}, { _id: true, timestamps: true });
exports.User = mongoose_1.model('User', exports.UserSchema);
exports.Voter = mongoose_1.model('Voter', exports.UserSchema);
exports.Reel = mongoose_1.model('Reel', exports.ReelSchema);
exports.Story = mongoose_1.model('Story', exports.StorySchema);
exports.Post = mongoose_1.model('Post', exports.PostSchema);
exports.Followers = mongoose_1.model('Follower', exports.FollowersSchema);
exports.Status = mongoose_1.model('Status', exports.StatusSchema);
exports.Hashtag = mongoose_1.model('Hashtag', exports.HashtagSchema);
exports.Posted = mongoose_1.model('Posted', exports.PostedSchema);
//# sourceMappingURL=db-schema.js.map