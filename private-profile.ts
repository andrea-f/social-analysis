
import {User, IUser, Post, Posted} from './db-schema';
import { IgApiClient, PostingPhotoOptions, MediaRepositoryConfigureResponseRootObject } from 'instagram-private-api';
import { PostingLocation, PostingUsertags } from 'instagram-private-api/dist/types/posting.options'
import { CONFIG, ANALYSIS_CONFIG } from './config.interface';
import {Utils} from './utils';
import {ProfileAnalysis } from './profile';

export class PrivateProfile {
    constructor () {
        this.utils = new Utils;
        
        this.IG = new IgApiClient();
        this.CONFIG = this.utils.CONFIG;
        this.ANALYSIS_CONFIG = this.utils.ANALYSIS_CONFIG;
        this.privateLoggedIn = false;
        this.profile = new ProfileAnalysis();
    }

    utils: Utils;
    IG: IgApiClient;
    CONFIG: CONFIG;
    ANALYSIS_CONFIG: ANALYSIS_CONFIG;
    privateLoggedIn: boolean;
    profile: ProfileAnalysis;

    async loginPrivateUser (username: string, password: string) {
        
        if (this.privateLoggedIn === false) {
            const login = await this.profile.login(0, true, username, password);
            if (login && login.pk) {
                this.utils.writeLog("Successfully logged in:  "+ login.pk + " " + login.username);
                this.privateLoggedIn = true;
                this.IG = <IgApiClient>this.profile.getIgApiClient();
                this.utils.writeLog("Got api client: " + ( (this.IG==undefined)? false: true) );
                return login;
            } else {
                this.utils.writeLog("Didn't log in :/");
                console.log("login ERRROR",login);
            }
            
        } else {
            this.utils.writeLog("Already logged in");
        }
        
    }

    async createPostConfig (text: string, file: Buffer, locationOfPost?: PostingLocation, userTags?: PostingUsertags) {
        let postConfig = <PostingPhotoOptions>{
            caption: text,
            file: file,
            //location: locationOfPost,
            //usertags: userTags
        };
        if (locationOfPost != undefined) {
            postConfig.location = locationOfPost;
        }
        if (userTags != undefined) {
            postConfig.usertags = userTags;
        }
        return postConfig;
    }

    async publishPost (postConfig: PostingPhotoOptions) {
        try {
            if (this.IG.state.deviceId !== undefined) {
                let postingResult = await this.IG.publish.photo(postConfig);
                //console.log("postingResult",postingResult)
                return postingResult;
            } else {
                this.utils.writeLog("IgApiClient does not have a device id, so can't publish post.");
            }
        } catch (err) {
            console.log("ERRRRR", err);
        }
        
        
    }

    /**
     * Posts an image with text after creating the config.
     * After posting the published post info are saved in db.
     * @param text what to write in the post to be published, string.
     * @param imagePath location of image on the file system, string.
     * @param locationOfPost contains location details, CURRENTLY NOT WORKING, `PostingLocation`.
     * @param userTags contains tagged in users, CURRENTLY NOT WORKING, `PostingUsertags`.
     */
    async postImageContent (text: string, imagePath: string, locationOfPost?: PostingLocation, userTags?: PostingUsertags) {
        try {
            const file = await this.utils.loadImage(imagePath);
            console.log("Image loaded: " + imagePath);
            let postConfig = await this.createPostConfig(text, file, locationOfPost, userTags);
            let postingResult = await this.publishPost(postConfig);
            if (postingResult && postingResult.media !== undefined) {
                this.utils.writeLog("Posted post with id: " + postingResult.media.pk + " for " + postingResult.media.user.username);
                try {
                    let savedPerson = await this.savePostedItem(postingResult);
                    this.utils.writeLog("Saved posted item " + savedPerson.posted[savedPerson.posted.length - 1].media[0].url + " for " + savedPerson.username + ", total posted: " + savedPerson.posted.length);
                    return savedPerson;
                } catch (err) {
                    this.utils.writeLog("Error in saving posted item");
                    console.log(err);
                }
            } else {
                this.utils.writeLog("Error in posting image: " + imagePath);
                console.log(postingResult); 
            }
            
        } catch (err) {
            this.utils.writeLog("Error in posting content");
            this.utils.writeLog(err);
        }

    }

    /**
     * Saves the posted media in the db.
     * @param item holds information on posted media, `MediaRepositoryConfigureResponseRootObject`.
     */
    async savePostedItem (item: MediaRepositoryConfigureResponseRootObject) {
        try {
            await this.utils.connectToDB();
            let post = new Posted();
            post.post_id = item.media.pk;
            post.user_id = item.media.user.pk;
            post.username = item.media.user.username;
            
            post.timestamp = item.media.taken_at;
            post.shortcode = item.media.code;
            post.info = item.media;
            post.media.push(item.media.image_versions2.candidates[item.media.image_versions2.candidates.length - 1]);
            let caption = <any>item.media.caption;
            post.text = (caption.text) ? caption.text: "";
            post.typename = "GraphImage";
            this.utils.writeLog("Saving published post: " + post.post_id);
            try {
                await post.save();
                this.utils.writeLog("Saved published post: " + post.post_id);
            } catch (err) {
                console.log(err);
            }
            let person = await this.utils.find(User, "username", post.username);
            if (!person) {
                person = new User();
                person.user_id = post.user_id;
                person.username = post.username;
                person.profile_pic = item.media.user.profile_pic_url;
                person.full_name = item.media.user.full_name;
                this.utils.writeLog("Saving new user: " + person.username);
                await person.save();
            }
            let postedObject = {
                post_id: post.post_id,
                timestamp: post.timestamp,
                media: post.media,
                text: post.text
            };
            if (person.posted) {
                person.posted.push(postedObject);
            } else {
                person.posted = [postedObject];
            }
            let savedPerson = await this.utils.createOrUpdate(User, "user_id", person.user_id, person);
            this.utils.writeLog("Updated person: " + savedPerson.username + " total published posts: " + savedPerson.posted.length);
            return savedPerson;
        } catch (err) {
            this.utils.writeLog("Error in saving published post: ");
            console.log(err);
        }
        
    }

    

    async postScheduledItems (username: string, scheduledPostsFile: string = "./post-schedule.json") {
        try {
            let scheduledPosts = require(scheduledPostsFile);
            //console.log(scheduledPosts);
            this.utils.writeLog("Found " + scheduledPosts.posts.length + " scheduled posts for all users.");
            let countPosted = 0;
            let posted = false;
            for (let i = 0; i < scheduledPosts.posts.length; i++) {
                if (scheduledPosts.posts[i].username === username && scheduledPosts.posts[i].posted === false) {
                    console.log("Found matching username", scheduledPosts.posts[i].username, username);
                    if (!scheduledPosts.posts[i].posting || scheduledPosts.posts[i].posting === false) {
                        
                        // Is it the right time to post?
                        if (this.utils.isItTimeToPost(scheduledPosts.posts[i].datetime_to_post) === true) {
                            console.log("Its time to post for " + username);
                            // Do the credentials exist?
                            for (let postingUser of this.utils.CONFIG.posting_users) {
                                if (postingUser.username === username) {
                                    scheduledPosts.posts[i].posting = true;
                                    await this.utils.writeFile(scheduledPostsFile, JSON.stringify(scheduledPosts, null, 4));
                            
                                    console.log("Found matching username in credentials", postingUser.username, username);
                                    // If the users are matching
                                    if (this.privateLoggedIn === false) {
                                        let loggedIn = await this.loginPrivateUser(postingUser.username, postingUser.password);
                                        if (loggedIn) console.log("Logged in with " + loggedIn.username);
                                        await this.utils.delay(20);
                                    }
                                    // Post content
                                    try {
                                        let savedPerson = await this.postImageContent(scheduledPosts.posts[i].text, scheduledPosts.posts[i].image);
                                        posted = true;
                                        scheduledPosts.posts[i].posted = true;
                                        
                                        scheduledPosts.posts[i].posted_at = this.utils.timestampToDate(savedPerson.posted[savedPerson.posted.length -1].timestamp);
                                        this.utils.writeLog("Saved published post: " + savedPerson.posted[savedPerson.posted.length -1].post_id);
                                    } catch (err) {
                                        this.utils.writeLog("Error in posting image...");
                                        console.log("error in posting", err);
                                    }
                                    
                                    scheduledPosts.posts[i].posting = false;
                                    //console.log(JSON.stringify(scheduledPosts, null, 4));
                                    await this.utils.writeFile(scheduledPostsFile, JSON.stringify(scheduledPosts, null, 4));
                                    // TODO: Wait some time delay?
                                    countPosted += 1;
                                    break;            
                                }
                            }
                        }
                    } else {
                        this.utils.writeLog("Currently posting something else for " + username);
                    }
                }
            }
            if (posted === true) {
                return countPosted;
            } else {
                this.utils.writeLog("Didn't post anything for "+ username + " so quitting....");
                process.exit(0);
            }
            
        } catch (err) {
            this.utils.writeLog("Error in scheduled posting");
            console.log(err);
        }
    }
}